<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/jso-2024-7688</identifier>
  <creators>
    <creator>
      <creatorName>Buskell, Andrew</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Cultural Ecology in the Court: Ontology, Harm, and Scientific Practice</title>
  </titles>
  <publisher>Journal of Social Ontology</publisher>
  <publicationYear>2024</publicationYear>
  <dates>
    <date dateType="Submitted">2022-11-28</date>
    <date dateType="Accepted">2023-09-29</date>
    <date dateType="Updated">2024-05-07</date>
    <date dateType="Issued">2024-05-07</date>
  </dates>
  <language>en</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">26-667-7688</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="https://creativecommons.org/licenses/by/4.0">This work is licensed under a Creative Commons Attribution 4.0 International License.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
This article charts a path between those who champion the culture concept and those who think it dangerous. This path navigates between two positions: realists who adopt realist conceptions of both the culture concept and the category of cultural groups, and fictionalists who see such efforts as just creative and fictional extrapolation. Developing the fictionalist position, I suggest it overstates the case against realism: there is plenty of room for realist positions that produce well-grounded empirical studies of cultural groups. Nonetheless, I stress the importance of one element of the fictionalist critique: that the choice of ontology can lead to downstream harm. Developing an extended case study around the work of the anthropologist Julian Steward, I show how ontological decision making is an important element of contemporary scientific and policymaking work. I conclude by arguing that greater attention should be paid both to the processes by which ontologies are adopted and the potential consequences that may result.
</description>
    <description descriptionType="SeriesInformation">Journal of Social Ontology, Vol. 10 No. 2 (2024): Special Issue: Cultures and Ontologies</description>
  </descriptions>
</resource>
