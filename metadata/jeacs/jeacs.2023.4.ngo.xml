<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/jeacs.2023.4.ngo</identifier>
  <creators>
    <creator>
      <creatorName>Ngo, Kelly</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Cultural Memory of Early Tang China in the Qunshu zhiyao</title>
    <title titleType="TranslatedTitle">《群書治要》</title>
  </titles>
  <publisher>The Journal of the European Association for Chinese Studies</publisher>
  <publicationYear>2023</publicationYear>
  <subjects>
    <subject/>
  </subjects>
  <dates>
    <date dateType="Submitted">2023-12-26</date>
    <date dateType="Updated">2023-12-26</date>
    <date dateType="Issued">2023-12-26</date>
  </dates>
  <language>en</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">19-652-8472</alternateIdentifier>
  </alternateIdentifiers>
  <sizes>
    <size>199–222 Pages</size>
  </sizes>
  <rightsList>
    <rights rightsURI="https://creativecommons.org/licenses/by/4.0">This work is licensed under a Creative Commons Attribution 4.0 International License.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
The Qunshu zhiyao 群書治要 (Essentials for bringing about order from assembled texts) is a compilation of statecraft writings that was imperially commissioned by and for the Tang emperor Taizong 太宗 (598–649) near the inception of his Zhenguan 貞觀 reign (r. 624–649). It is one of the earliest extant anthologies in China designed to educate a ruler in cultivating an ethical character and governing the state. As its title suggests, the Essentials articulates a distinctive political philosophy through its collection of excerpts drawn from canonical, historical, and masters writings, and from their commentaries. Although this period of Chinese history has attracted scholarly attention in the fields of politics, history, and culture, the Essentials seems to have largely eluded Occidental researchers and there is to date no complete translation into any European language. This article explores the Essentials’ corpus of excerpted materials through the lens of cultural memory, as theorised by Jan and Aleida Assmann since the late 1980’s. In particular, the circumstances surrounding the production of the Essentials are analysed through the institutional communication, reconstruction of cultural knowledge, and binding nature elements of cultural memory theory. The findings shed light on both the Essentials as a cultural memory text of the Zhenguan era and the nature of political discourse during that period of early Tang China.
&#13;

唐太宗於貞觀初期下令編纂的資政典籍《群書治要》，是君王乃至輔臣和各級官吏修身、治國、平天下的教科書，是中國現存最早的匡政著作之一。正如書名所示，《群書治要》通過摘選經典、史書、諸子百家以及相關注疏的治國理政精華，來闡述獨特的政治哲學。初唐時期的政治、歷史和文化，受到學者的廣泛關注，但《群書治要》在西方的研究中似乎鮮有涉及，至今尚未有完整的外文譯本。本文通過揚·阿斯曼（Jan Assmann）和阿萊達·阿斯曼（Aleida Assmann）自1980年代以來提出的文化記憶理論，分析《群書治要》輯錄的內容。文章以文化記憶理論中的三大要素：機構化的關聯、文化的重構性及集體認同的凝聚性為切入點，對《群書治要》的成書歷史背景進行深入的解讀。研究不僅闡明《群書治要》作為貞觀時期文化記憶文本的特點，同時也揭示初唐時期政治語境的本質。
</description>
    <description descriptionType="SeriesInformation">The Journal of the European Association for Chinese Studies, Vol. 4 (2023): Culture and Memory</description>
  </descriptions>
</resource>
