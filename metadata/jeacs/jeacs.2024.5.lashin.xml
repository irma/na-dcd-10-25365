<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/jeacs.2024.5.lashin</identifier>
  <creators>
    <creator>
      <creatorName>Lashin, Roman</creatorName>
      <affiliation>Hong Kong Baptist University</affiliation>
    </creator>
  </creators>
  <titles>
    <title>Reading Yan Lianke’s Fiction through the Lens of Shijing Exegesis</title>
    <title titleType="TranslatedTitle">從《詩經》訓詁視角解讀閻連科小說</title>
  </titles>
  <publisher>The Journal of the European Association for Chinese Studies</publisher>
  <publicationYear>2024</publicationYear>
  <subjects>
    <subject>Chinese Literature</subject>
    <subject>Yan Lianke</subject>
    <subject>Shijing</subject>
    <subject>interpretation</subject>
    <subject>Ballads</subject>
    <subject>Hymns</subject>
    <subject>Odes</subject>
    <subject>fiction</subject>
    <subject>Literature</subject>
  </subjects>
  <dates>
    <date dateType="Submitted">2024-09-12</date>
    <date dateType="Accepted">2024-11-19</date>
    <date dateType="Updated">2024-12-23</date>
    <date dateType="Issued">2024-12-23</date>
  </dates>
  <language>en</language>
  <resourceType resourceTypeGeneral="JournalArticle"/>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">19-663-8955</alternateIdentifier>
  </alternateIdentifiers>
  <sizes>
    <size>18MB</size>
  </sizes>
  <rightsList>
    <rights rightsURI="https://creativecommons.org/licenses/by/4.0">This work is licensed under a Creative Commons Attribution 4.0 International License.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Yan Lianke is one of the most prominent contemporary writers in China and worldwide. This study reaches beyond the established masterpieces of Yan’s oeuvre and concentrates on works that have received less critical attention—namely the satirical novels Hard Like Water and Ballads, Hymns, Odes. The former work is a “revolution plus love” story set in the early People’s Republic of China, while the latter is an account of the travails of a contemporary Chinese professor. This essay focuses on the recurring motif of the protagonists rendering the world around them in ways that resemble different aspects of the Shijing exegetic tradition. While some elements of this tradition may appear dated and of limited value, this essay argues that Yan is interested in exegesis as a practice of unconstrained literary imagination. Yan’s characters, who are either feverishly devoted to a single interpretive framework or are stumbling in search of an innermost meaning, serve as metaphors for trials and tribulations experienced by intellectuals in their quest for truth and knowledge. Thus, the close reading presented in this essay explores the focal point of Yan’s fiction and the long-lasting tradition of interpreting Shijing poems that is manifested in the inquisitive intellectual spirit of the protagonists of Hard Like Water and Ballads, Hymns, Odes.


本文通過對閻連科長篇小說《風雅頌》和《堅硬如水》的分析，探討其作品中主人公觀察和解讀世界的方式與《詩經》詮釋傳統之間的內在聯繫。文章首先簡要回顧《詩經》詮釋史中的關鍵流派，尤其是“毛詩”的解讀傳統。在《風雅頌》中，主人公楊科以知識份子的身份詮釋世界，其關鍵的詮釋轉變過程映射了《詩經》詮釋的發展脈絡，特別是對“淫詩”的註釋。《堅硬如水》則展現了文革時期高愛軍如何借助“毛體”語言將日常生活政治化為革命寓言，這與“毛詩”對《詩經》愛情詩的政治化解讀異曲同工。通過對這兩部作品的對比分析，本文揭示了閻連科作品中人物形象與藝術風格的發展軌跡，為深入理解其後期創作提供了重要參照。
</description>
  </descriptions>
  <relatedItems>
    <relatedItem relationType="IsPublishedIn" relatedItemType="Journal">
      <relatedItemIdentifier relatedItemIdentifierType="EISSN">2709-9946</relatedItemIdentifier>
      <titles>
        <title>The Journal of the European Association for Chinese Studies</title>
      </titles>
      <volume>5</volume>
      <issue>The Journal of the European Association for Chinese Studies, Vol. 5 (2024): Commentary and Exegesis</issue>
    </relatedItem>
  </relatedItems>
</resource>
