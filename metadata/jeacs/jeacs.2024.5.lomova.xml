<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/jeacs.2024.5.lomova</identifier>
  <creators>
    <creator>
      <creatorName>Lomová, Olga</creatorName>
      <affiliation>Charles University</affiliation>
    </creator>
  </creators>
  <titles>
    <title>Exposing the Authorial Intent? </title>
    <title titleType="TranslatedTitle">自現其意？</title>
  </titles>
  <publisher>The Journal of the European Association for Chinese Studies</publisher>
  <publicationYear>2024</publicationYear>
  <subjects>
    <subject>Xie Lingyun</subject>
    <subject>Shanju fu</subject>
    <subject>grand fu</subject>
    <subject>self-commentary</subject>
    <subject>fu rhapsodies</subject>
    <subject>Xie Lingyun</subject>
  </subjects>
  <dates>
    <date dateType="Submitted">2024-01-22</date>
    <date dateType="Accepted">2024-11-19</date>
    <date dateType="Updated">2024-12-23</date>
    <date dateType="Issued">2024-12-23</date>
  </dates>
  <language>en</language>
  <resourceType resourceTypeGeneral="JournalArticle"/>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">19-663-8505</alternateIdentifier>
  </alternateIdentifiers>
  <sizes>
    <size>18MB</size>
  </sizes>
  <rightsList>
    <rights rightsURI="https://creativecommons.org/licenses/by/4.0">This work is licensed under a Creative Commons Attribution 4.0 International License.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
One remarkable feature of the early-fifth-century autobiographical poem Shanju fu 山居賦 (Fu on dwelling in the mountains) by Xie Lingyun 謝靈運 (385–433) is that the poet provided his verse with regular annotation in what he called “self-commentaries” (zizhu 自註). The Shanju fu, composed in the grand fu genre, was written after the poet, following his abrupt decision, not sanctioned by the court, to retire from office, settled on his ancestral estate in Shining in Guiji prefecture on the eastern periphery of Song state. The poem provides extensive descriptions of the environment through the eyes of the poet personally observing, discovering, inspecting, managing, and enjoying his estate. These descriptions are framed with brief meditations on the general topic of living in reclusion, the lives of the poet and his ancestors, and his own pursuit of Daoist longevity and Buddhist enlightenment. Unlike previous scholarship about the Shanju fu, I discuss the poem from the perspective of its self-commentaries and argue that they are an important structural device enabling the author to mitigate the potentially dangerous rhetoric of political independence and sovereignty subtly expressed in the poem.


新劉宋朝局勢剛剛穩定不久，謝靈運（385–433年）辭官歸隱，回到祖居始寧別墅，創作了著名的自傳體辭賦《山居賦》，展現了作為莊園主的獨立性和高雅風致。在這篇宏大的賦作中，詩人不僅繼承了漢大賦恢弘的結構和磅礴的氣勢，還大膽地融入了創新元素。不同於傳統賦體的誇張與華麗，他以第一人稱生動地描繪家族莊園的細節和自己的山居生活，記錄了個人的親身經歷與細膩觀察。此外，作者在行文中添加了豐富的自註，使《山居賦》在賦史上獨樹一幟。與以往關於《山居賦》的研究不同，本文從自註的角度切入，將其視為一種重要的結構設計。通過對內容的精心註釋，謝靈運巧妙地引導讀者按照他設定的路徑理解作品，避免因誤讀而引發潛在的顛覆性解讀，進而消解關於他“背叛朝廷”或“稱雄鄉裡”的猜測和議論。
</description>
  </descriptions>
  <relatedItems>
    <relatedItem relationType="IsPublishedIn" relatedItemType="Journal">
      <relatedItemIdentifier relatedItemIdentifierType="EISSN">2709-9946</relatedItemIdentifier>
      <titles>
        <title>The Journal of the European Association for Chinese Studies</title>
      </titles>
      <volume>5</volume>
      <issue>The Journal of the European Association for Chinese Studies, Vol. 5 (2024): Commentary and Exegesis</issue>
    </relatedItem>
  </relatedItems>
</resource>
