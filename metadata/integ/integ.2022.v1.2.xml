<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/integ.2022.v1.2</identifier>
  <creators>
    <creator>
      <creatorName>Szafran, Matt</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Surface Pitting on Predynastic Palettes</title>
  </titles>
  <publisher>Interdisciplinary Egyptology</publisher>
  <publicationYear>2022</publicationYear>
  <subjects>
    <subject>predynastic</subject>
    <subject>palette</subject>
    <subject>surface pitting</subject>
    <subject>use wear</subject>
    <subject>experimental archaeology</subject>
  </subjects>
  <dates>
    <date dateType="Submitted">2021-11-19</date>
    <date dateType="Accepted">2022-03-08</date>
    <date dateType="Updated">2024-11-29</date>
    <date dateType="Issued">2022-06-25</date>
  </dates>
  <language>en</language>
  <resourceType resourceTypeGeneral="JournalArticle"/>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">21-599-6473</alternateIdentifier>
  </alternateIdentifiers>
  <relatedIdentifiers>
    <relatedIdentifier relatedIdentifierType="DOI" relationType="IsPartOf">10.25365/integ.2022.v1</relatedIdentifier>
  </relatedIdentifiers>
  <rightsList>
    <rights rightsURI="https://creativecommons.org/licenses/by-nc-nd/4.0">This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Stone palettes varying in size and morphology are the third most common objects found in Predynastic Egyptian burials, after beads and pottery. The understanding of their use is based in the late 19th century assertions that they were devices for processing pigments, specifically malachite, for use as eye paint – for this reason they are often referred to as ‘cosmetic palettes’. However, 21st century scholars have begun to suggest that palettes played a much more nuanced and ritualistic role with the elites of the Predynastic societies.
&#13;

 
&#13;

Whilst it has been overlooked in previous scholarly studies, a significant number of Predynastic palettes display pitting on their surfaces, and this may be indicative of a specific type of use. This article will investigate and test this hypothesis using data analysis of the Predynastic Palette Database (PPDB). This statistical analysis will look at the both the overall prevalence of surface pitting on the PPDB corpus of palettes, and also to investigate whether the period of the palettes’ manufacture (as determined by morphology) is significant in the presence of surface pitting – something which may indicate a change in use of palettes over time. Using the results of this data and the results from Reflectance Transformation Imaging (RTI), microscopy, and experimental archaeological studies, two possible causes of surface pitting will be explored to determine the more probable. The presence and location of surface pitting on a number of different palettes, including the later carved ‘ceremonial’ palettes, and the possible meaning will then be considered.
</description>
  </descriptions>
  <relatedItems>
    <relatedItem relationType="IsPublishedIn" relatedItemType="Journal">
      <relatedItemIdentifier relatedItemIdentifierType="EISSN">2788-7405</relatedItemIdentifier>
      <titles>
        <title>Interdisciplinary Egyptology</title>
      </titles>
      <volume>1</volume>
      <issue>1</issue>
    </relatedItem>
  </relatedItems>
</resource>
