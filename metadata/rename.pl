#!/usr/bin/perl

use strict;

use Data::Dumper;
$Data::Dumper::Indent= 1;

my $ticket_nr;
my $do_rename= 0;

my %journal_info=
(
  # === prefix 10.25365 ==============================
  cts    => { domain => 'chronotopos.eu',         journal_path => 'cts',    prefix => '10.25365', na_id => 1 },
  kf     => { domain => 'journals.univie.ac.at',  journal_path => 'kf',     prefix => '10.25365', na_id => 1 },
  oezg   => { domain => 'journals.univie.ac.at',  journal_path => 'oezg',   prefix => '10.25365', na_id => 1 },
  integ  => { domain => 'journals.univie.ac.at',  journal_path => 'integ',  prefix => '10.25365', na_id => 1 },
  rezens => { domain => 'rezenstfm.univie.ac.at', journal_path => 'tfm',    prefix => '10.25365', na_id => 1 },
  vlr    => { domain => 'viennalawreview.com',    journal_path => 'vlr',    prefix => '10.25365', na_id => 1 },
  wbagon => { domain => 'wbagon.univie.ac.at',    journal_path => 'wbagon', prefix => '10.25365', na_id => 1 },
  wdr    => { domain => 'journals.univie.ac.at',  journal_path => 'wdr',    prefix => '10.25365', na_id => 1 },
# yis    => { domain => 'journals.univie.ac.at',  journal_path => 'yis',    prefix => '10.25365', na_id => 1 }, # bis 2021-02-02, siehe #24063
  yis    => { domain => 'yis.univie.ac.at',       journal_path => 'yis',    prefix => '10.25365', na_id => 1 }, # ab  2021-02-02, siehe #24063
  aaj    => { domain => 'journals.univie.ac.at',  journal_path => 'aaj',    prefix => '10.25365', na_id => 1 }, # AAJ Minikomi; DOI pattern: 10.25365/aaj-2020-88-01
  lili   => { domain => 'journals.univie.ac.at',  journal_path => 'lili',   prefix => '10.25365', na_id => 1 }, # libri liberorum
  pzb    => { domain => 'www.protokollezurbibel.at', journal_path => 'pzb', prefix => '10.25365', na_id => 1 }, # *Protokolle zur Bibel* <https://www.protokollezurbibel.at/>
  tde    => { domain => 'www.tde-journal.org',    journal_path => 'tde',    prefix => '10.25365', na_id => 1 }, # *TDE* <https://www.tde-journal.org/index.php/tde>
  rhy    => { domain => 'journals.univie.ac.at',  journal_path => 'rhy',    prefix => '10.25365', na_id => 1 },
  mpzd   => { domain => 'journals.univie.ac.at',  journal_path => 'mpzd',   prefix => '10.25365', na_id => 1 },
  jeacs  => { domain => 'journals.univie.ac.at',  journal_path => 'jeacs',  prefix => '10.25365', na_id => 1 }, # NOTE: DOI pattern: 10.25365/jeacs.2020.1.317-323, see below; "Journal of the European Association for Chinese Studies"
  exf    => { domain => 'exfonte.org',            journal_path => 'exf',    prefix => '10.25365', na_id => 1 }, # NOTE: DOI pattern: 10.25365/exf-2022-1-0, see #29284; Ex Fonte – Journal of Ecumenical Studies in Liturgy

  tyche => { domain => 'tyche.univie.ac.at', journal_path => 'tyche', prefix => '10.25365', na_id => 1 }, # DOI pattern: 10.25365/tyche-2020-35-3
# earlier editions: tyche     => { domain => 'tyche-journal.at',  journal_path => 'tyche',     prefix => '10.15661', na_id => 4, separator => '.' }, # Tyche Journal, WIP
  jso    => { domain => 'journalofsocialontology.org', journal_path => 'jso', prefix => '10.25365', na_id => 1 }, # "Journal of Social Ontology" (https://journalofsocialontology.org
  kshf   => { domain => 'journals.univie.ac.at',  journal_path => 'keshif',   prefix => '10.25365', na_id => 1 },
  adv    => { domain => 'journals.univie.ac.at',  journal_path => 'adv',      prefix => '10.25365', na_id => 1 },

  # === prefix 10.21243 ==============================
  mi     => { domain => 'journals.univie.ac.at',  journal_path => 'mp',     prefix => '10.21243', na_id => 2 },

  # === prefix 10.14764 ==============================
  '10.ASEAS' => { domain => 'aseas.univie.ac.at',     journal_path => 'aseas',  prefix => '10.14764', na_id => 3 },

);

my @files= ();
while (my $arg= shift (@ARGV))
{
  if ($arg =~ m/^-/)
  {
    if ($arg eq '--ticket') { $ticket_nr= shift(@ARGV); }
    elsif ($arg eq '--rename') { $do_rename= 1; }
    else { usage(); }
  }
  else
  {
    push (@files, $arg);
  }
}


my @hdr= qw( id na_id identifier context_id context_pid canonical_url ticket ts_md_fetch ts_dcd_registration ts_dcd_update );
my $ids_file= 'ids.tsv';
local *FO;

if (-f $ids_file)
{
  print "appending to [$ids_file]\n";
  open (FO, '>>:utf8', $ids_file) or die("can't save [$ids_file]");
}
else
{
  print "creating [$ids_file]\n";
  open (FO, '>:utf8', $ids_file) or die("can't save [$ids_file]");
  print FO join("\t", @hdr), "\n";
}

my $ts_dcd_reg= ts();

foreach my $fnm (@files)
{
  my $res= check_file($fnm);
  print "res: ", Dumper ($res);

  print FO join("\t", '', $res->{na_id}, $res->{identifier}, 1, '', $res->{article_url}, $ticket_nr, $res->{ts_md_fetch}, $ts_dcd_reg, ''), "\n";

  if (-f $res->{new_name})
  {
    print STDERR "ALERT: target file already exists! [", $res->{new_name}, "]\n";
  }
  elsif ($do_rename)
  {
    rename($res->{old_name}, $res->{new_name})
  }
  else
  { # nothing to do
  }
}
close(FO);

exit (0);

sub usage
{
  system('perldoc', $0);
  exit(1);
}

sub check_file
{
  my $fnm= shift;

  # TODO: maybe we should use some XML library instead of reading the plain file
  open (FI, '<:utf8', $fnm) or die "can't read $fnm";

  my ($identifier, $prefix, $suffix, $journal_id, $journal_info);
  while (<FI>)
  {
    chop;
    if (m#<identifier identifierType="DOI">(.+)</identifier>#)
    {
      $identifier= $1;
      ($prefix, $suffix)= split(/\//, $identifier);

      # match DOI pattern
      if ($suffix =~ m#^(jeacs)\.#)
      {
        $journal_id= $1;
      }
      else
      {
        my @suffix= split('-', $suffix);
           @suffix= split(/\./, $suffix) if (@suffix == 1);
        print __LINE__, " suffix: ", Dumper(\@suffix);
        $journal_id= $suffix[0];
      }
      print __LINE__, " journal_id=[$journal_id]\n";
      $journal_info= $journal_info{$journal_id};

      die("invalid prefix [$prefix], identifier=[$identifier] suffix=[$suffix] journal_id=[$journal_id] in fnm=[$fnm]") unless ($prefix eq $journal_info->{prefix});
    }
  }
  close (FI);

  if (defined ($identifier) && defined ($suffix))
  {
    my @fnm= split('-', $fnm);

    my $content_class= $fnm[3]; # articles or issues (plural form!)
    my $content_id=    $fnm[4];

    chop($content_class); # remove the s at the end

    my $new_name= $identifier .'.xml';
    my $article_url= sprintf ("https://%s/index.php/%s/%s/view/%s", $journal_info->{domain},
                       $journal_info->{journal_path}, $content_class, $content_id);

    my @st= stat($fnm);

    return
    {
      identifier => $identifier,
      journal_id => $journal_id,
      old_name   => $fnm,
      new_name   => $suffix .'.xml',
      metadata_filename => $new_name,
      article_url => $article_url,
      ts_md_fetch => ts($st[9]),
      na_id       => $journal_info->{na_id},
    };
  }

  return undef;
}

sub ts
{
  my $e= shift || time ();

  my @ts= gmtime($e);
  sprintf ("%4d-%02d-%02dT%02d%02d%02dZ", $ts[5]+1900, $ts[4]+1, $ts[3], $ts[2], $ts[1], $ts[0]);
}

__END__
