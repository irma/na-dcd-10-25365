<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/oezg-2021-32-1-1</identifier>
  <creators>
    <creator>
      <creatorName>Garstenauer, Therese</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Historicizing Bureaucratic Encounters</title>
    <title titleType="TranslatedTitle">Historicizing Bureaucratic Encounters</title>
  </titles>
  <publisher>Austrian Journal of Historical Studies</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2021-12-11</date>
    <date dateType="Updated">2021-12-14</date>
    <date dateType="Issued">2021-12-14</date>
  </dates>
  <language>en</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">15-542-6520</alternateIdentifier>
  </alternateIdentifiers>
  <sizes>
    <size>5–12 Seiten</size>
  </sizes>
  <rightsList>
    <rights rightsURI="https://creativecommons.org/licenses/by/4.0/deed.de">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Über Bürokratie spricht heute kaum noch jemand.” – so leitete David Graeber sein Buch über dieses Phänomen ein. Dabei sind bürokratische Begegnungen in den Sozialwissenschaften seit den 1960er Jahren ein immer wieder aktuelles Thema, wie die Studien von Katz Hasenfeld Michael Lipsky bis Vincent Dubois zeigen. Diese Studien befassen sich mit Begegnungen zwischen zumeist staatlichen Bürokratien und Bürokraten einerseits und deren Gegenüber andererseits – „bureaucratic subjects” (Michael Lipsky). 
Anders als in der genannten einschlägigen Forschungsliteratur, die vor allem gegenwartsorientiert ist, wird in diesem Heft eine historisierende Perspektive eingenommen. Die Beiträge decken eine Periode vom ausgehenden 18. Jahrhundert bis zur Gegenwart ab. Dabei geht es um klassische „street-level bureaucracy” im Sinne Michael Lipskys, also Fürsorgeeinrichtungen, Polizei und ähnliche, aber auch um die Bereiche von politischer Verwaltung, Eisenbahnen und öffentlichen Archiven im Zeitalter der Digitalisierung. Der regionale Fokus der hier versammelten Forschungen liegt auf Österreich bzw. der Habsburgermonarchie, Deutschland, Frankreich, Polen, Russland und der Schweiz. Seit dem späten 19. Jahrhundert begann der Staat mehr Kontrolle über und Einfluss auf seine Bürger*innen auszuüben. Diese reagierten auf den Aufstieg der staatlichen Bürokratie, indem sie auf ihre Weise und für ihre Zwecke nutzten, was der Staat anbot und vorschrieb. In der Interaktion mit Behörden kam und kommt es am häufigsten zum Kontakt zwischen Bürger*innen und Staat – verkörpert durch die entsprechenden Bürokrat*innen. 
So breit das Spektrum der in den Beiträgen erforschten Themen ist, gibt es doch Fragen, die in ihnen allen behandelt werden. So geht es etwa darum, welche Vorstellungen es vom idealen oder zumindest adäquaten Gegenüber der Behörde gibt und wie solche Vorstellungen in bürokratischen Interaktionen produziert und korrigiert werden. Es geht um Machtverhältnisse in diesen Interaktionen, um das Auseinanderklaffen von Theorie und Praxis des Umgangs miteinander. Über die Interaktion zwischen Bürokrat*innen und Klient*innen hinaus wird im Band auch das materielle Umfeld – architektonische und technische Bedingungen von bürokratischen Interaktionen – erforscht.
</description>
    <description descriptionType="SeriesInformation">Austrian Journal of Historical Studies, Bd. 32 Nr. 1 (2021): Historicizing Bureaucratic Encounters</description>
  </descriptions>
</resource>
