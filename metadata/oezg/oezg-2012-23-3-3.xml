<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/oezg-2012-23-3-3</identifier>
  <creators>
    <creator>
      <creatorName>Lichtblau, Klaus</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Max Webers „Protestantische Ethik“ in werkgeschichtlicher Betrachtung</title>
    <title titleType="TranslatedTitle">Max Weber’s Protestant Ethic contextualized</title>
  </titles>
  <publisher>Österreichische Zeitschrift für Geschichtswissenschaften</publisher>
  <publicationYear>2020</publicationYear>
  <dates>
    <date dateType="Submitted">2020-06-08</date>
    <date dateType="Updated">2020-06-08</date>
    <date dateType="Issued">2020-06-08</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">15-299-3740</alternateIdentifier>
  </alternateIdentifiers>
  <sizes>
    <size>33–49 Seiten</size>
  </sizes>
  <descriptions>
    <description descriptionType="Abstract">&#13;
&#13;
&#13;

In this essay the ‘Weber-thesis’ is compared with the ‘Steinert-thesis’. Only an interpretation of Max Webers Protestant Ethic which takes into account his ‘systematic’ Sociology of Religion in his posthumus published Economy and Society as well as his Collected Essays of The Economic Ethic of the World Religions is able to clarify Weber’s explanatory historical interests. This includes also the differencies between the idealtypical methodology in his essays concerning the Protestant Ethic and his comparative approach in his later work. Last not least the differencies between a causal analysis in a strict sense and a multicausal analysis in the sense of Goethe’s allegory of the ‘Elective Affinities’ are elucidated. 
&#13;
&#13;
&#13;
</description>
    <description descriptionType="SeriesInformation">Österreichische Zeitschrift für Geschichtswissenschaften, Bd. 23 Nr. 3 (2012): Max Webers Protestantismusthese. Kritik und Antikritik</description>
  </descriptions>
</resource>
