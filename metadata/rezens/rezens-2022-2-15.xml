<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2022-2-15</identifier>
  <creators>
    <creator>
      <creatorName>Krems, David</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Peter Geimer: Die Farben der Vergangenheit. Wie Geschichte zu Bildern wird.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2022</publicationYear>
  <dates>
    <date dateType="Submitted">2022-06-13</date>
    <date dateType="Accepted">2022-11-15</date>
    <date dateType="Updated">2022-11-16</date>
    <date dateType="Issued">2022-11-16</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-609-7394</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Wie Geschichte zu Bildern wird, das verrät bereits der Untertitel, ist Thema dieses Buches. Warum wirken manche Bilder authentischer, also näher an der Wirklichkeit, als andere? Wie nützen Künstler*innen die ihnen zur Verfügung stehenden Mittel, um eine derartige Wirkung zu erzielen? Die Frage ließe sich also auch umgekehrt formulieren: Wie werden Bilder zu Geschichte?


Verfahren der historischen Rekonstruktion oszillieren meist zwischen Vergegenwärtigung und Entzug. Ein Zuviel an Informationen steht dabei einem Zuwenig gegenüber. Geimer vollzieht einen Rösselsprung durch zweihundert Jahre des Nachdenkens über das Wiederherstellen der Vergangenheit, der ihn von Goethe über Nitzsche bis hin zu Niklas Luhmann und Reinhardt Koselleck führt. Am Ende der Einleitung steht damit das Programm fest: Untersucht werden – nun noch einmal in der Sprache des Werks ausformuliert – Verfahren visueller Repräsentation von Geschichte, wobei "historische Rekonstruktion als eine Arbeit am Diskontinuierlichen, einer Verschränkung von Sichtbarkeit und Unsichtbarkeit, Vergegenwärtigung und Entzug" (S. 11) verstanden wird.


Verhandelt wird dieses Vorhaben anhand höchst unterschiedlicher Beispiele einer visuellen Geschichtsschreibung und ihren medialer Techniken, die von der Historienmalerei des 19. Jahrhunderts über Fotografie und Film bis hin zu deren (digitalen) Nachbearbeitungen reichen. Es beginnt mit einer Spurensuche nach vermeintlich unnützen Details im Werk des Historienmalers Ernest Meissonier unter Miteinbezug der damaligen Kritik (Baudelaire) und einer Deutungsweise nach Barths’ Konzept des Wirklichkeitseffekts.


Auch das Bestreben des Malers, sich über den Umweg "authentischer Reliquien" (gemeint sind originale Waffen, Kostüme oder Dekorationsgegenstände) (S. 47) einen privilegierten Zugang zu historischen Momenten zu verschaffen, untersucht Geimer in Hinblick auf die Bedeutung derartiger Verfahren für das fertige Gemälde, wobei im Hintergrund freilich stets die Fotografie mitzudenken ist, die jenen Anspruch auf Vollständigkeit, wie er hier anhand der Malerei verhandelt wird, längst eingelöst hatte.


Eine Steigerung an Wirklichkeitsnähe erfuhr das Schlachtenbild in den Rundbildern, die Ende des 18. Jahrhunderts populär werden und die räumliche Begrenzung zum Kunstwerk aufzuheben versuchten. Geimer untersucht einige der Schlachtenpanoramen (etwa von Jean-Charles Langlois und Anton von Werner), die in Teamarbeit über einen Zeitraum von sechs bis zwölf Monaten entstanden sind, und von denen heute meist nur noch Fragmente, Fotografien oder Konstruktionspläne erhalten sind. Geimer geht es vor allem um die Frage, wie es die die Panoramamaler schafften, die Besucher*innen möglichst umfassend in die Illusion eintauchen zu lassen. Als zentrales Element dieses Immersionsverfahrens fungierte dabei das faux terrain, ein in das Kunstwerk integriertes, reales Objekt, das den Übergang zum Bildraum markiert und gleichzeitig aufhebt. Geimer beschreibt diesen Effekt in Anschluss und Abgrenzung zu vergleichbaren Techniken der bildenden Kunst wie Trompe l’œil oder die Einbeziehung realer Objekte, wie sie etwa Kurt Schwitters und Max Ernst forcierten. Gegen Ende des Kapitels über die Panoramen geraten neue bildgebende Innovationen in den Blick. So dienten dem Panorama-Maler Jean-Charles Langlois bereits Camera Obscura und frühe Fotografie als Hilfsmittel. Mit dem dritten Kapitel "Im Fixierbad der Geschichte" landet Geimer denn auch in seinem ausgewiesenen Fachgebiet, nämlich jenem der Fotografie. Hier begegnen wir rasch den altbekannten Größen der Fototheorie. So werden etwa Louis de Clercs Ansichten von Jerusalem (1860) in ein Verhältnis zur Malerei gestellt und in Hinblick auf ihr auratisches Potential (Benjamin) und ihre Beweisfunktion (Barthes) diskutiert. In Krakauers berühmten Aufsatz zur Photographie von 1927 erkennt Geimer schließlich die Grundfigur der eigenen Untersuchung: "das Nebeneinander von Sichtbarkeit und Wiederholbarkeit, Vergegenwärtigung und Entzug." (S. 118) Die von Krakauer in Bezug auf die Fotografie entwickelten Begriffe "Generalinventar" und "Sammelkatalog" – im Sinne eines visuellen Überschusses, wie er bereits vom Foto-Pionier Fox Talbot beschrieben wird – dienen in der Folge zur Erklärung dessen, was Fotografie von anderen bildgebenden Verfahren wesentlich unterscheidet. Wo die zuvor behandelten Historienmaler noch einen Wirklichkeitseffekt herbeikonstruieren mussten, erledigt das die Kamera nun ganz von selbst. Ein Trugschluss freilich, wenn man denkt, damit wäre dauerhaft etwas eingelöst. Bereits in einer der folgenden Analysen zweier Aufnahmen der, im Zweiten Weltkrieg umkämpften, Pazifikinsel Iwo Jima (einmal das bekannte Foto Joe Rosenthals, einmal ein vergleichsweise Unbekanntes von Lou Lowery) wird gezeigt, wie sehr sich ikonische Aufnahmen dieses fotografischen Grundprinzips entledigen können, um stattdessen den Weg eines festgeschriebenen Interpretationsspielraums zu beschreiten.


An derartigen Stellen drängen sich Henri Cartier-Bressons Aussagen zur Fotoreportage auf. Demnach geht es nämlich nicht bloß um jene raren Bilder, die den entscheidenden Augenblick festhalten, sondern auch um all die zahllosen Aufnahmen, die beim "Zusammenschneiden des Rohmaterials" (Cartier-Bresson) eben ganz einfach anfallen. Geimer verzichtet auf eine derartige Kategorisierung und betrachtet alle erwähnten Fotos nach exakt gleichen Maßstäben. Ganz so als würden Herstellungsbedingungen und Verwertungslogik (zumal im Bereich des Fotojournalismus) keine Rolle spielen. Es scheint dem Autor bei derartigen Analysen also allein am Zeugnischarakter der Fotografien gelegen zu sein. Für die zentrale Fragestellung des Buches funktioniert das, dennoch würde man einige der gebrachten Beispiele – gerade weil sie so umfassend eingeführt werden – auch gerne in eine weiter gefasste Analyse eingebunden sehen.


Anhand eines bekannten Beispiels (Odeonsplatz 1914) des späteren "Führer-Fotografen" Heinrich Hoffmann, auf dem der damals noch unbekannte Hitler – scheinbar zufällig mitaufgenommen und später aus der anonymen Masse herausgehoben – zu erkennen ist, beschreibt Geimer wie Fotos nachträglich Bedeutung zugeschrieben werden kann. Im Extremfall findet ein solcher Vorgang anhand von Bildern statt, die besonders markante Geschichts- oder Lebensabschnitte festhalten. Auf mehrfache Weise fügen sich die letzten Aufnahmen von Robert Capa (einmal als abgebildete Person, einmal als Fotograf) in diesen Abschnitt. Leitet sich doch auch Robert Capas Bekanntheit ganz wesentlich von einem Bild ab, dessen Authentizität bis heute nicht geklärt ist (das Negativ ist, genauso wie das des noch unbekannten Hitler von 1914, verschollen). Der Verdacht der Inszenierung/Fälschung ändert nichts an der Wirkungsmacht der Zuschreibung: Hoffmanns Bild wird – unwiderlegbar – in eine Fotografie des zukünftigen Reichskanzlers umgedeutet. Und genauso wird der fallende republikanische Milizionär von Robert Capa für immer ein Zeichen für die Gräuel des Spanischen Bürgerkriegs bleiben. Die historischen Zusammenhänge sind bekannt und werden etwas später neuerlich Thema: Geimer rollt die Auseinandersetzung um die Wehrmachtsausstellung auf und beschreibt das Spannungsverhältnis von Text und Bild, mit dem Historiker*innen umzugehen haben. In der Analyse greift der Autor auch auf jene bekannten Aufnahmen zurück, die Georges Didi-Huberman in seinem bedeutenden Werk Bilder trotz allem (orig. 2004, 2007 von Peter Geimer ins Deutsche übersetzt) behandelt hat. Jene vier Bilder also, die im Sommer 1941 heimlich von Häftlingen im Konzentrationslager Auschwitz gemacht wurden. Geimer verbindet diese – nicht zuletzt aufgrund ihrer formalen Mängel – hochwirksamen Bilder mit jenen, die in den Wehrmachtsausstellungen zu sehen waren und reflektiert die im Ausstellungskontext verwendeten Zuschreibungen: Neben "angemessener Deutung" und "strenger Sachlichkeit" ist hier auch immer wieder von der "undisziplinierten Wirkung" der Bilder die Rede, der eine derartige Ausstellung Herr werden müsse. Wer jedoch die Bilder selbst betrachtet, ist freilich geneigt, alle Versuche der Interpretation zurückzuweisen und sich jenem Gedanken anzuschließen, den Geimer unter Rückgriff auf Helmut Lethen formuliert: methodische Handwerkszeug sei hier nicht zuständig. Es ist schlichtweg unmöglich, auf derartigen Bildern etwas zu verstehen. Ein Befund, der auf die Ausgangsfrage des vorliegenden Werks zurückfällt: "Vielleicht muss man die Wirkung der Bilder gar nicht beschreiben. Ihr stummes Zeugnis stellt sich ohnehin ein, indem man sie – begleitet vom notwendigen Wissen – zeigt." (S. 164)


In Folge widmet sich der Autor einem unterrepräsentierten Kapitel der Fotografiegeschichtsschreibung, jenem der Farben. Oder besser ausgedrückt: dem Fehlen der Farben. Hierfür wesentliche Theorien (Barthes, Flusser) werden vor dem Hintergrund der zunehmend populären Praxis der Nachkolorierung verhandelt. So wie die – zugegeben spärliche – Forschung zum Thema fast ausschließlich nach Antworten in den Theorien der Säulenheuligen des Mediums sucht, werden Zwänge der Praxis (die Technik will es, dass Fotografien in ihrer massenmedialen Ausformung nur selten auch als Fotografien, sondern meist als Drucke reproduziert/betrachtet werden) auch hier weitgehend ausgeblendet. Geimer erkennt zwei Modi der Rezeption, die unterschiedliche Reaktionen adressieren: Beglaubigung und Distanzierung hier (SW), Empathie und Anverwandlung dort (Farbe). Beispiele von Nachkolorierungen (etwa jene aus dem Konzentrationslager Buchenwald) belegen mit Nachdruck Barthes Ablehnung der Farbe, die er in Die helle Kammer als "unechte Zutat, eine Schminke (von der Art, die man den Toten auflegt)", bezeichnet hat. Die von Geimer attestierte Medienvergessenheit, die im Zusammenhang mit solchen Nachkolorierungen stehe, kann in ihrer (durchaus epistemiologischen) Tragweite gar nicht unterschätzt werden. Nachkolorierte Aufnahmen, so argumentiert Geimer etwas später anhand des Films, befeuerten den Irrlauben, "es sei irgendwo zwischen der Wirklichkeit der Geschichte und ihrer filmischen Darstellung eine parasitäre Instanz getreten, deren Wirkung man nun neutralisieren müsse." (S. 222) Die Rede ist von der, mit großem Aufwand hergestellten, TV-Produktion Apokalypse, die siebzig Jahre nach dem Überfall auf Polen vom französischen Fernsehen ausgestrahlt wurde. Der Vorwurf der Entkontextualisierung, den Geimer unter Rückgriff auf Didi-Huberman formuliert, reicht bis zu gegenwärtigen Produktionen und wird anhand Peter Jacksons leidenschaftlich diskutierter Weltkriegsinszenierung They Shall Not Grow Old (2018) noch einmal angebracht. "Die Idee, etwas historisch zu sehen", zitiert Geimer gegen Ende des Buches Getrud Koch, "heißt, etwas in einer Distanz sehen zu können." (S. 237) Vom Umgang mit dieser Distanz, die kein Hindernis, sondern eine Bedingung der Erkennbarkeit sei – so ließe sich zusammenfassend festhalten – handelt das gesamte vorliegende Werk.


Zahlreiche der angesprochenen Punkte (und behandelten Werke) – vor allem aus dem Bereich der Fotografie – kennt man bereits aus anderen Publikationen. Nicht zuletzt aus jenen des Autors selbst. Mitunter wirkt es so, als müsste Geimer ganz einfach noch etwas von seinem breiten Fachwissen loswerden, noch die eine oder andere Überlegung nachreichen. Der umfangreich und vorbildlich illustrierte Text fällt nicht nur sprachlich klar und exakt durchargumentiert aus, er liest sich abschnittweise sogar spannend wie ein Kriminalroman. Mit den zahlreichen – dabei nur scheinbar bedeutungslosen – Episoden und Verweisen auf die Biographien der erwähnten Künstler, erinnert seine Methode selbst an jenen Barthschen Wirklichkeitseffekt, den er zur Analyse der untersuchten Werke heranzieht. Geimer muss sich den Vorwurf gefallen lassen, dass er sowohl in Bezug auf die behandelten Beispiele – als auch deren Referenzierung – fast ausschließlich einem männlichen Blick folgt. Wer gegenüber diesem, etwas aus der Zeit gefallenen, Zugang unempfindlich ist, findet ein fesselndes Buch vor, verspricht: Auskunft darüber, wie Geschichte zu Bildern wird.



Bibliografie:


Roland Barthes, Die helle Kammer, 1980.


Henri Cartier-Bresson, Der entscheidende Augenblick, 1952. 


Georges Didi-Huberman, Bilder trotz allem, 2004.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2022/2</description>
  </descriptions>
</resource>
