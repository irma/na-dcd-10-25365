<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2022-2-03</identifier>
  <creators>
    <creator>
      <creatorName>Sommer-Mathis, Andrea</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Marion Linhardt: Stereotyp und Imagination. Das 'türkische' Bühnenkostüm im europäischen Theater vom Barock bis zum frühen Historismus.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2022</publicationYear>
  <dates>
    <date dateType="Submitted">2022-10-20</date>
    <date dateType="Accepted">2022-10-20</date>
    <date dateType="Updated">2022-11-16</date>
    <date dateType="Issued">2022-11-16</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-609-7602</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Die an der Universität Bayreuth tätige Theaterwissenschaftlerin Marion Linhardt widmet sich der Entwicklung des 'türkischen' Kostüms auf den europäischen Bühnen vom 17. bis zur Mitte des 19. Jahrhunderts. Damit füllt sie eine von ihr zu Recht monierte Lücke in der theaterwissenschaftlichen Forschungsliteratur. Obwohl gerade in den letzten Jahren eine Vielzahl von Publikationen, Ausstellungen und Forschungsprojekten zu den vielfältigen Beziehungen zwischen Orient und Okzident in der Frühen Neuzeit zu verzeichnen ist und dabei gerade auch dem 'Türkischen' in den bildenden und darstellenden Künsten besondere Aufmerksamkeit geschenkt wurde, stellt eine systematische Auseinandersetzung mit dem orientalischen Bühnenkostüm nach wie vor ein Forschungsdesiderat dar.


Das Buch fügt sich hervorragend in den Forschungs- und Publikationsschwerpunkt des Don-Juan-Archivs bzw. des Hollitzer-Verlags, wo seit 2013 in der Reihe Ottomania elf Bände zum kulturellen Austausch zwischen dem Osmanischen Reich und Europa mit besonderem Fokus auf Theater und Musik erschienen sind; Marion Linhardts Studie bildet den 12. Band.


Ausgehend von der Prämisse, dass das 'türkische' Kostüm zu Beginn des 18. Jahrhunderts auf den europäischen Bühnen bereits etabliert gewesen sei, jedoch einen Sonderstatus in der Ästhetik und Theaterpraxis eingenommen habe, nähert sich Linhardt dem Thema im ersten knappen Kapitel ihrer Einleitung mit drei signifikanten Beispielen: Zunächst verweist sie auf einen programmatischen Text von Christlob Mylius aus dem Jahre 1743, in dem dieser sich mit der szenischen Umsetzung von Schauspielen und deren Wirkung auf das Publikum beschäftigt und dabei das Prinzip der "Wahrscheinlichkeit" auch für die Kostümpraxis postuliert. Es geht ihm zwar primär darum, den Einsatz 'originaler', d. h. nicht von der Zeitmode determinierter, antiker Kostüme auf der damaligen Bühne zu fördern, doch wird dabei deutlich, dass im Gegensatz dazu das 'türkische' Kostüm dem Theaterpublikum damals bereits wohl vertraut war. Ähnliches gilt auch für das zweite Beispiel, eine etwa gleichzeitig entstandene Abbildung des Augsburger Kupferstechers Martin Engelbrecht, auf der er die negativ konnotierte Figur des "aller veränderlichsten" Komödianten im Kostüm eines Türken mit Turban, Kaftan, Dolman mit Schärpe und Säbel darstellt, was "für die Bekanntheit der entsprechenden Tracht bei potenziellen Käufern dieses Blattes wie für den Sonderstatus des 'türkischen' Bühnenkostüms im Kontext der Bühnenpraxis jener Zeit" (S. 7) spricht. Auch das dritte von Linhardt gewählte Beispiel, das Libretto des Singspiels Das Serail. Oder: Die unvermuthete Zusammenkunft in der Sclaverey zwischen Vater, Tochter und Sohn (1779), zeigt den hohen Bekanntheitsgrad der 'türkischen' Kostüme im späten 18. Jahrhundert, der es überflüssig machte, sie in den Didaskalien ausführlich zu beschreiben.


Im zweiten Teil ihrer Einleitung wendet sich Linhardt dem Wandel in der Kostümästhetik zu und misst ihn an der Kostümpraxis der Zeit. Das Theaterkonzept der "Wahrscheinlichkeit", das Mylius vertrat, habe zu einem Paradigmenwechsel geführt, der für die praktische Umsetzung erst im letzten Drittel des 18. Jahrhunderts echte Konsequenzen haben sollte, denn bis dahin bildete das französische Hofkleid mit wenigen Abwandlungen die Grundlage für das Bühnenkostüm in fast allen dramatischen Genres (Tragödie, Komödie, Oper, Ballett). Damit war die höfische Zeitmode auch für die 'orientalischen' Kostüme die Norm. Die neue Ästhetik des Theaters, die auf Illusionierung und Emotionalisierung der Theaterbesucher abzielte, führte erst im Theater des späteren 18. Jahrhunderts zu individualisierten Kostümen.


Linhardt nennt als Meilensteine auf dem Weg zu einem 'wahren' Kostüm die Auftritte der beiden Schauspielerinnen Marie Favart (1753 als einfach gekleidete Bäuerin in Les Amours de Bastien et Bastienne, 1761 in einem 'originalen' türkischen Kostüm in Soliman second, ou Les trois sultanes) und La Clairon (1755 in einem 'echt' chinesischen Kleid in Voltaires L'Orphelin de la Chine), denen in der zweiten Hälfte des 18. Jahrhunderts weitere Versuche einer Differenzierung der Kostüme nach Ständen und Charakteren folgten; diese wurden allerdings durch die Gepflogenheiten der Theaterpraxis konterkariert und mündeten letztlich wieder in einer neuerlichen Standardisierung der Kostüme.


Im Hauptteil ihres Buches demonstriert Marion Linhardt auf der Grundlage von zahlreichen Bildquellen zu Theaterstücken aus der Zeit vom frühen 17. bis zur Mitte des 18. Jahrhunderts, wie sich die Vorstellungen von 'türkischen' Kostümen auf den europäischen Bühnen manifestierten und veränderten. Sie betont die Sonderstellung der 'türkischen' Kostüme im westlichen Theater, dienten sie doch schon vor der programmatischen Auseinandersetzung mit dem 'wahren' Kostüm als Bekleidung für sämtliche Figuren orientalischer Herkunft – d. h. für Gestalten aus dem Osmanischen Reich ebenso wie aus Persien oder auch von der arabischen Halbinsel – ohne weitere historische Differenzierung.


Das Spektrum der von Linhardt ausgewählten Beispiele reicht vom französischen Hofballett des frühen 17. Jahrhunderts (Ballet du Grand bald de la Douairière de Billebahaut, 1626) über das deutsche barocke Trauerspiel (Andreas Gryphius, Catharina von Georgien. Oder Bewehrete Beständigkeit, um 1650) und die klassische französische Tragödie (Jean Racine, Bajazet, 1672 und Voltaire, Le Fantasme, ou Mahomet le prophète, 1741) zum italienischen Dramma per musica im höfischen Kontext (Giovanni Ambrogio Migliavacca/Johann Adolph Hasse, Solimano, 1753) und von dort zu den Turquerien des Rokoko (Ballettpantomime von Franz Anton Hilverding, Le Turc généreux, 1758 und "Acte Turc" aus L'Europe galante, um 1765), weiters zur französischen Comédie mit Gesang und Tanz (Charles Simon Favart, Soliman second, ou Les trois sultanes, 1761) und schließlich zur Version eines italienischen Dramma per musica für das kommerzielle Londoner Theater (Pietro Metastasio/Thomas Arne, Artaxerxes, 1762).


Linhardt spannt also nicht nur einen weiten zeitlichen und geographischen Bogen, sondern berücksichtigt auch die wichtigsten dramatischen Gattungen des 17. und 18. Jahrhunderts. Die einzelnen Werke werden jeweils auf dieselbe Art in drei Teilen präsentiert: Zunächst werden in einem Absatz (typografisch mittig gesetzt) die wesentlichen Erkenntnisse vorweggenommen, es folgen in einem weiteren Absatz (kursiv gesetzt) Angaben zum jeweiligen dramatischen Werk, seiner Uraufführung und eventuell auch zu späteren Aufführungen sowie Kommentare zu den Bilddokumenten; daran schließt sich die eigentliche Kostümanalyse samt den entsprechenden Abbildungen an. Dabei fällt wohltuend auf, dass auch die Bildlegenden wesentlich ausführlicher als üblich gestaltet sind und weiterführende Erklärungen zu den Bildern enthalten.


Im Anschluss an diesen Hauptteil zieht Marion Linhardt ein Zwischenresümee, in dem sie einige Tendenzen in der Entwicklung des 'türkischen' Bühnenkostüms vom 17. bis ins frühe 19. Jahrhundert festhält. So bildete das 'türkische' Bühnenkostüm auch während des gesamten 18. Jahrhunderts die Norm für sämtliche 'orientalischen' Sujets, wobei sowohl bei den Männer- als auch bei den Frauenkostümen eine Reihe von konkreten Kleidungsstücken, Materialien und Zuschnitten als typisch 'türkisch' galt. Während für das Männerkostüm zunehmend die oben erwähnte Forderung nach dem 'wahren' Bühnenkostüm laut wurde, erwies sich für das Frauenkostüm die aktuelle europäische Mode als weit wichtiger.


In einem Exkurs widmet sich Marion Linhardt der 'türkischen' Mode außerhalb der Bühne und zeigt auf, wie die mediale Vermittlung der Kenntnisse über türkische Bekleidungspraxis in West- und Mitteleuropa erfolgte. Sie verweist einerseits auf die große Bedeutung von Reiseberichten und Briefen von Personen, die sich (wie etwa Lady Mary Wortley Montagu) selbst im Osmanischen Reich aufgehalten hatten und türkische Kleidung daher aus eigener Anschauung kannten, andererseits auf die zahlreichen mehr oder weniger freien Interpretationen in Werken der bildenden Kunst. Im letzten Viertel des 18. Jahrhunderts gesellte sich zu den vielen Inszenierungen "à la Turque" in Porträts, Maskeraden und Raumausstattungen im Rahmen der zivilen Damenmode die sogenannte "Robe à la Turque", die, ausgehend von Paris, in vielen Modejournalen propagiert wurde.


In den letzten beiden Kapiteln ihres Buches wendet sich Marion Linhardt wieder dem Theater zu und zeigt durch die detaillierte Analyse der Kostüminventare, die sich aus den Schlosstheatern von Jarmeritz/Jaromĕřice (1762) und Krumau/Český Krumlov (1763, 1807) erhalten haben, wie man sich die damalige Kostümpraxis vorzustellen hat. Sie bestand aus der Mehrfachverwendung, Adaption und neuen Kombination der einzelnen im Fundus erhaltenen Kostümteile.


Im Abschlusskapitel zieht die Autorin ein Resümee ihrer Untersuchungen über die 'türkischen' Kostüme im vestimentären Code des Theaters, formuliert nochmals klar ihre Thesen und zeigt interessante Perspektiven für die künftige Forschung auf. Der Anhang, in dem die Bildquellen in chronologischer Folge wiedergegeben werden, ein umfangreiches bibliografisches Verzeichnis, ein Personen- und ein Stückregister sowie der Bildnachweis komplettieren diesen auch optisch ansprechend gestalteten Band.


Die Autorin versteht ihre Auseinandersetzung mit dem 'türkischen' Bühnenkostüm im europäischen Theater "ausdrücklich als Anregung für nachfolgende Forschungen" (S. X), und man darf gespannt sein, ob diese Anregung von der theaterwissenschaftlichen Scientific Community aufgenommen und weitergeführt wird. Die Grundlage, die Marion Linhardt dafür mit ihrer Studie bietet, ist jedenfalls eine überaus solide.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2022/2</description>
  </descriptions>
</resource>
