<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2024-1-01</identifier>
  <creators>
    <creator>
      <creatorName>Seidler, Lisa-Frederike</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Charlotte Arnsperger: Im Archiv. Der Suhrkamp Theaterverlag in den 1960er und 1970er Jahren.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2024</publicationYear>
  <dates>
    <date dateType="Submitted">2023-06-20</date>
    <date dateType="Accepted">2024-04-03</date>
    <date dateType="Updated">2024-05-15</date>
    <date dateType="Issued">2024-05-15</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-674-8068</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Kulturbetriebliche Einrichtungen wie Buchverlage und Agenturen wurden bislang in historiografischen Zugängen zum Theater nach 1945 nicht zentral berücksichtigt. Dies ändert die Literaturwissenschaftlerin Charlotte Arnsperger mit Im Archiv, ihrer 2018 an der Universität Tübingen angenommenen und 2022 publizierten Dissertation. Sie legt das Augenmerk auf das dramenspezifische Verlegen im Suhrkamp Verlag, indem sie die Theaterabteilung des renommierten Frankfurter Verlagshauses in den 1960/70er Jahren untersucht. An der Schnittstelle von Literatur- und Theaterwissenschaft widmet sich die innovative Studie damit dem Verlegen deutschsprachiger Dramatik innerhalb eines Buchverlags und stellt die Theaterverlagsgeschichte ausgehend von noch unerschlossenen Archivbeständen dar. Dadurch erweitert sie den bislang vorrangig fokussierten Zeitraum theaterhistoriografischer Untersuchungen zu Agenturtätigkeiten vom Beginn des 20. Jahrhunderts (vgl. Watzka 2006) um das Verlegen von Theater- und Medientexten in der Bundesrepublik nach 1945.


Im Archiv reiht sich ein in buch-, literatur- und kulturwissenschaftliche Studien zu Verlagsgeschichten, welche die Relevanz solcher Kulturunternehmen für das intellektuelle Klima der alten Bundesrepublik diskutieren (Felsch 2015; Gerlach 2005; Misterek 2002). Nicht selten ist der Suhrkamp Verlag sozialgeschichtlicher Gegenstand, um das Publizieren von Lyrik, Romanen oder auch Theorie basierend auf Archivmaterial zu beleuchten. Einschlägig war dafür das Suhrkamp-Forschungskolleg (2012–2016) am Deutschen Literaturarchiv in Marbach a. N. zur Bestandserschließung und -erforschung des Siegfried-Unseld-Archivs (vgl. Jaspers 2022; Amslinger 2018; Sprengel 2016). In ebenjenem Kontext entstand auch die vorliegende Arbeit, die das Archiv der Theaterabteilung von Suhrkamp zum Ausgangspunkt nimmt und seine prekäre Systematik nicht nur problematisiert, sondern methodisch in einen deskriptiven Zugang zum Verlegen von Dramatik im Archiv überführt.


In acht Teilen beschreibt die Autorin den Suhrkamp Theaterverlag auf Basis von Korrespondenzen, Reisenotizen und Ankündigungen und spürt dabei einer verlagsspezifischen Konzeption des Dramas nach. Arnsperger arbeitet Verbindungen zwischen Theaterinstitutionen, Dramaturg*innen, Autor*innen, Regisseur*innen, aber auch zwischen theater- und medienwissenschaftlichen Einrichtungen heraus, die den Verlag als "'Hauptumschlagplatz' für Autoren, Ideen und Stücke" (S. 9) sichtbar werden lässt.


Die Einleitung gibt einen kurzen Überblick zur Genese von Bühnen- und Theatervertriebsstrukturen im deutschsprachigen Raum. Davon ausgehend argumentiert die Verfasserin, dass es ihr weniger um Distributionswege zwischen Verlag und Bühnen gehe. Vielmehr stünden verlagsseitige Konzeptionen von Drama und Theater im Zentrum, die für den Aufbau der Theaterabteilung ab 1959 konstitutiv gewesen seien. Für deren Freilegung werden vor allem bekannte Namen aus Theater und Literatur der Nachkriegszeit aufgerufen (z. B. Max Frisch, Peter Suhrkamp, Martin Walser).


Der II. Teil ("Quellen und Vorgehensweise") ist methodologischen Überlegungen zum Verlagsarchiv gewidmet. Arnsperger geht der Spezifik von Verlagsarchiven nach, die sie gleichermaßen als Archive für Literatur und die Privatwirtschaft beschreibt. Sie gelangt so von allgemeinen Verlagsarchiven in Deutschland zur Bestandsebene des Siegfried-Unseld-Archivs und differenziert die archivischen Voraussetzungen von Suhrkamps Buch- und Theaterverlag: das "Theaterarchiv" (S. 48) sei vergleichsweise lückenhaft und stehe in Zusammenhang mit differenzierten Arbeitsmodi der Bühnen.


Am umfangreichsten ist der III. Teil, der sich dem "Dramenboom" widmet. Ausgehend von den Reihen Suhrkamp Spectaculum und Im Dialog diskutiert die Verfasserin die Aufmachung von Dramenanthologien. Im Rekurs auf Willy Fleckhaus werden Umschlaggestaltungen und Typographien exemplifiziert. Entscheidend ist, dass die Verfasserin nicht nur Designmerkmale wiedergibt, sondern die "Lesebühne[n] der Dramatik des Verlags" (S. 91) im Zusammenspiel mit Peter Szondis Theorie des modernen Dramas von 1956 diskutiert. Anders als z. B. in Siegfried Melchingers aufführungszentriertem Theater der Gegenwart aus demselben Jahr, erkennt sie in der Dissertation Szondis eine prinzipielle Aufwertung der jüngeren Dramenlektüre. Ausgehend davon attestiert sie ab Ende der 1950er Jahre, und damit parallel zum Aufbau der Theaterabteilung, eine Konjunktur des Lesedramas im Taschenbuch, das als "mobiles Buch" (S. 79) auch zur Mitnahme ins Theater einlade.


Ab dem IV. Teil wird der Theaterverlag in Konkurrenz zum Mediensektor in der BRD dargestellt. Auf die Verbreitung von Massenmedien reagiert der Verlag mit der Platzierung von Theateraufzeichnungen im öffentlich-rechtlichen Fernsehen und mit Rundfunkkooperationen. Der "Medienwandel" (S. 104) werde aber auch innerhalb des Verlagshauses von politischen Nutzungsdebatten flankiert, wie die Autorin in Bezug auf Enzensbergers Bewusstseins-Industrie verdeutlicht (vgl. S. 118).


Mit den medialen Verschiebungen modifiziert Arnsperger ihre Perspektive: Der Teil "Theaterräume" löst sich von den engen Vorgaben des Archivs und nimmt Bezug auf theaterwissenschaftliche Theorien. Im Rekurs auf Hans-Thies Lehmanns Postdramatisches Theater (1999) und Erika Fischer-Lichtes Ästhetik des Performativen (2004) konstatiert sie eine Zäsur im Theater der 1960er Jahre, welche das "Zentrum der Theaterpraxis" (S. 129) verrücke und infolge dessen dem Absatz von Dramatik zuwiderlaufe. Referenziert werden einschlägige Debatten seit den 1950er Jahren. Happening, "Mitspiel" (S. 139) oder auch Diskussionen um Theaterarchitektur bindet Arnsperger an personelle Knotenpunkte im Suhrkamp-Netzwerk zurück, wobei der Bezug zum Verlegen von Dramatik an dieser Stelle eher latent ist.


Die Absatzminderung deutschsprachiger Dramen aus dem Suhrkamp Verlag bilanziert die Verfasserin abschließend als "Dramenkrise". Begleitet wird diese auf institutioneller Ebene von der kulturgeschichtlich weithin bekannten Kündigungswelle in Zusammenhang mit dem "Aufstand der Lektoren" 1968. Gleichzeitig identifiziert Arnsperger neben einer Renaissance von Klassikerinszenierungen auch die "Werktreue"-Diskussion als Teil der "Dramenkrise". Sie nimmt zwar Christopher Balmes Einordnung der national-völkischen Färbung des Begriffs zur Kenntnis, hält allerdings entgegen, dass der Werkstatus für kulturökonomische Debatten um die Entlohnung zeitgenössischer Theaterautor*innen, wie sie sie im Archiv vorfinde, entscheidend sei.


Der VII. Teil wagt einen "Ausblick in die 1970er Jahre". Die Gründung des Medienverlags und der Suhrkamp-Produktionsgesellschaft markiere eine Verlagerung von der Theorie zur Praxis innerhalb des Unternehmens. Damit schließt Arnsperger wieder an ihre Ausgangsthese des Aufbruchs der Dramenlektüre durch Szondis Theorie des modernen Dramas an. Der Dramenboom ende sodann in den 1970er Jahren in einer Krise, die im "Fehlen zeitgenössischer Dramatik" (S. 199) kulminiere.


Gibt diese Gesamtkonzeption der Dissertation über den Theaterverlag in zwei Dekaden zwar eine thesenhafte Form, führt sie im Fazit jedoch zu einer nicht unproblematischen Generalisierung der Dramenkrise als Theaterkrise: "Die 1960er Jahre sind für das Theater eine Phase der Blüte, des Experiments und der Visionen, und diese Bewegungen werden im Theaterverlag aufgegriffen oder angestoßen. Dagegen bricht mit den angehenden 1970er Jahren eine Krisenstimmung an, die die enthusiastische Aufbruchsbewegung der vorherigen Dekade ablöst." (S. 211) Interessant wäre hier allerdings auch, den ausgehend vom Suhrkamp Theaterverlagsarchiv akzentuierten Bedeutungsverlust eines dramenbasierten Theaters im Hinblick auf den größeren kulturellen Zusammenhang der 1970er Jahre zu betrachten. Die Frage, wie es im Verhältnis dazu anderen Verlagen und Agenturen erging – eventuell sogar unter Berücksichtigung der Dramenproduktion in der DDR – bleibt offen.


Aus theaterwissenschaftlicher Perspektive irritierend gestaltet sich der Versuch der Engführung rezeptionsästhetischer Theatertheorien mit der Konzeption von Theater im Verlagsarchiv. Hier stellt sich die Frage, wieso die Autorin den Bezug des Postdramatischen Theaters zu Szondi weitgehend unkommentiert lässt und sich stattdessen für die Darstellung von Architekturkommentaren und Performances kanonisierter Theatermacher*innen entscheidet.


Dennoch ist es ein besonderer Verdienst dieser Arbeit, dass sie im virulenten Feld von Verlagsgeschichten auf das Verlegen von Dramen aufmerksam macht, ohne dabei den Dramentext hermetisch aufzufassen. Vielmehr zeigt Arnsperger, dass der Theaterverlag unterschiedliche Medien adressiert. Dahingehend ist das Kapitel zu den Dramenanthologien besonders hervorzuheben, da es den Blick auf andere Präsentationsorte jenseits der Bühneninszenierung freigibt.


Wäre zwar ein noch stärkerer Fokus auf nicht realisierte Projekte im Verlagsarchiv wünschenswert, legt die Studie doch auch interessante Spuren für fachgeschichtliche Fragen aus: So wäre es spannend, mehr über die Konzeption theater- und medienwissenschaftlicher Reihen, wie die 1971 geplante Zeitschrift Media/Diskurs, zu erfahren. Denn solche Fallbeispiele befragen die Funktion von Verlagen nicht nur für die Publizität von Drama und Bühne, sondern eben auch für die Theaterwissenschaft.


Literatur:


Amslinger, Tobias: Verlagsautorschaft. Enzensberger und Suhrkamp. Göttingen: Wallstein 2018.


Felsch, Philipp: Der lange Sommer der Theorie. München: C.H. Beck 2015.


Gerlach, Rainer: Die Bedeutung des Suhrkamp Verlags für das Werk von Peter Weiss. St. Ingbert: Röhrig 2005.


Jaspers, Anke: Suhrkamp und DDR. Literaturhistorische, praxeologische und werktheoretische Perspektiven auf ein Verlagsarchiv. Berlin/Boston: De Gruyter 2022.


Misterek, Susanne: Polnische Dramatik in Bühnen- und Buchverlagen der Bundesrepublik Deutschland und der DDR. Wiesbaden: Harrassowitz 2002.


Sprengel, Marja-Christine: Der Lektor und sein Autor. Vergleichende Fallstudien zum Suhrkamp Verlag. Wiesbaden: Harrassowitz 2016.


Watzka, Stefanie: Verborgene Vermittler. Ansätze zu einer Historie der Theateragenten und -verleger. Marburg: Tectum 2006.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2024/1</description>
  </descriptions>
</resource>
