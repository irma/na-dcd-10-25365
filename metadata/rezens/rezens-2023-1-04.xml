<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2023-1-04</identifier>
  <creators>
    <creator>
      <creatorName>Pogoda, Sarah</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Sarah Ralfs: Theatralität der Existenz. Ethik und Ästhetik bei Christoph Schlingensief. </title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2023</publicationYear>
  <dates>
    <date dateType="Submitted">2023-04-14</date>
    <date dateType="Accepted">2023-04-14</date>
    <date dateType="Updated">2023-05-10</date>
    <date dateType="Issued">2023-05-10</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-624-7964</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Christoph Schlingensief nahm die Kunst so ernst wie das Leben. Und so machte er sich für beides haftbar – wie er auch den Zuschauer für beides in Haftung nahm. Erkannte er die einfachste künstlerische Aktion in Bretons Schuss in die Menge, so sah er zugleich, dass auch die Theaterzuschauer "wirklich Pfeile auf einen abschießen, bis man anfängt zu bluten" (Dietz/Dürr 1997, S. 12). Mag seine Kunst immer auch verspielt und experimentell dahergekommen sein, sie meinte es ernst. Wie ernst, das zeigt Sarah Ralfs in ihrer zugleich als Dissertation an der FU Berlin eingereichten Monografie Theatralität der Existenz. Ralfs arbeitet die ethische Dimension einer solchen Ästhetik der Existenz heraus, indem sie aufzeigt, wie Schlingensief künstlerisch die Spezifika von Theater, Film, Oper und Installation auslotet und gegeneinander führt, um einen ästhetischen Erfahrungsraum der dialektischen Verwobenheit und Reziprozität von Leben, Sterben und Tod herzustellen. Dabei mögen existentielle Fragen in Schlingensiefs Schaffen mit seiner Krebsdiagnose unübersehbar und explizit hervortreten. In ihren fünf Kapiteln zeigt Ralfs aber auf, dass diese Deutlichkeit keinen Bruch im Schaffen markiert, sondern eher als Ausreifung einer lebenslang erprobten existentiellen Kunstpraxis zu verstehen ist.


Als substanziell für Schlingensiefs existentiellen Kunstbegriff "negativer Gattungsästhetik" (S. 24) versteht Ralfs die (Neo-)Avantgarden, die Schlingensief in einem "spielwütige[n] Eklektizismus" (S. 17) nach ihren Kunst- und Lebensfunktionen befragt habe. Im Unterschied zu den Avantgarden gehe es ihm aber nicht um ein Kunstwerden des Lebens (und vice versa), sondern um die Genese von Freiheit und Autonomie im abklopfenden Spiel mit der Differenz zwischen Kunst und Leben. Im ersten Kapitel gelingt es Ralfs mit ihrer Detailanalyse des Theaterstücks Atta Atta. Die Kunst ist ausgebrochen (2003), Schlingensiefs Techniken der Bezugnahme nachzuzeichnen. Mittels medialer Verschiebungen, dilettantischen Kopierens, übertriebener Affirmation, ironischem Bekenntnis und ausgestellt fehlerhaften Zitierens (neo-)avantgardistischer Vorgänger*innen, schreibt Schlingensief die historischen Vorläufer*innen und ihre transgressiven Bewegungen nicht einfach fort, er potenziert sie. Statt den gängigen Geniekult der Avantgarde zu reproduzieren, habe Schlingensief ästhetische Strategien entwickelt, Kunst zu einem freiverfügbaren weltimmanenten Verweisnetzwerk umzugestalten, innerhalb dessen Künstler*innen zu Schaltstellen prozessualer Arbeitsstrukturen werden. Die Avantgarde wird zum Ready-made, dem sich Schlingensief zur Reflektion des historischen Stands des ästhetischen Materials bedient. Dabei wird dieser Bestand durch die dilettantische Verfehlung aus kunsthistorischen Wertbeständen befreit und der immer schon in Traditionen stehende Schaffensprozess emanzipiert. Ralfs findet dafür den Neologismus "Avantgardeentzug" (S. 68). Dieser öffne die verweisende und verwiesene Kunst, um neue Funktionen zur Aushandlung von Leben, Sterben und dem Tod zu übernehmen.


Die folgenden Kapitel widmen sich den ästhetischen und medientheoretischen Spezifika der Genre Theater, Film, Oper und Installation, wobei es gerade die Bezogenheit auf ein Publikum ist, die – wenn auch qualitativ und graduell unterschiedlich – bei allen Arbeiten Schlingensiefs immer gegeben ist und aus der Ralfs vor allem die ethische Dimension gewinnt. Wie genau der "theatrale Zug der Kunst als ästhetisches Szenario der ethischen Verwobenheit zwischen Akteur, Bühne und Publikum" (S. 78) zu verstehen ist, legt Ralfs überzeugend im zweiten Kapitel dar. Die Klarheit von Ralfs Analyse in diesem Kapitel ergibt sich nicht nur durch den Rückgriff auf die inzwischen einschlägigen Performativitätsstudien Erika Fischer-Lichtes, sondern auch durch Ralfs eigene kreative Verschiebungen dieser Begrifflichkeiten. Dadurch eröffnen sich neue epistemische Wege, die zum Beispiel hin zu einem nomadischen Theater führen. Das nomadische Theater, das Ralfs bei Schlingensief herausschält, sei künstlerisch und intermedial derart differenziert, dass es kaum mehr unter dem Begriff "Theater" zu fassen sei; zugleich gehe es aber einzig aus der theatralen Aufführungssituation von "Medialität, Performativität, Semiotizität und Ästhetizität" (S. 320) hervor. Nomadisch verorte sich ein solches Theater in unterschiedlichen künstlerischen Feldern und Institutionen als ein immer wieder anderes.


Schlingensiefs Theater sei prozesshaft ausgerichtet und mittels des Zusammenwirkens intermedialer Praktiken von einer dialektischen Dynamik getrieben. Erst aus der darin sich konstituierenden performativen Widersprüchlichkeit bildet sich für Ralfs der Kunstbegriff von Schlingensief heraus, der als "ästhetische[s] Analogon der Existenz" zu verstehen sei (S. 135). In dieser intermedialen Ausweitung der prozesshaften Aufführungssituation öffne Schlingensief einen ästhetischen Erfahrungsraum, um Leben, Sterben und Tod gemeinsam mit dem Publikum zu befragen; ein existenziell ethischer Grundzug ergebe sich also nur qua dieses theaterästhetischen Prinzips. Nur die Theatersituation der Aufführung erlaube es, Krankheit und Sterben in einer vieldeutigen und vielschichtigen ästhetischen Gestalt zu zeigen, wodurch Erfahrungen und Subjektivität der Künstler*innen in intersubjektive gemeinschaftliche Erfahrungsräume der Existenz transzendiert würden.


Ebenso theoretisch versiert ist Ralfs Analyse im dritten Kapitel, in dem die Autorin das Verhältnis von Film und Existenz medientheoretisch perspektiviert. Schlingensiefs filmische Praxis interpretiert Ralfs als Suche danach, "was die Filmtechnik mit dem Leben zu tun haben könnte". (Schlingensief 2012, S. 217, zit. S. 197). Dabei untersucht sie, wie Schlingensief die Spezifika des Films – darunter Indexikalität, Technizität, der technische Apparat, bewegte Bilder und Maschine, Illusion sowie Komposition – diesseits und jenseits von Aufführungen nutzt, um "ein vielschichtiges, ästhetisches und filmspezifisches Gewebe des Lebens, des Sterbens und des Todes sowie einen ästhetischen Erfahrungsraum ihrer Verwobenheit und ihres wechselseitigen Aufeinanderbezogenseins zu schaffen" (S. 202).


Im vierten Kapitel zur Oper betont Ralfs vor allem, wie Schlingensief die unterschiedlichen Künste (Musik, Text, Film, Bühne) als ein Nebeneinander organisiert – und zwar bewusst als Opposition zu Richard Wagners Organisationsprinzip des Übereinander, aus dem sich ein organisches Zusammenwirken zum Gesamtkunstwerk ergebe. Schlingensiefs Nebeneinander aber wirke als ein Gegeneinander, so dass von vorneherein alle organische Überwältigungseinheit unterbunden und stattdessen eine ästhetische Erfahrung des Widersprüchlichen und Unvereinbaren evoziert werde, die – so Ralfs – auch dem Leben eigen sei.


Diese ästhetische Erfahrung von Existenz mündet für Ralfs in der Installation, mit der sie sich im fünften Kapitel ihrer Monografie auseinandersetzt. Die Installation führe ästhetische Spezifika, die sich Schlingensief in Theater, Film und Oper spielerisch erschlossen habe, zusammen und einen Schritt weiter. Die Installationen des Animatographen erlaubten einen Prozess, an dem potenziell alle und alles teilnehmen könne, womit die Idee der nomadischen Ästhetik wieder auftaucht. Die Installation – und das hätten die Ausstellungen der Animatographen auch nach dem Tod Schlingensiefs gezeigt – erlaube es sogar, ganz ohne Künstler*innen auszukommen. Ralfs deutet den Animatographen daher auch als Schlingensiefs Versuch, mittels primär räumlicher und installativer Ordnungsprinzipien "eine strukturelle Hinausrechnung seiner Person" zu finden (S. 277f). Durch Abwesenheit lägen Autorschaft und Erfahrungssteuerung fast ausschließlich beim Publikum. Hier scheint die ethische Dimension der Kunst von Schlingensief erneut auf: eine ethische Dimension, die nicht erst in seiner Beschäftigung mit dem Sterben hervortritt, sondern der Kunstpraxis Schlingensiefs grundsätzlich inhärent ist, ob sie "im Horizont der Existenz" (S. 320) stattfindet oder nicht.


Ralfs Monographie rekonstruiert einen zutiefst sinnhaft stimmigen Kunstbegriff Schlingensiefs, und kommt ganz ohne jeden Verweis auf Schlingensief als Provokateur aus. Stattdessen nutzt sie die 344 Seiten, um sich theatertheoretisch informiert mit Schlingensief zu befassen, der sich und seine Kunst auch stets vehement ernst genommen hat.


Schließen möchte ich in diesem Sinne mit einem glücklich ernsthaften Christoph Schlingensief, der bei der Abschlussveranstaltung seiner Chance 2000 Partei zur Bundestagswahl 1998 in der Volksbühne Berlin in der ihm typischen Verbindung von Ironie und Emanzipation mit Relevanz und Dringlichkeit halb fordert, halb singt: "Und wenn Euch morgen einer fragt, warum habt ihr so viel Spaß gehabt, dann sagt ihm, das liegt daran, dass ihr es ernst meint und weil der Ernst ab sofort hart ist und lebensfroh und weil wir es immer ehrlich gemeint haben. Und ich bin oft gefragt worden: Christoph, wo nimmst du nur all diese Kraft her? Und ich habe immer gesagt: Weil ich an die Sache glaube! Weil ich an die Sache glaube, und die Sache bin ich selber! [...] Und du und du und du, und alle sind es selber. Und alle nehmen es ernst: das Leben, das Leben, das Leben." (Böhler 2020, ab 01:08:36)


Quellen:


Böhler, Bettina: Christoph Schlingensief. In das Schweigen hineinschreien. Filmgalerie 451 2020.


Diez, Georg/Dürr, Anke: "'Tötet Christoph Schlingensief!' Interview mit dem Aktionskünstler über kostenlosen Kaffee, fliegende Autos, Blindheit und Zerstörung". In: KulturSPIEGEL 11/1997, S. 12-15.


Schlingensief, Christoph: Ich weiß, ich war's. Hg. v. Aino Laberenz, Köln: Kiepenheuer &amp; Witsch 2012.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2023/1</description>
  </descriptions>
</resource>
