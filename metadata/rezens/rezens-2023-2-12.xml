<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2023-2-12</identifier>
  <creators>
    <creator>
      <creatorName>Khouloki, Rayd</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Andreas Veits: Narratologie des Bildes. Zum narrativen Potenzial unbewegter Bilder.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2023</publicationYear>
  <dates>
    <date dateType="Submitted">2022-01-13</date>
    <date dateType="Accepted">2023-10-19</date>
    <date dateType="Updated">2023-11-14</date>
    <date dateType="Issued">2023-11-14</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-650-6601</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">Der Untertitel Zum narrativen Potential unbewegter Bilder macht in seiner Bescheidenheit stutzig. Ist das nicht längst ein von der Narratologie zur Genüge erforschtes Feld? Ist es mitnichten, wie diese sehr lohnenswerte Studie von Andreas Veits zu Beginn bereits deutlich macht. Denn wie Veits in der Einleitung ausführt, wurde die Motivation für eine dezidiert narratologische Erforschung unbewegter Bilder trotz deren Bedeutung in der Kulturgeschichte – so könnte zusammenfassend gesagt werden – zwischen der auf Stilgeschichte konzentrierten Kunstwissenschaft und der auf sequenzielle Repräsentationen wie Texte, Filme, Games fokussierten Narratologie zerrieben. So liegt der vorliegenden publizierten Dissertation angesichts des Gegenstands und der Fragestellung schon zwingend ein interdisziplinärer Ansatz zugrunde. Das theoretische Fundament dafür bilden die literaturwissenschaftliche Narratologie sowie die Kognitionswissenschaft und die narrative Psychologie. Damit werden sowohl die Seite des Gegenstands als auch die der Rezeption in den Blick genommen:

"Um die Fragen zu beantworten, inwiefern und auf welche Weise Bilder erzählen können, bedarf es aus Sicht dieser Arbeit stattdessen einer Betrachtung von Eigenschaften der Bildrepräsentation sowie von Prozessen des Bildverstehens als analytische Einheit." (S. 21)

Die Studie klärt zunächst, von welchem Verständnis von Bild ausgegangen wird. Veits entwickelt hier ein dezidiert "exklusives" Bildverständnis, was sich folgerichtig aus der Frage nach der Erzählhaftigkeit unbewegter Bilder ergibt (S. 40). Ein Bild wird hier als "'Fenster' in eine fiktive Welt" (S. 40) verstanden, die von den Rezipierenden als medial vermittelte konstruiert und von der außermedialen Welt unterschieden wird. Diese Welthaftigkeit medialer Repräsentationen konstituiert, so Veits, die Voraussetzung für deren potenzielle Narrativität. Im Folgenden entfaltet Veits dann akribisch, dabei aber immer gut verständlich, die kognitionstheoretischen und narratologischen Grundlagen. Zentrale Begriffe auf der kognitionstheoretischen Seite sind etwa "Schemata" und "Skripte". Die beiden Begriffe beschreiben verschiedene Ebenen der Wissens- und Erfahrungsstrukturierung, die bei der Betrachtung von Bildern aktiviert werden können. Schemata beziehen sich auf die Ebene der Objekterkennung, während Skripte ganze Situationen und Handlungsabläufe umfassen können.

Bei kognitionstheoretischen Argumentationen ist in der Regel die Übertragbarkeit von Ergebnissen der zugrunde liegenden Studien auf künstlerische Artefakte ein Problem, da sich die entsprechenden Experimente auf einfache Reizkonstellationen beziehen und nicht auf fiktionale Welten. Veits aber gelingt die Verbindung verschiedener kognitionstheoretischer Erkenntnisse zur Hypothesenbildung für eine Narratologie des unbewegten Bildes. Dazu trägt zwar auch die relative Reizarmut unbewegter Bilder im Vergleich etwa zum Film bei, was der reduzierten Komplexität im Experiment nahekommt und die Übertragung der kognitionstheoretischen Erkenntnisse nachvollziehbarer macht; vor allem aber überzeugt in diesem Zusammenhang die differenzierte und anschaulich exemplifizierte Darstellung der theoretischen Grundlagen durch den Autor.

Die Brücke zur Narratologie bildet dann ein Kapitel zu verschiedenen Darstellungsmodi von Bildern. Damit sind verschiedene "Bildtypen" (S. 61) gemeint, die dann ausgehend vom Einzelbild noch in Formen von seriellen Bildtypen unterteilt werden. Auf dieser Basis kann im Folgenden jene Annahme in der Narratologie kritisch geprüft werden, der zufolge Zustandsveränderung die Minimalbedingung für Narrativität bilde: Veits arbeitet sich hier an rigiden Positionen ab, die davon ausgehen, dass eine Zustandsveränderung mehr oder weniger direkt dargestellt sein müsse, um Narrativität zu erzeugen, und bringt in diesem Zusammenhang anschließend den Begriff der "Erfahrungshaftigkeit" von Monika Fludernik in Stellung: Fludernik nimmt eine Wendung hin zu den Rezipient*innen vor (S. 82ff), indem sie die Bedingung für das Verstehen medialer Deutungen in den Wissensbeständen der Rezipient*innen begründet sieht. Nach Fludernik entwickelt sich zwischen dem Medium und den Rezipient*innen ein Prozess der Erfahrungshaftigkeit, wenn zwischen den Wissensressourcen der Rezipient*innen und der medialen Darstellung eine Verknüpfung entsteht. Dieses Erfahrungswissen kann auf körperlicher Ebene, der Ebene des Bewusstseins und des Zeiterlebens bestehen (S. 82f). Hier kommen dann auch die Schemata ins Spiel, die als Wissensressourcen bei den Rezipient*innen durch Einzelbilder abgerufen werden können.

Um die bisher entwickelten theoretischen Grundlagen seiner Studie weiter abzusichern, geht Veits noch auf die narrative Psychologie ein. Im Grunde geht es hier um eine grundsätzliche narrative Verfasstheit des Menschen im Austausch mit seiner Umwelt, die auch evolutionär begründet wird. Ob dieses Kapitel in seiner Ausführlichkeit notwendig gewesen wäre, um die Argumentation zu stützen, ist fraglich; interessant ist es aber allemal.

Die Bestimmbarkeit des narrativen Potentials in unbewegten Bildern stellt eine zentrale Herausforderung dar, um die Anwendbarkeit des narratologischen Ansatzes auch für deren Analyse zu gewährleisten. Dem widmet Veits zwei weitere Kapitel. Etwa definiert Veits Formen von Aktivitätsdarstellungen nach den drei Dimensionen "kräftebezogen, räumlich und zeitlich" (S. 145ff). Auch hier überzeugt die Studie durch verschiedene Bildbeispiele mit Anschaulichkeit. Überhaupt werden in der Studie auch immer wieder Typologien aufgeführt, die eine gewisse Übersichtlichkeit gewähren. An dieser Stelle sei außerdem noch der Begriff der Prozessualität erwähnt, den Veits einführt, um die Analyse von unbewegten Bildern auf deren narratives Potential hin auszurichten und den Begriff der Narrativität schließlich zu vermeiden. Veits definiert Prozessualität folgendermaßen:

"Fiktive Situationen, die auf problemorientierte Verhaltensweisen von Figuren verweisen, werden stets als 'prozesshafte' Verläufe rekonstruiert. Die Umsetzungsphase einer Problemlösung beinhaltet eine sukzessiv fortschreitende Kette von Aktivitäten, die zur Problemreduzierung zielgerichtet ausgeführt werden. In Bezug auf die Nachvollziehbarkeit eines auf diese Weise verknüpften Verlaufs von Problemlösungsschritten, die auf ein zu befriedigendes Bedürfnis ausgerichtet sind, spreche ich von Prozessualität." (S. 190)

Wie stark die Darstellung von den Rezipient*innen gewissermaßen aufgefüllt werden muss, damit sie als narrativ wahrgenommen wird, hängt auch vom Bildtypus ab. Bei seriellen Bildern, die mehrere Phasen zeigen, muss eventuell weniger Aufwand betrieben werden als bei einem Einzelbild. Das und den Umstand, dass der Begriff der Narrativität in der Narratologie sehr unterschiedlich verwendet wird, führt Veits als Begründung für seinen Begriff des "narrativen Potentials" als Ziel der Analyse unbewegter Bilder an.

Im letzten Kapitel werden die Kategorien dann anhand von Beispielanalysen erprobt, die durchaus auch den Anspruch haben, eine Typologie zu entwickeln: "Gleichzeitig erlaubt es das Korpus, einen breit gefächerten Überblick über die verschiedenen Strategien zu gewinnen, die von Bilderproduzierenden eingesetzt werden, um narrative Deutungen zu evozieren." (S. 201) Die Analysen zeigen dann auch überzeugend die Anwendbarkeit der Kategorien auf.

Die Anschlussfähigkeit der Analysen an weitere Forschungsperspektiven bilden zudem eine der großen Qualitäten von Veits' Buch, wobei nicht ganz klar wird, warum er diese stellenweise nicht stärker hervorgehoben hat. So werden bei der Analyse des Gemäldes Die Wahrsagerin konsequent das Sujet und die Interaktion der Figuren mit den Kategorien der Aktivitätsdarstellung analysiert. Auf die auffällige Mimik der im Zentrum stehenden männlichen Figur geht der Autor allerdings nicht ein. Auch bleibt der Umstand, dass die einzige männliche Figur von lauter Frauen umringt und offenbar bestohlen wird, unerwähnt – was aus heutiger, genderpolitisch sensibilisierter Perspektive verstärkt Fragen aufwirft.

Die analysierten Bilder sind ausreichend groß in Farbe abgedruckt und den Analysen jeweils vorangestellt, wodurch ein lästiges Blättern weitgehend entfällt. Das Buch ist insgesamt ansprechend gestaltet, das Schriftbild gut lesbar und der Aufbau übersichtlich. Das gut strukturierte Inhaltsverzeichnis ermöglicht es, auch zentrale Begriffe schnell zu finden, was den fehlenden Index zwar nicht komplett ersetzen kann, aber immerhin eine Orientierungshilfe darstellt. Mit der Narratologie des Bildes hat Andreas Veits wichtige Grundlagenforschung zum Verständnis unbewegter Bilder und deren Funktionsweisen geleistet. Darüber hinaus bietet die Studie ein effektives und anschlussfähiges Instrumentarium zur Analyse narrativer Potentiale unbewegter Bilder, das auch zahlreiche Anregungen für die Auseinandersetzung mit bewegten Bildern evoziert.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2023/2</description>
  </descriptions>
</resource>
