<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2021-2-09</identifier>
  <creators>
    <creator>
      <creatorName>Gönitzer, Daniel</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Daniel Mourenza: Walter Benjamin and the Aesthetics of Film.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2021-03-10</date>
    <date dateType="Accepted">2021-10-22</date>
    <date dateType="Updated">2021-11-30</date>
    <date dateType="Issued">2021-11-30</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-533-5966</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Walter Benjamin kann zweifellos als einer der bedeutendsten Kunst- und Medientheoretiker des 20. Jahrhunderts bezeichnet werden. Dennoch hat seine Beschäftigung mit dem Film überraschenderweise bislang nur wenig im Zentrum gestanden. Mourenzas Walter Benjamin and the Aesthetics of Film ist die erste eigenständige Monografie, die gänzlich Benjamins Auseinandersetzung mit dem Film gewidmet ist. Mourenzas Bezugspunkt bilden die Arbeiten von Miriam Hansen, die bereits Ende der 1980er-Jahre einige Aufsätze zum Thema publiziert hat (vgl. Hansen 1987, 1993, 1999). Im deutschsprachigen Raum veröffentlichte etwa Andreas Jacke 2013 Traumpassagen. Eine Filmtheorie mit Walter Benjamin, doch anstatt Benjamins Auseinandersetzung mit dem Film zu zentrieren, versucht Jacke ausgehend von einzelnen Begriffen wie 'Wahrheit', 'Schock', 'Kontemplation' oder 'Aura' eine eigene Filmtheorie zu entwickeln. Ebenfalls erwähnt werden muss der 2018 von Christian Schulte, Birgit Haberpeuntner, Valentin Mertes und Veronika Schweigl herausgegebene Sammelband Walter Benjamin und das Kino, der eine Pionierarbeit in der deutschsprachigen Benjamin-Forschung darstellt. In diesem gelang zum ersten Mal eine Ausdifferenzierung der Benjaminschen Filmtheorie, wobei dort sowohl Benjamins Auseinandersetzung mit Film und Kino Gegenstand der Untersuchungen ist, als auch dessen Wirkungs- und Rezeptionsgeschichte. 


Mourenza weist nun in seiner Arbeit auf die terminologischen Schwierigkeiten hin, wenn es darum geht, Benjamins Beschäftigung mit dem Medium Film zu beschreiben. Denn selbst die großen Bereiche der Filmtheorie, Medientheorie, -kritik oder -philosophie werden der Vielfältigkeit von Benjamins Überlegungen zum Film nicht gerecht. Mourenza bezeichnet diese als "Aesthetics of Film".  


Sein Fokus liegt auf Benjamins undogmatischer Analyse des Films als Massenmedium, den kritischen Reflexionen zum Mensch-Technik-Natur-Verhältnis und Benjamins Konzept des "Anthropologischen Materialismus". Dieses verbindet romantische Visionen einer kommenden Gesellschaft mit wissenschaftlich-materialistischen Erkenntnissen. Basis für diesen experimentellen Zugang, der als Gegenbewegung zu einem dogmatischen, historischen Materialismus diente, bilden die französischen Utopie-Entwürfe des 19. Jahrhunderts (etwa jene von Saint-Simon oder Charles Fourier) sowie die naturwissenschaftlichen Ansichten Isaac Newtons.


Im ersten Kapitel des Buches, das den Titel "Anthropological Materialism and the Aesthetics of Film" trägt, diskutiert Mourenza, inwiefern Benjamins Interesse am Film vor allem dem Umstand geschuldet ist, dass der Film das Verhältnis der Menschen zur Technik verändern und verbessern könnte. Das enorme Potenzial, das Benjamin der (Film-)Technik zuschreibt, bezieht sich auf dessen Fähigkeit zur "Innervation" und damit zur Schaffung neuer Kollektiverfahrungen. Innervation im Benjaminschen Sinne meint eine neurophysiologische Verschränkung mit der technologischen Apparatur (vgl. Hansen 1999, S. 317). Benjamin entwendet diesen Begriff aus der Biologie und setzt ihn mit jenen von 'Revolution', 'Film' und 'Technik' in Beziehung. Er schreibt dem Film des frühen 20. Jahrhunderts die Aufgabe zu, dem Kinopublikum einen reflektierten Umgang mit der neuen technifizierten Realität zu ermöglichen.


Im zweiten Kapitel "Soviet Film: The Giant Laboratory of Technological Innervation" steht Benjamins Kontakt mit dem sowjetischen Film im Zentrum. Mourenza behandelt hier neben Benjamins Ausführungen zu Revolutionsfilmen wie Ein Sechstel der Erde (1926) und Panzerkreuzer Potemkin (1926) sein Verhältnis zur sowjetischen Kultur und Politik allgemein. Mit großer Sorgfalt führt Mourenza nicht nur sämtliche Filme an, die Benjamin bei seinem Aufenthalt in Moskau im Winter 1926/27 nachweislich gesehen hat, sondern behandelt auch den Kontext, in dem er dies tat. Benjamins Kritik an den sowjetischen Filmproduktionen bestehe darin, dass es dort zwar ein großes Interesse an der künstlerischen Auseinandersetzung mit Technik gäbe, diese jedoch zu einseitig und unkritisch sei. Dem russischen Film fehle die kritische Distanz zur Technik, stattdessen werde der technische Fortschritt widerspruchslos abgefeiert. Dies hänge mit dem großen Versäumnis zusammen, dass Benjamin zufolge etwa der amerikanische Groteskfilm von der sowjetischen Filmszene abgelehnt bzw. ignoriert wurde. Mourenza kritisiert Benjamin an dieser Stelle und ergänzt, dass die sowjetische Avantgarde etwa die Filme von Charlie Chaplin sehr wohl wahrnahm und euphorisch rezipierte: Viktor Šklovskij, El Lissitzky, Ilya Ehrenburg und Sergej Eisenstein setzten sich äußerst intensiv mit diesen auseinander. Benjamin hingegen habe es versäumt, so Mourenza weiter, zwischen der staatlichen Filmpolitik und den einzelnen Filmemacher*innen und Künstler*innen, die das Potenzial der amerikanischen Grotesk- und Zeichentrickfilme durchaus erkannten, zu differenzieren.


In Kapitel 3 "Film and the Aesthetics of German Fascism" bespricht Mourenza Benjamins Auseinandersetzung mit dem Film im Kontext der deutschen Filmlandschaft der 1920er- und 1930er-Jahre. Zwei Filme, denen Mourenza besondere Aufmerksamkeit schenkt, sind Fritz Langs Metropolis von 1927 und Leni Riefenstahls Triumph des Willens von 1935. Beide Filme dienen als Anhaltspunkte, um fundamentale Bausteine von Benjamins Theoriegebäude zu besprechen. Es werden etwa die Ästhetisierung von Politik, die Politisierung der Kunst und der Technik, Benjamins Überlegungen zur Masse und zum Kollektiv in diesem Kapitel behandelt.


Kapitel 4 "Charlie Chaplin: The Return of the Allegorical Mode in Modernity" widmet sich dann Benjamins Rezeption von Chaplin als Kritiker der kapitalistischen Entfremdung und Erneuerer der Allegorie in der Moderne. Dabei stellt Mourenza die Begriffe 'Allegorie', 'Fragment' und 'Gestus' ins Zentrum. Er knüpft hier erneut an die Arbeiten von Miriam Hansen (2011, S. 47f.) an. Chaplin übersetze, so Mourenza, die Fragmentierung der Wahrnehmung und des Körpers in seine eigene gestische Performance. Ein treffendes Beispiel für dieses Vorgehen ist die Fabrikszene aus Modern Times (1936). In der künstlerischen Verarbeitung der Fragmentierung des und Abstraktion vom eigenen Körper zeigt sich die Nähe Chaplins zu Franz Kafka. Beiden gelinge es, die Selbstentfremdung des Menschen zu veranschaulichen. Genau dieses Vorgehen nennen Mourenza und Hansen in Anlehnung an Benjamin 'allegorisch'.


Im fünften Kapitel "Mickey Mouse: Utopian and Barbarian" richtet Mourenza schließlich sein Augenmerk auf einen durchaus unterschätzten Untersuchungsgegenstand: Benjamins Beschäftigung mit der Figur Mickey Mouse (Benjamin verwendet stets eigentümliche Schreibweisen wie "Micki Maus" oder "Mickey Maus"). Hier folgt das Buch auch Esther Leslie, die mit ihrer Monografie Hollywood Flatlands (2002) eine Pionierarbeit auf diesem Gebiet geleistet hat. Mourenza legt in seiner Analyse der Mickey-Maus-Rezeption Benjamins im Unterschied zu Hansen den Fokus auf den von Benjamin hochgeschätzten Wiener Satiriker Karl Kraus. Benjamins Ausführungen zur Mickey Maus und zu Kraus sind in derselben Zeit, Anfang der 1930er-Jahre, entstanden und es finden sich etliche inhaltliche Parallelen. Diese betreffen die destruktiven und dadurch Platz schaffenden Funktionen beider, die Benjamin dazu veranlassen, sie dem "positiven Barbarentum" zuzuordnen, ein Konzept welches er in seinem Aufsatz "Erfahrung und Armut" von 1933 entwickelt. Im Unterschied zu den sowjetischen Revolutionsfilmen ironisieren die Mickey-Maus-Filme die Technologie, die Anfang der 1930er-Jahre die Menschen teilweise noch in Angst und Schrecken versetzte. Gleichzeitig verstehen diese Filme die transformativen Momente der Technik. Das utopische Element der grotesken Zeichentrickwelt liegt Benjamin zufolge in der Verlebendigung des Mechanischen und der Technisierung des vermeintlich Natürlichen.


In der Konklusion "Benjamin’s Belated Aktualität" fragt Mourenza abschließend, welche Möglichkeiten Benjamins Texte uns heute noch für die Analyse von Film bieten können. Diese Frage drängt sich umso mehr auf, da heute das Verhältnis zwischen Mensch und Technik noch um einiges enger geworden ist, als es zu Benjamins Zeiten der Fall war.   


Die einzelnen Kapitel, die hier chronologisch besprochen sind, sind dabei ebenso gut achronologisch und einzeln lesbar – so entsteht der Eindruck, das Buch fungiere in gewisser Weise als Aufsatzsammlung. Mit der vorliegenden Monografie hat Mourenza jedenfalls eine beeindruckende Studie zu Benjamins Auseinandersetzung mit Film vorgelegt. Es ist es ihm gelungen, an Miriam Hansens 2011 posthum veröffentlichte, richtungsweisende Studie zur Kritischen Theorie und Film Cinema and Experience anzuschließen und diese mit zentralen Überlegungen zu Benjamins Begriff der Innervation und dem damit zusammenhängen Konzept des anthropologischen Materialismus zu ergänzen.


Literatur:


Hansen, Miriam Bratu: "Benjamin and Cinema: Not a One-Way-Street". In: Critical Inquiry, 25, 1999, S. 306-343.


Hansen, Miriam Bratu: Cinema and Experience. Siegfried Kracauer, Walter Benjamin, and Theodor W. Adorno. Berkeley/Los Angeles/London: University of California Press 2011.


Hansen, Miriam Bratu: “Benjamin, Cinema, and Experience: 'The Blue Flower in the Land of Technology'.” In: New German Critique, Nr. 40, 1987, S. 179-224.


Hansen, Miriam Bratu "Of Mice and Ducks: Benjamin and Adorno on Disney". In: South Atlantic Quarterly 92, 1993, S. 27-61.


Jacke, Andreas: Traumpassagen. Eine Filmtheorie mit Walter Benjamin. Würzburg: Königshausen &amp; Neumann 2013.


Leslie, Esther: Hollywood Flatlands. Animation, Critical Theory and the Avant-Garde. London/New York: Verso 2002.


Schulte, Christian/Haberpeutner, Birgit/Mertes, Valentin/Schweigel, Veronika (Hg.): Walter Benjamin und das Kino. In: Maske und Kothurn. Internationale Beiträge zur Theater-, Film- und Medienwissenschaft, 60. Jg. Heft 3-4. Wien: Böhlau 2018.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2021/2</description>
  </descriptions>
</resource>
