<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2020-2-16</identifier>
  <creators>
    <creator>
      <creatorName>Mücke, Laura Katharina</creatorName>
    </creator>
    <creator>
      <creatorName>Dörre, Robert</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Daniel Rode/Martin Stern (Hg.): Self-Tracking, Selfies, Tinder und Co. Konstellationen von Körper, Medien und Selbst in der Gegenwart.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2020</publicationYear>
  <dates>
    <date dateType="Submitted">2020-05-07</date>
    <date dateType="Accepted">2020-11-12</date>
    <date dateType="Updated">2020-11-18</date>
    <date dateType="Issued">2020-11-18</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-347-3614</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Das Cover des Sammelbandes Self-Tracking, Selfies, Tinder und Co. ziert die Darstellung eines ausgestreckten Arms – ein Smartphone in der Hand haltend, Bildschirm und Frontkamera auf das Selbst gerichtet. In diesem reflexiven Bild kommt der im Untertitel des Bandes explizierte Anspruch, "Konstellationen von Körper, Medien und Selbst" zu untersuchen, zum Ausdruck. Hierfür stecken die Herausgeber Martin Stern und Daniel Rode in der Einleitung ein weites begrifflich-theoretisches Feld ab, innerhalb dessen sich die Autor*innen des Bandes, der im Umfeld der Soziologie, Pädagogik und Sportwissenschaft entstanden ist, für ihre jeweilige Beschäftigung mit "Self-Tracking, Selfies, Tinder und Co." ebenso weiträumig verorten.  
&#13;

Die in diesen Untersuchungen zentralen Begriffe Körper, Medien, Selbst und Konstellationen haben uns als Rezensent*innen indes dazu inspiriert, über sie und damit den Sammelband selbst in den Dialog zu treten.  Aus dem Gespräch heraus versuchen wir, den Band auf seine disziplinäre Anschlussfähigkeit zu untersuchen und unser Verständnis der Beiträge dialogisch zu entfalten. Wie das Cover des Bandes richten wir in unserer Rezension also auch den Blick auf uns selbst, oder besser: unser Fach, die Medienwissenschaft. Laura Katharina Mücke spricht dabei aus Sicht der ersten beiden Segmente des Sammelbands, Robert Dörres Ausführungen nehmen auf die letzten beiden Abschnitte Bezug – eine Aufteilung, die sich nicht zuletzt aus den jeweiligen Forschungsinteressen ergeben hat, die im rezensierenden Blick selbstredend mitklingen. Das entstandene Gespräch befragt die einzelnen Beiträge des Sammelbands also aus einer dezidiert medienwissenschaftlichen Perspektive und versucht damit auch, akademische Disziplinen miteinander in den Dialog zu bringen.
&#13;

 
&#13;

KÖRPER

&#13;

Laura: Der Band begreift den Körper gleichsam als Agenten und Symptom von Subjektwerdung in medialen Konstellationen. Eine kritische Befragung des Begriffs des Körpers steht dabei aber, (wie ich es sehe), allerdings nur selten im Vordergrund. Dieser wird lediglich etwa über den soziologischen Leibbegriff Robert Gugutzers, der selbst 2016 einen Band zum Self-Tracking mitherausgegeben hatte und der den Leib als Variable zwischenmenschlicher Interaktion beschreibt, sowie über leibphänomenologische Zugriffe etwa Helmuth Plessners, Pierre Bourdieus oder Maurice Merleau-Pontys grundiert.
&#13;

Robert: Anschaulich wird dabei aber schon, wie der Band den Körper zugleich als Werkzeug wie als Bühne von Subjektivierung versteht. Beispielsweise Thomas Dambergers Überlegungen zum "Self-Tracking als medienpädagogische Herausforderung" nehmen den Körper als Medium in den Blick, formulieren von dort aus allerdings vornehmlich Bedenken: "[es] besteht die Gefahr einer Identifikation des Selbst mit dem, was datenmäßig erfasst und zahlenmäßig abgebildet wird. Der Körper nimmt dann lediglich und zugleich ausschließlich die Rolle eines Mediums an, in dem das ist und wirkt, was der Mensch eigentlich zu sein scheint" (S. 217). Über diese  kausale Logik hinaus gewinnt in den Beiträgen von Clarissa Schär, Benjamin Zander und Daniel Rode ein Konzept an Gewicht, das vom letzteren als "Körperlichkeit des Sozialen" (S. 154) beschrieben wird und die somatisch-grundierten Subjektivierungspraktiken innerhalb sozialer Lebenswelten verortet. Ob Tracking oder Bilderproduktion, die beschriebenen Medienpraktiken werden dabei immer auch als Verwirklichung somatischer Optimierungsimperative gelesen.
&#13;

Laura: Allerdings lässt der Band zwischen den aufgezeigten Optimierungsimperativen insbesondere eine genderpolitische Dimensionierung missen, die sich in formeller Hinsicht übrigens auch in der ungleichen Besetzung der Autor*innen und teilweise im Fehlen von genderneutraler Sprache verdeutlicht. Gerade im Hinblick auf das im Buch angesprochene Thema des sportlichen, sich selbst verhandelnden und identitätspolitisierenden Selbst (in den Beiträgen von Martin Stern zur Individualisierungs-Optik beim Skaten als "Passungsverhältnis" (S. 47), in Sascha Oswalds These, dass Körper und Geist beim Tinder-Wischen getrennt agieren (S. 72), und in Karolin Eva Kapplers, Eryk Nojis und Uwe Vormbuschs Fallstudien zu zwei männlichen Probanden, die die Apps zum Sport und zu Stressbewältigung nutzen) ist es verwunderlich, dass die wichtige Infragestellung von  biologischem und sozialem Geschlecht keinerlei Berücksichtigung findet. Obwohl dabei etwa historische Perspektiven auf Selbstvermessungspraktiken wie etwa das Tagebuch zum Tragen kommen, wird die subversive Funktion der Körper lediglich in jenen Texten besprochen, in denen Körperlichkeit in ihrer sozialen Verfasstheit thematisiert wird: etwa in der Erwähnung einer vorwiegend weiblich besetzten Bildkultur von Selbstvermessungs-Apps wie Fitbit oder Misfit im Text von Franz Krämer und Denise Klinge und insbesondere in der soziohistorischen Befragung des Zusammenhangs von Selbstvermessungs-Technologien und Körper in Bezug auf Ernährung als Praxis der Selbstreflexion im Artikel von Gerrit Fröhlich und Daniel Kofahl.
&#13;

Robert: Im zweiten Teil leistet das besonders der Artikel von Clarissa Schär. Dieser verortet die Selbstdokumentation von Jugendlichen in sozialen Medien ostentativ zwischen der Erfüllung hegemonialer Körpernormen und ihrer subversiven Umgehung, wodurch nicht zuletzt die damit einhergehenden Aushandlungsprozesse fokussiert werden (S. 188ff). Medienpraktiken wie das Self-Tracking oder die fotografische Selbstdokumentation ziehen so allerdings Fragen nach sich, denen lediglich mit einer Aufwertung des Medienbegriffes nachzukommen wäre.
&#13;

 
&#13;

MEDIEN
&#13;

Laura: Stimmt, der definitorische Zugriff auf Medien wird nämlich lediglich in der Einleitung der Herausgeber aufgegriffen, wo ein deterministisches Medienverständnis von medialen Repräsentationen als Manipulationen und ein funktionalistisches Medienverständnis vom Medium als Werkzeug und komplexeren Wirkverhältnissen unterschieden wird (vgl. S. 23). Dieses Spektrum wird jedoch nur von wenigen Artikeln ausgereizt bzw. überhaupt thematisiert.
&#13;

Robert: Ich kann diesen Eindruck nur bekräftigen, die Ästhetik, Geschichte, Theorie oder Kultur digitaler Medien scheint einfach nicht im Interessensbereich der Autor*innen zu liegen. Zumindest das von Dir zitierte funktionalistische Medienverständnis wird aber hin und wieder relevant, wenn auch erneut lediglich in Bezug auf den Körper. Vor allem die Beiträge von Thomas Damberger und Simon Schaupp verdeutlichen dahingehend, wie die quantified self (QS)-Bewegung den eigenen Körper mediatisiert und zum Ziel wie zum Werkzeug ihres Selbstverbesserungswillens macht.
&#13;

Laura:  Aus meiner Lektüre kristallisiert sich eher der Eindruck eines vermehrt deterministischen Verständnisses heraus. Häufig werden Medien in diesem Sinne lediglich als Verrechnungsinstrumente begriffen, die den von Dir angesprochenen Selbstverbesserungswillen automatisch hervorrufen würden.
&#13;

Robert: Einem solchen deterministischen Medienverständnis unterliegen aus meiner Sicht auch die verschiedenen Bemerkungen zu körperlichen Repräsentationen. So geht es anders als proklamiert nur selten um Aspekte einer "medienvermittelten Körperlichkeit" (S. 184), sondern viel häufiger um medienvermittelte "Körperbilder" (S. 186). Dabei hätte die Frage danach, ob neue Medientechniken auch neuen Formen von Körperlichkeit und Körpererfahrung sekundieren können, durchaus instruktiv sein können.
&#13;

Laura: Diese einseitigen Auslegungen medialer Wirkzusammenhänge resultieren ganz wesentlich aus der fehlenden Beschäftigung mit kontemporärer Medientheorie. Während der Artikel von Gerrit Fröhlich und Daniel Kofahl zum diet-Tracking mittels des Terminus "mediale[r] Biografiegeneratoren" (S. 128) immerhin die Verwobenheit von Medium und Selbst weiter zu spezifizieren sucht, findet sich im Beitrag von Franz Krämer und Denise Klinge, die Bilder als "Hilfsmittel der Orientierung und Verständigung in sozialen Situationen" (S. 108) verstehen, beispielsweise keinerlei dezidierte Bezugnahme auf einen fachlich verankerten Medienbegriff. Dabei wären ihre Beschreibungen von Technologien des Selbst für mich insbesondere anschlussfähig an aktuelle medienwissenschaftliche Perspektiven, wie sie etwa im Sammelband Locative Media Medialität und Räumlichkeit – Multidisziplinäre Perspektiven zur Verortung der Medien (2013) abgebildet sind, der über mediale Räume und dividuelle Verortungen spricht, oder in Smartphone Ästhetik. Zur Philosophie und Gestaltung mobiler Medien (2018), weil diese Bände das Nutzungssubjekt – das Selbst – explizit aus dem Medialen heraus denken.
&#13;

 
&#13;

SELBST
&#13;

Robert: À propos, die Beiträge des Bandes zeigen ein dezidiertes Interesse an einem "kalkulierten Wissen" über das Selbst, das sich im Befragen der technologisch bedingten "Arbeit am Selbst" (S. 19) manifestiert. Hierbei spielt die Dimension der Selbsterkenntnis eine zentrale Rolle, wobei es einen klaren Tenor zu geben scheint: Die kalkulierende Selbstevaluation führe den meisten Autor*innen zufolge gerade nicht zu einem elaborierten Selbstverständnis.
&#13;

Laura: Hast du ein Beispiel dafür?
&#13;

Robert: Thomas Damberger setzt beispielsweise bei der Ideengeschichte der Selbsterkenntnis an und differenziert zwei paradigmatische Grundhaltungen, die er aus Philosophien der griechischen und römischen Antike herleitet. Während im ersten Fall das Selbst in dialogischer Anschauung zur Selbsterkenntnis gelangt, bleibt die zweite Variante dem Autor zu Folge bei einer erkenntnisarmen Beschreibung stehen. Medientechnische Praktiken der Selbstvermessung seien nun der Mentalität der römischen Antike näher, in der eine Beschäftigung mit dem Selbst vor allem über deskriptive und aufzeichnende Verfahren erfolgte, die jedoch über das Selbst nichts zu entbergen vermögen (vgl. 210ff). Ob dieser Dualismus eine wohlbegründete Differenz veranschaulicht oder lediglich einen medienkulturkritischen Vorwand darstellt, bleibt für mich trotz Dammbergers intensiver philosophischer Auseinandersetzung fraglich.
&#13;

Laura: Eine weitere identitätspolitische Sicht weist das Buch in der durch Tracking-Apps etablierten Annahme des Selbst als unternehmerisches, sich selbst optimierendes auf. In diesem Sinne wird die Emanzipation des Selbst durch Prozesse der Reflexion betont: Etwa in der Relationalität, die sich als das Erproben von Passungsverhältnissen, der Versicherung in einer unsicheren Welt, Selbstbeherrschung und Identitätssuche (vgl. S. 129) veräußert, wird mit Michel Foucault das Selbst schlussendlich eher implizit zwischen Ermächtigung und Unterwerfung lokalisiert.
&#13;

Robert: Immer wieder fällt diese Bewertung jedoch zu Gunsten der Unterwerfung aus: So wird z. B. im Beitrag von Simon Schaupp Selbstevaluation im Zuge einer "materialistischen Dispositivanalyse" (S. 227) sogar als Teil eines übergeordneten kybernetischen Paradigmas gedeutet. Was dabei – außer am Rande bei Martin Rode – keine Erwähnung findet, ist die Tatsache, dass man die Praktiken der QS-Bewegung nicht nur als Verlust einer reflexiven Fähigkeit der Selbsterkenntnis oder als Einspannung in ein kybernetisches System deuten kann, sondern dadurch auch – wie in der Einleitung im Sinne Nelson Goodmans beschrieben – neue "Weisen der Welterzeugung" (S. 16) entstehen können und sich das Selbst in diesen neuen Welten neu erfahren kann.
&#13;

Laura: Ergänzen würde ich dahingehend aber schon, dass die Beziehung zwischen Medium, Körper und Selbst in dem Sammelband als eine dynamische und wechselseitige erscheint, die jedenfalls in vielen der Fallstudien der Artikel als solche zumindest erwähnt wird. Dafür scheint insbesondere der in der Einleitung aufgerufene Begriff der Konstellationen interessant.
&#13;

 
&#13;

KONSTELLATIONEN
&#13;

Robert: An das Denken in Konstellationen statt Entitäten wird in der Einleitung zwar vielversprechend herangeführt, der Anspruch der Autor*innen, die verschiedenen Begriffe und Dimensionen in relationalen Zusammenhängen zu denken, gelingt in den Artikeln jedoch nur mit Einschränkungen. So reduziert sich der theoretisch durchaus elaborierte Ansatz in den einzelnen Beiträgen zumeist auf bilaterale Gefüge von z. B. Körper und Selbst oder Selbst und Medium. Von der holistischen Geste der Einleitung bleibt daher häufig nur ein Fingerzeig.
&#13;

Laura: Da würde ich Dir nur teilweise zustimmen. Denn neben den wenig tiefen Texten, die unnötigerweise allgemeingültige Diagnostiken zum digitalen Zeitalter proklamieren möchten – so wie Karolin Eva Kappler, Eryl Noji und Uwe Vormbusch etwa die zeitgenössische Überwachungsgesellschaft, einen Verlust stabiler Selbstbilder und eine grundlegende Unsicherheit über den Wert der Dinge beklagen (vgl. S. 84) – gibt es immerhin Texte, die den vorübergehenden Charakter solcher Konstellationen betonen und Konstellationen auch jenseits bilateraler Konstrukte denken. Etwa der Beitrag von Martin Stern weist individuelle Performanzen von Skating-Posen stets als ein wechselseitiges Verhältnis in potentialis aus, das sich erst im Zusammenspiel von sportlicher Praxis, instantaner Videoaufnahme des moves und Weitertragen des Video-Materials in der community etabliert, das letztlich die Sportart selbst erst formen würde.
&#13;

Robert: Mein Eindruck ist jedenfalls, dass sich diese fehlende Relationalität auch in der asymmetrischen Gewichtung von Theorie und Analyse manifestiert. Die Darstellungen verharren häufig im Abstrakten und immer wieder verliert der theoretische Höhenflug die phänomenale Basis, von der aus gestartet wurde, aus dem Blick. So muss sich beispielsweise die Tauglichkeit des vielversprechenden Analysemodells, das Daniel Rode in seinem Beitrag entwickelt (vgl. S. 163ff), nie an einem Beispiel bewähren. Die wohltuende Ausnahme bildet hierbei der Beitrag von Benjamin Zander, der seine methodischen Überlegungen zu Gruppendiskussionen immer wieder an Fallbeispiele aus seiner eigenen Forschung zu "sport- und körperbezogene[n] Orientierungen von Jugendlichen" (S. 249)  zurückbindet und dadurch den  Mehrwert seiner Anregungen schlüssig plausibilisieren kann. Letztlich ist seine Aufmerksamkeit dabei aber auch eher 'Verhältnissen' als Konstellationen gewidmet.
&#13;

Laura: Zumindest in puncto Fallstudien verspricht der klar sozialwissenschaftlich grundierte Band tatsächlich einen Zugewinn: Viele der Artikel greifen auf Datenmaterial aus qualitativen Studien (oder Zwischenständen daraus) zurück und bieten somit eine konkrete Anwendung: So befragen Kappler, Noji und Vormbusch Menschen auf die konkrete Auswirkungen, die Vermessungstechnologien auf sie nehmen und Oswald befragt Personen auf das Immersionspotenzial ihrer Tinder-Nutzung, wodurch den Nutzer*innen zumindest vorübergehend eine Stimme gegeben wird. Gleichsam scheint der Rückbezug der Studien auf kulturwissenschaftliche bzw. -theoretische Fragestellungen, wie Du schon sagst, letztlich nur marginal repräsentiert.
&#13;

 
&#13;

SCHLUSS
&#13;

Zusammenfassend ist also festzuhalten, dass im medienwissenschaftlichen Blick auf den sport- und sozialwissenschaftlichen Band deutlich wird, dass hier nicht etwa, wie eingangs ausgewiesen, die facheigenen Gegenstände als "Beobachtungskategorie[n]" (S. 153) fungierten, um den tendenziell kulturkritischen Befürchtungen ein Gegenangebot machen zu können. Vielmehr hat der Band, der vom Spielerischen als mediale Praktik ausging und in seiner Einleitung eine thematische wie interdisziplinär anschlussfähige Basis eröffnet hat, einen ernsthaften Versuch der Bindung an die Diskurse seiner gewählten Gegenstände – die Medien – eher verpasst. Statt einer Heterogenität und Vielseitigkeit der Medialität, die sich im Titel über die Aufzählung "Self-Tracking, Selfies, Tinder und Co." vielversprechend andeutete, beschäftigen sich die allermeisten Beiträge des Bandes ausschließlich mit Self-Tracking, wodurch die kulturellen Gefüge zwischen verschiedenen Praktiken leider ebenfalls aus dem Fokus geraten. Statt diese Relationalitäten und Konstellationen ernst zu nehmen, wird in den meisten Beiträgen zusätzlich die unterkomplexe Annahme von determinierenden – manipulierenden – Medien weitergereicht, statt von reziproken, ephemeren Verhältnissen auszugehen, die das digitale Zeitalter zuhauf prägen. Dass Publikationen dieser Art jedoch über ihre Sichtbarkeit im geisteswissenschaftlichen Fachgefüge, das sich nicht zuletzt auch durch das Publizieren in denselben Verlagen (hier: transcript) ergibt, zu interdisziplinären Diskussionen anregen (und auch anregen sollen!), sollte die kontroverse, dialogische Form unserer Rezension nachdrücklich betonen. Allein im interdisziplinären Dialog und im gegenseitigen Verständlichmachen fachspezifischer Anliegen, so denken wir, lassen sich die Schwierigkeiten lösen, die entstehen, wenn sich andere Fachdisziplinen selbstverständlicherweise medienwissenschaftlicher Gegenstände bedienen.
&#13;

 
&#13;

Literatur: 
&#13;

Duttweiler, Stefanie/Gugutzer, Robert/Passoth, Jan-Hendrik/Strübing, Jörg: Leben nach Zahlen. Self-Tracking als Optimierunsprojekt? Bielefeld: transcript 2016.
&#13;

Buschauer, Regine/Willis, Katharine S.: Locative Media. Medialität und Räumlichkeit – Multidisziplinäre Perspektiven zur Verortung der Medien / Multidisciplinary Perspectives on Media and Locality. Bielefeld: transcript 2013.
&#13;

Ruf, Oliver: Smartphone-Ästhetik. Zur Philosophie und Gestaltung mobiler Medien. Bielefeld: transcript 2018.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2020/2</description>
  </descriptions>
</resource>
