<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2022-2-05</identifier>
  <creators>
    <creator>
      <creatorName>Dammel, Jan</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Azadeh Sharifi/Lisa Skwirblies (Hg.): Theaterwissenschaft postkolonial/dekolonial. Eine kritische Bestandsaufnahme.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2022</publicationYear>
  <dates>
    <date dateType="Submitted">2022-07-21</date>
    <date dateType="Accepted">2022-11-07</date>
    <date dateType="Updated">2022-11-16</date>
    <date dateType="Issued">2022-11-16</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-609-7459</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Es ist keine Übertreibung, den Band Theaterwissenschaft postkolonial/dekolonial und sein Erscheinen ein veritables Ereignis in der deutschsprachigen Theaterwissenschaft zu nennen.


Mit der Frage, was postkoloniale und dekoloniale Theorien und Methoden für die Theaterwissenschaft und allgemein das Beforschen und Lehren von Theater bedeuten, haben sich Azadeh Sharifi und Lisa Skwirblies mit den Beitragenden einer Frage angenommen, die in unserem Feld im Unterschied zu anderen Geisteswissenschaften viel zu lange abgewehrt und marginalisiert wurde. Die Herausgeberinnen weisen eingangs auf diese Dynamiken und ihre diesbezügliche "Unruhe und Ernüchterung" (S. 12) hin.  


Neben der Einleitung umfasst der knapp 300 Seiten starke Band ganze achtzehn Beiträge, aufgeteilt in die sechs Abschnitte "Dekolonisierungsversuche in der Theaterwissenschaft", "Returning the Gaze", "Allyship und Selbstpositionierung", "Kulturpolitische und aktivistische Positionen", "Rückblicke und Standortbestimmungen" sowie "Ausblick". Es sprechen aber weitaus mehr als achtzehn Stimmen, denn über die Hälfte der Beiträge sind Gespräche, basieren auf solchen oder finden Formen der Mehr-Autor*innenschaft; auch dies ein Ausdruck eines "Wunsch[s] nach anderen Formen des akademischen Arbeitens" (S. 11). Dass der Band auch als Open Access vorliegt, ist angesichts eines großen Interesses von Studierenden folgerichtig, welches die Herausgeberinnen in der Lehre erfahren. Der Band stellt für Personen im Feld der Theaterwissenschaft mit Interesse an post- und dekolonialen Fragestellungen, Ansätzen und Gegenständen, eine Ressource und einen multiplen Denkanstoß von sehr großem Wert dar. Ich skizziere im Folgenden vier von vielen möglichen Spuren, die sich durch den Band verfolgen lassen.


Die erste Spur besteht in einem "Aufräumen im eigenen Haus" (S. 53). Dies ist ein Aufruf zur selbstkritischen Introspektion, die die "potenzielle Kolonialität der eigenen Disziplin" (S. 27) herausarbeitet. Auf dieser Spur leistet der Band ein wertvolles Zusammentragen von problematischen, machtvollen Dynamiken in Theaterpraxis, Theaterkritik sowie Forschung und Lehre von Theaterwissenschaft. Diese "Macht-Mechanismen" (Bühnenwatch, S. 283) beruhen u.a. auf weißen, bürgerlichen, patriarchalen und eurozentrischen Hegemonien. Die sich durch den Band ziehende ‘Liste‘ diskutiert Abwehr (etwa die Ausrufung einer "Ära des Post-Postkolonialen", S. 13; vgl. auch Heinickes Beitrag), "Ignoranz gegenüber […] Rassismen" (Heinicke, S. 179), die fehlende Bereitschaft "sich mit den Machtdimensionen von Zeichen auseinanderzusetzen" (Kalu, S. 79), das Abgleiten in "weiße Fragilität" (Tischkau, S. 98) sowie Prozesse "der Ver-Anderung, Viktimisierung und Tokenisierung" (S. 21) gegenüber BIPOC Personen (wie u.a. im Beitrag von N'Diaye ausgeführt). Ein weiterer Punkt ist die Selbstverständlichkeit mit der aus weißer Perspektive Texte über Inszenierungen von Schwarzen Künstler*innen produziert werden ohne jegliches Bemühen, sich ein notwendiges “Vorwissen zur Dechiffrierung“ (Ayivi, S. 91) zu erarbeiten, obwohl die primären Referenzsysteme dieser Arbeiten Schwarzes Wissen und Schwarze Diskurse sind. Auch wirken allerorts die von Sruti Bala beschriebenen "globalen Asymmetrien" (S. 69). Die Mechanismen verweisen ex negativo auf konkrete Aufgaben für die Theaterwissenschaft. Eine mögliche Umsetzung dessen aus weißer Perspektive findet sich zum Beispiel darin, was Ann-Christine Simke – in Anlehnung an den offenen Brief "White Colleague Listen!*" von 2020 – als "Doing the Work" beschreibt. Alle auf dieser Spur benannten Dynamiken lese ich mit Nana Adusei-Poku auch als eine spezifische Form von hegemonialem Widerstand, der Dekolonisierungsanstrengungen, die stets "uncomfortable work" sind, ausbremst (Adusei-Poku 2018, S. 40).


Eine zweite Spur betont indes einen gegenhegemonialen Widerstand, der sich gegen Kolonialität, Rassismus und andere Formen der strukturellen Unterdrückung richtet. Im Theater hat dieser zu juristischen und kulturpolitischen Werkzeugen geführt oder er zeigt sich in kuratorischen und ästhetischen Strategien (vgl. die Beiträge von Liepsch und Carvalho/Larsson). So erläutert Necati Öziri in Bezug auf seine Dramatik, wie er "Empathie als eine Form von Widerstand" poetologisch einsetzt, "damit sie – die weiße Dominanzgesellschaft – mich nicht mehr entmenschlichen kann und stattdessen sieht, was sie ihrer eigenen Menschlichkeit damit antun." (S. 168). Der Widerstand an Universitäten hat auch die Herausgabe des Bandes motiviert.


Die dritte Spur ist gut gefasst durch den Untertitel des Bandes, nämlich "Eine" – von vielen möglichen – "kritische Bestandsaufnahme" vorzulegen. Viele Beiträge zentrieren das Anekdotische, verkörpertes Wissen und Erfahrungswissen sowie die Einsicht in die Unabgeschlossenheit jeglicher Dekolonisierungsversuche, alles Elemente für Bewegungen in Richtung Dekolonisierung, die bereits Sruti Bala 2017 in ihrem Aufsatz "Decolonising Theatre and Performance Studies" diskutiert hat. Der Text, im Band zurecht gewürdigt als "eine der einschlägigsten Publikationen zu diesem Thema innerhalb unserer Disziplin der letzten Jahre" (S. 20), ist in deutscher Übersetzung und leicht ergänzter Fassung enthalten. Da post- und dekoloniale Theorie oft das Verhältnis von Macht und Wissen anvisiert, heißt Bestandaufnehmen hier auch zu befragen, welches Wissen und welche Wissensproduzent*innen ‘akzeptiert‘ oder durch gatekeeping und epistemische Grenzziehungen zwischen Wissenschaft und Praxis oder Academia und Aktivismus "(de)klassifiziert" (S. 40) werden (vgl. Beiträge von Marschall, Carvalho/Larsson, Hajusom). Der Sammelband behandelt daher alle Texte und Autor*innen deutlich weniger hierarchisch als üblich und verfolgt damit die Absicht, "den akademischen Ort als den vermeintlich einzigen Ort theoretischer Produktion [zu] provinzialisieren und dezentrieren" (S. 17). Ganz besonders überzeugen die drei Gespräche, die Aidan Riebensahm mit den Theatermacherinnen Simone Dede Ayivi, Joana Tischkau und Olivia Hyunsin Kim geführt hat und den Abschnitt "Returning the Gaze" bilden. Es entsteht ein Bild von eklatanten Missständen, mit denen sie sich in der theaterwissenschaftlichen Lehre und in Texten über ihr Theater immer wieder konfrontiert sahen und sehen, was einmal mehr für einen großen u.a. diskriminierungskritischen Bildungs- und Handlungsbedarf im Feld spricht. Ein weiteres Highlight stellt das Gespräch mit der Initiative Schwarzer Menschen in Deutschland dar, in dem Ayivi und Tahir Della Einblick geben in die Arbeit dieser Schwarzen Selbstorganisation, die, neben vielen anderen Aktivitäten, Schwarze Theatermacher*innen im von Rassismus durchsetzten Alltag unterstützt.


Das eigene ForschungsHandeln mit post-/dekolonialen Perspektiven grundlegend zu befragen und anders zu gestalten betrifft auch die Frage der Methode – die vierte Spur. Der Aufsatz von Sharifi und Skwirblies besticht, neben Hilfestellung zur Orientierung im breiten Feld post- und dekolonialer Diskurse und wertvoller Begriffsarbeit (u.a. zu postkolonialem Theater vs. postkolonialer Theaterwissenschaft), vor allem durch einen Transfer auf die Methoden der Aufführungsanalyse und Theaterhistoriographie, etwas was die beiden jüngst erschienen Methodenbände bezeichnenderweise weitestgehend ‘umschifften‘. Es wird ein lautes Schweigen zu "race […] in den meisten Aufführungsanalysen" (S. 44) dargelegt, was sich nur sehr langsam ändert, also dass zum Beispiel weiße Theaterwissenschaftler*innen die Wirkungen von Weißsein einbeziehen. Dies verwundert, da doch zur Frage der Inszenierungs- und Aufführungsanalyse unter rassismuskritischen und post-/dekolonialen Vorzeichen beziehungsweise zu Weißsein im Theater spätestens seit 2012 in Deutschland bereits geforscht wurde, neben Joy Kristin Kalu und Azadeh Sharifi auch von Daniele Daude (etwa in Daude 2014). So diskutieren auch im Band Kalu und Simke die Probleme und Entwicklungsnotwendigkeit dieser Methode. Kalu legt konzise dar, warum eine "Reflexion der eigenen Positionalität", den wirkenden Blickregimen und den Vorannahmen "Grundlage der Auseinandersetzung" (S. 81) sein sollten.


Die vier Spuren zeigen, dass sich der Band in eine Wissenschaftspraxis einreiht, in der das eigene Handeln in Lehre, Forschung, zivilgesellschaftlichem Engagement und anderen Lebensbereichen als inhärent verknüpft, etwa als ForschungsHandeln verstanden wird (vgl. AK ForschungsHandeln 2015) Folglich unterstreichen viele Beiträge, dass Theatermacher*innen und -wissenschaftler*innen eine “gesellschaftliche Verantwortung“ haben (etwa S. 73, 263).


Es bleibt zu hoffen, dass es der deutschsprachigen Theaterwissenschaft gelingt, dem Band und postkolonialen Ansätzen allgemein mit einem anderen ForschungsHandeln zu begegnen, als dies bisher getan wurde. Ob dies passiert, wird sich zeigen, denn in vielen wissenschaftlichen Kontexten im Globalen Norden lässt sich die Tendenz beobachten, nach einer Abwehr machtkritische Ansätze derart zu behandeln (Stichwort "'Trend' zum Postkolonialismus"; Kim, S. 103), dass es primär zu deren "Depolitisierung sowie einer Profilierung von weißen, privilegierten Wissenschaftler*innen" (Mauer/Leinius 2021, S. 36) kommt. Der Band jedenfalls lädt in der Gesamtschau dazu ein, aus den vorherrschenden Dynamiken zu lernen sowie demütig die Arbeit derer wahrzunehmen, die dies in Wissenschaft, Theaterpraxis und Aktivismus schon sehr lange machen. Erfrischend ist die offene Selbstkritik mit der etwa Joachim Fiebach das “Vertrackte“ im eigenen Handeln beleuchtet oder die Herausgeberinnen festhalten, dass "die Annahme, jegliche wissenschaftliche Disziplin aus der Position des Globalen Nordens heraus ‘dekolonisieren‘ zu können, vermessen ist" (S. 18). Eine Theaterwissenschaft in diesem Sinne, die von postkolonialen Ansätzen lernt, Hegemonien de- statt rezentriert, die "eigene Komplizenschaft" (S. 52) darin reflektiert, Verantwortung übernimmt und sich neu entwirft, gibt Hoffnung auf die Theaterwissenschaft der schon lange begonnenen Zukunft.




Literatur


Nana Adusei-Poku: "Everyone has to learn everything or Emotional Labour Revisited". In: Allianzen. Kritische Praxis an weißen Institutionen. Hg. v. Elisa Liepsch, Julian Warner, Matthias Pees, Bielefeld: Transcript 2018, S. 34-49.


AK ForschungsHandeln (Hg.): InterdepenDenken! Wie Positionierung und Intersektionalität forschend gestalten? Berlin: w_orten &amp; meer 2015.


Daniele Daude: "Racialization in Contemporary German Theater". Textures, Berlin 2014.


Heike Mauer, Johanna Leinius (Hg.): Intersektionalität und Postkolonialität. Kritische feministische Perspektiven auf Politik und Macht. Opladen u.a.: Barbara Budrich 2021.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2022/2</description>
  </descriptions>
</resource>
