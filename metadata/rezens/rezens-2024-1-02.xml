<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2024-1-02</identifier>
  <creators>
    <creator>
      <creatorName>Davidovic, Darija</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Friederike Oberkrome: Über die Wiederkehr des Botenberichts im Theater der Migration.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2024</publicationYear>
  <dates>
    <date dateType="Submitted">2023-06-20</date>
    <date dateType="Accepted">2024-05-05</date>
    <date dateType="Updated">2024-05-15</date>
    <date dateType="Issued">2024-05-15</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-674-8069</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Die Studie Recherche und Erkundung. Über die Wiederkehr des Botenberichts im Theater der Migration von Friederike Oberkrome untersucht – wie im Titel vorweggenommen – eine Wiederkehr des Botenberichts im postmigrantischen Theater der Gegenwart. "Recherche" und "Erkundung" dienen dabei als Oberbegriffe für unterschiedliche Verfahrensweisen des Berichtens, während der Botenbericht als heuristische Denkfigur verstanden wird, mit der die Autorin untersucht, wie im postmigrantischen Theater soziale, kulturelle und ethnische Ausgrenzung sicht- und erfahrbar gemacht werden können. Als Analysebeispiele werden vier dokumentarische Produktionen herangezogen, die Oberkrome zufolge für das Theater der Migration charakteristisch sind: In Form einer städtischen Erkundung rücken migrantische Lebensrealitäten in den Mittelpunkt der Aufführung, indem öffentliche Räume als Aufführungsort(e) bespielt werden und deren Anwohner*innen als Bot*innen in Erscheinung treten. Darüber hinaus werden in Theaterarbeiten, die als fiktionalisierte Recherchestücke teilweise auf den Biografien des Ensembles basieren, Konstellationen sozialer Marginalität beleuchtet.


Die Studie gliedert sich in zwei Hauptteile mit jeweils drei Kapiteln. Im ersten Teil erarbeitet Oberkrome eine theoretische Grundlage für das Verständnis des Botenberichts aus medien- und gesellschaftstheoretischer sowie theaterhistorischer und -theoretischer Perspektive. Dabei werden verschiedene Konzepte wie die Medialität des Boten, dessen Rolle als Vermittler zwischen heterogenen Welten sowie die damit verbundene Problematik der Authentizität und Evidenz von Dokumentarismen eingehend diskutiert. Im zweiten Hauptteil werden vier Theaterarbeiten einer detaillierten Analyse unterzogen: Kahvehane – Turkish Delight, German Fright? (2008) von Tunçay Kulaoğlu und Martina Priessner, Die Lücke (2014) von Nuran David Calis, Common Ground (2014) von Yael Ronen und Ultima Ratio (2015) von Nicole Oder. Diese Theaterproduktionen zeichnen sich durch ihre Annäherung an migrantisch geprägte Wissens- und Erfahrungsräume aus. Sie basieren auf Rechercheprozessen und kollaborativer Stückentwicklung und setzen dabei auf dokumentarische Mittel wie Video- und Audiodokumente.


Nach Oberkrome ist den von ihr gewählten Beispielen gemein, dass sie von performativen Widersprüchen des Dokumentarischen geprägt sind: einerseits werden sie mit Authentizität, Evidenz und Echtheit in Verbindung gebracht, wodurch ein Objektivitätsanspruch der inszenierten (Lebens-)Geschichten impliziert wird. Andererseits sind diese dokumentarischen Mittel als Erzeugnisse gesellschaftlicher Vermittlungs- und Konstruktionsprozesse stets kritisch zu betrachten. Diese in den Inszenierungen beobachtbare, paradoxe Gegenbewegung bildet den Ausgangspunkt der Untersuchung und zugleich ihren analytischen und methodischen Ansatz. Darüber hinaus folgt die Studie zwei wechselseitig aufeinander verweisenden Thesen: Der Botenbericht wird zur Form eines (postmigrantischen) marginalen Berichtens, wogegen die vier Theaterbeispiele der These von einer Wiederkehr des Botenberichts im Theater der Migration dienen. Aufgrund der gesellschaftlichen Randständigkeit solcher migrantischen Wissens- und Erfahrungsräume, bezeichnet die Autorin die untersuchten Botenformen als "marginales Berichten" (S.14). Die unterschiedlichen Formen des Berichtens gestalten sich "weniger als ein Berichten vom Rand, sondern als ein Berichten am oder auf dem Rand" (S. 15), wodurch mediale Konstruktionen des Migrantischen zwar berührt, jedoch nicht affirmiert und reproduziert werden. 


Schwerpunktmäßig geht es in der Arbeit um dokumentarische Ästhetiken des Berichtens, die Oberkrome als mediale Vorgänge mit politischen Implikationen versteht, weswegen diese zugleich ein unscharfes und im Spannungsfeld von Authentizitätsversprechen und Realitätskonstruktion ambivalentes Medium darstellen. Die Autorin nähert sich dieser Problemstellung, indem sie zunächst die medien- und gesellschaftstheoretischen Implikationen des Berichtens in den Fokus ihrer Untersuchung rückt. Hierfür stützt sich Oberkrome auf Sybille Krämers medienphilosophische Überlegungen zur Botenfigur als einer zentralen Reflexionsfigur für Übertragungsvorgänge. In Medium, Bote, Übertragung. Kleine Metaphysik der Medialität (2008) beschreibt Krämer den Boten als Mitte und Mittler – als eine Figur des Dritten, die eine Kommunikation zwischen heterogenen Welten ermöglicht. Ferner sind für diese ersten theoretischen Schritte vor allem Aspekte der Distanz und der Heteronomie in Krämers Botenmodell zentral: Der Bote tritt als Reflexionsfigur für eine Vielfalt medialer Gegebenheiten auf, in denen Distanzierung und Annäherungsprozesse zusammenfallen. Diese Überlegungen zum Vermittlungsvorgang sowie zur Figur des Boten bei Krämer sind für die Analysen Oberkromes insofern ausschlaggebend, als dass sie unterschiedliche asymmetrische und nicht-dialogische Formen der Ansprache untersuchen und zugleich inszenatorische Strategien sowie ästhetische Besonderheiten in den Fokus der Untersuchung rücken kann.


Vor dem Hintergrund einer postmigrantisch strukturierten und kolonial geprägten Gesellschaft, die in den vier Theaterbeispielen thematisiert wird, geht Oberkrome in ihrer Studie der Frage nach, inwiefern das Botenmodell Fremd- und Selbstbestimmtheit kritisieren und reflektieren kann bzw. ob der Bote als Mittler vorhandene Wahrnehmungsmuster und bestimmte soziale Ordnungen sichtbar macht und aufbricht. Hierzu bezieht sich Oberkrome zunächst auf Homi K. Bhabhas Hybriditäts-Begriff, um "die subversive Kraft des Dritten als Störfaktor binärer Ordnungssysteme zu bestimmen" (S. 23). Um sich dem machtkritischen Potenzial der Botenfigur zu nähern, stützt sich Oberkrome ferner auf Frantz Fanons phänomenologische Auseinandersetzung mit kolonialen Blickregimen, wodurch sie sich den Bedingungen und Effekten von rassifizierenden Fremdzuschreibungen medialer Übertragung nähert.


In Schwarze Haut Weiße Maske (1952) kritisiert Fanon basierend auf seinen eigenen Erfahrungen, wie eine "Welt der Weißen" die Selbsterfahrung sowie die Erfahrungswelt Schwarzer Menschen bestimmt und dominiert. Bei Fanon steht dementsprechend ein medial übertragenes Bild von Schwarzen Menschen im Zentrum, das durch weiße Blickregime und radikale Fremdbestimmung determiniert ist. Ferner scheint sich Oberkrome auf die Begriffe weiß und Schwarz, die besonders im Kontext der Critical Whiteness Anklang finden, zu berufen, was jedoch nicht näher ausgeführt wird und bezogen auf die vielfältigen und disparaten migrantischen Erfahrungswelten im Kontext der deutschen Einwanderungsgeschichte stellenweise für Irritationen sorgt. Hierzu wäre eine kritische Diskussion bezüglich der Verwendung dieser Konzepte, die sich nicht auf die Hautfarbe, sondern auf Rassismuserfahrungen sowie auf gesellschaftliche Normen und Machtpositionen beziehen, aufschlussreich gewesen.


Im Hinblick auf die Auswahl der Theaterbeispiele, in denen rassistische Erfahrungen türkischer Gastarbeiter*innen und ihrer Nachkommen sowie Kriegs- und Fluchterfahrungen eines postjugoslawischen Ensembles thematisiert werden, stellt sich die Frage, warum sich Oberkrome hierzu nicht auf konkrete Untersuchungen von Postkolonialität im deutschsprachigen Raum bezieht, etwa auf den von Hito Steyerl und Encarnación Gutiérrez Rodriguez herausgegeben Band Spricht die Subalterne deutsch? Migration und postkoloniale Kritik (2003). Auch hinsichtlich rassistischer Fremdzuschreibungen von Personen aus Südosteuropa hätte sich Oberkrome auf eine vielfältige Auswahl an bedeutenden Studien zu antislawischem Rassismus stützen können, wodurch es ihr möglich gewesen wäre, sich den jeweils unterschiedlichen migrantischen Erfahrungen in Deutschland, die in den Theaterbeispielen bereits implementiert sind, präziser zu nähern.


Im zweiten Teil der Studie widmet sich Oberkrome schließlich den vier Theaterbeispielen, die sie einerseits unter dem Aspekt der urbanen Erkundung und andererseits als szenischen Rechercheprozess untersucht. Hierzu nimmt sie zunächst Kahvehane – Kahvehane ‒ Turkish Delight, German Fright? und Die Lücke vergleichend in den Blick, die auf unterschiedliche Weise einen öffentlichen städtischen Raum bespielen. Theater-Parcours Kahvehane – Kahvehane ‒ Turkish Delight, German Fright? wurde 2008 im Rahmen von Dogland – Junges postmigrantisches Theaterfestival produziert. Das Theaterprojekt brachte insgesamt zwölf junge Künstler*innen aus unterschiedlichen Herkunftsländern, wie etwa Bosnien und Herzegowina, Israel oder der Türkei mit jeweils einem Kaffeehaus in Kreuzberg oder Neukölln zusammen, um vorherrschende Bilder und Mythen über türkische Arbeitsmigrant*innen zu beleuchten. Auf der anderen Seite thematisiert das Stück Die Lücke den verheerenden Nagelbombenanschlag, der am 9. Juni 2004 in der Kölner Keupstraße vom Nationalsozialistischen Untergrund (NSU) verübt wurde. Diese Aufführung stellt die Lebensrealität der migrantischen Anwohner*innen nach dieser traumatischen Erfahrung extremer Gewalt in den Mittelpunkt ihrer Handlung.


Daran anschließend analysiert Oberkrome im fünften Kapitel die Inszenierungen Common Ground und Ulima Ratio, um sich der zeitlich komplexen Vermittlungen zwischen Gegenwart und Vergangenheit zu nähern, die sich in beiden Beispielen im Motiv der Reise niederschlagen. Während in Common Ground die vom Ensemble für die Stückentwicklung unternommene Reise nach Sarajevo sowie darauf basierende Erinnerungen mit Kindheitserinnerungen des postjugoslawischen Ensembles verschränkt werden, erzählt Ultima Ratio die Fluchtgeschichte eines somalischen Paares, das in Deutschland vergeblich Asyl zu beantragen versucht.


Mit dieser gezielten Auswahl strebt Oberkrome einen repräsentativen Querschnitt an, der das Theater der Migration in Deutschland breit abbilden soll. Dabei werden spezifische Zeitabschnitte abgedeckt, die für die Geschichte der Migration nach Deutschland von großer Bedeutung sind, wie etwa die Migration von Arbeitskräften, die ab den 1950er Jahren als sogenannte Gastarbeiter*innen in die Bundesrepublik kamen. Des Weiteren wird die Migration der vor den jugoslawischen Zerfallskriegen flüchtenden Menschen während der 1990er Jahre thematisiert sowie die jüngste Fluchtmigration nach Deutschland.


In den Analysen gelingt es der Autorin, auf präzise Weise zentrale Aspekte des Berichtens beziehungsweise eine aktualisierte Version der Botenfigur im postmigrantischen Theater herauszuarbeiten: Der Bote ist selbstreflexiv, bezieht sich auf (inter-)mediale Konstellationen und ist eng mit der Frage nach Ethik und Darstellung verbunden sowie von sozialer Marginalität geprägt. Die Studie bietet somit präzise Einblicke in Strategien der Emanzipation von hegemonialen Identitätszuschreibungen im Theater der Migration. Sie setzt dabei konkrete Vorkenntnisse über postkoloniale Theorien voraus und richtet sich darum vor allem an Leser*innen, die mit postkolonialen Diskursen im deutschsprachigen Raum und postmigrantischem Theater vertraut sind.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2024/1</description>
  </descriptions>
</resource>
