<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2023-2-13</identifier>
  <creators>
    <creator>
      <creatorName>Gönitzer, Daniel</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Walter Benjamin: Kleine Geschichte der Photographie.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2023</publicationYear>
  <dates>
    <date dateType="Submitted">2023-10-05</date>
    <date dateType="Accepted">2023-11-02</date>
    <date dateType="Updated">2023-11-14</date>
    <date dateType="Issued">2023-11-14</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-650-8283</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Im September 1931 erschien Walter Benjamins "Kleine Geschichte der Photographie" aufgeteilt auf drei Ausgaben in der Wochenzeitung Die literarische Welt (vgl. Benjamin GS II, S. 1130). Fast 100 Jahre später hat nun Wolfgang Matz diesen Klassiker der modernen Medien- und Kulturtheorie beim Alexander Verlag Berlin neuherausgegeben und dem Aufsatz seinen Essay "Die Wirklichkeit, das Bild" beigefügt. Das Buch wurde im handlichen Format mit schwarz-silbernem Cover gestaltet. Dem Text wurden dabei sämtliche Fotografien beigefügt, die auch in der ursprünglichen Veröffentlichung abgedruckt waren. Diese Fotografien von Germaine Krull, August Sander, David Octavius Hill, Karl Dauthendey und einem unbekannten deutschen Fotografen sind hingegen in den Gesammelten Schriften lediglich geblockt am Ende des Textes zu finden: kurioserweise zwischen der vorletzten und letzten Seite des Aufsatzes (vgl. Benjamin GS II, S. 384). Die verstreute Platzierung der Fotografien in der Neuausgabe entspricht vom Layout her viel eher der ursprünglichen Veröffentlichung des Aufsatzes. Dies wird durch einen Blick auf die Abbildungen der Originalveröffentlichung deutlich, die auf die Innenseiten des Umschlags der Neuedition gesetzt wurden. Allein aufgrund dieses sneak peek ins Archivmaterial empfiehlt sich meiner Ansicht nach die Neuausgabe. Zur Buchgestaltung ist zusätzlich auf eine fehlende Information am Buchrücken hinzuweisen. Es findet sich dort folgendes Zitat aus Benjamins "Kleine Geschichte der Photographie": "Nicht der Schrift- sondern der Photographieunkundige wird der Analphabet der Zukunft sein" (S. 52). Dieses Zitat kann nicht Benjamin zugeschrieben werden. Denn Benjamin weist in "Neues von Blumen", der bereits 1928 veröffentlichten Rezension von Karl Bloßfeldts Urformen der Kunst, darauf hin, dass dieser hellsichtige Satz vom "Pionier des neuen Lichtbilds, [László] Moholy-Nagy" stammt (Benjamin GS III, S. 151). Dass die Auswahl gerade auf dieses (nicht ausgewiesene) Zitat eines Zitates gefallen ist, entspricht immerhin durchaus Benjamins Arbeitsweise, wenn man sich seine unvollendet gebliebene "Passagenarbeit" in Erinnerung ruft, die zu großen Teilen aus Zitaten bestehen hätte sollen.


Inhaltlich führt das Moholy-Nagy Zitat jedenfalls mitten ins Zentrum von Benjamins Überlegungen zur Fotografie. Benjamin kritisiert die kunst- und filmtheoretische Diskussion darüber, ob Fotografie eine Kunst sei oder nicht (vgl. S. 9), und kontrastiert dagegen, in Anlehnung an Gisèle Freund, dass die Erfindung der Fotografie den "Gesamtcharakter der Kunst" von Grund auf verändert habe (Benjamin GS III, S. 542). Die Fotografie stellt für Benjamin schließlich das erste "wirklich revolutionäre Reproduktionsmittel" dar (Benjamin GS VII, S. 356), das womöglich "in unterirdischen Zusammenhang mit der Erschütterung der kapitalistischen Industrie stünde" (S. 9). Erst mit dem Aufkommen der Fotografie kommt es folglich zur "Zertrümmerung der Aura" (S. 37) und somit zur Emanzipation von dem kultisch-religiösen Kern der Kunst. Im Fotografie-Aufsatz findet sich Benjamins erste Bestimmung der Aura: "Was ist eigentlich Aura? Ein sonderbares Gespinst von Raum und Zeit: einmalige Erscheinung einer Ferne, so nah sie sein mag" (S. 36). Benjamins Aura-Konzeption beinhaltet eine räumliche sowie eine zeitliche Ebene. Während die räumliche Dimension von den Polen Nähe und Ferne bestimmt sei, ließe sich die zeitliche Ebene durch Einmaligkeit und Wiederholung beschreiben. Künstlerische Äußerungen, die eine auratische Wahrnehmung hervorrufen, sind nach Benjamin durch Distanz und Unnahbarkeit sowie Einmaligkeit, Echtheit und Originalität bestimmt. Die von der Aura befreite Kunst lässt sich im Umkehrschluss durch Wiederholbarkeit und Nähe beschreiben. Doch Benjamin hat kein Interesse daran, die Aura genau zu definieren. Fokus seiner kultur- und medientheoretischen Texte und somit auch seiner "Kleine[n] Geschichte der Photographie" ist vielmehr die These vom Verlust oder Verfall der Aura im Zeitalter der technischen Reproduzierbarkeit von Kunstwerken.


Benjamins Aufsatz prägte sowohl die Theorie und Geschichte der Fotografie als auch Theorien der Wahrnehmung und Bildbetrachtung. Dass dieser Text nun als Einzelausgabe in modernem Layout und Design neuherausgegeben wurde, ist meiner Ansicht nach ein verdienstvolles Unterfangen. Äußerst schade ist jedoch, dass in der Neuausgabe der Einfluss von Gisèle Freund und Germaine Krull auf Benjamins Auseinandersetzung mit Fotografie nicht stärker betont wird. Während Freund zweifelsohne Benjamins wichtigste Gesprächspartnerin in Bezug auf Theorie, Technik und Geschichte der Fotografie war, beeinflusste Krull ihn insbesondere durch ihre avantgardistischen Fotografien. Im Pariser Exil pflegte Benjamin zu beiden enge freundschaftliche Beziehungen sowie einen intensiven inhaltlichen Austausch. Beide fertigten Portraitaufnahmen von ihm an. Im beigefügten Essay von Matz findet sich zwar eine äußert aufschlussreiche und informative, historische Kontextualisierung und Kommentierung, doch diese Fotografinnen werden kaum erwähnt. Dass Matz in seinem Essay darüber hinaus aufs Gendern verzichtet, verstärkt den Eindruck fehlender feministischer Positionierung.


Im Abschlussteil des Textes versucht der Autor, Benjamins Überlegungen weiterzudenken und zu aktualisieren, und widmet sich medientheoretischen Fragen der Gegenwart. Dabei nimmt er jedoch eine kulturpessimistische und medienkonservative Haltung ein, die meiner Ansicht nach konträr zu den Positionen Benjamins steht. Matz zufolge sei etwa das "emanzipatorische Potential, das Benjamin der technischen Reproduzierbarkeit zuschrieb" eine "Illusion" (S. 97). Unerwähnt bleibt dabei, dass sich Benjamin durchaus bewusst war, dass Fortschritte in der (Reproduktions-)Technik, den politischen Kampf nicht ersetzen würden. Jedoch hoffte er, dass Film und Fotografie kritisch und reflektiert eingesetzt dem Proletariat im Kampf für eine bessere Welt helfen könnten. Die Entfaltung des emanzipatorischen Potentials dieser technischen Kunstformen vollzieht sich für Benjamin erst im Rahmen einer revolutionären Veränderung der Gesellschaft. So heißt es im Kunstwerkaufsatz, der in großen Teilen auf dem Fotografie-Text aufbaut, dass die "politische Auswertung des Films, so lange auf sich wird warten lassen, bis sich der Film aus den Fesseln seiner kapitalistischen Ausbeutung befreit haben wird" (Benjamin GS VII, S. 370). Gleiches gilt für die Fotografie. Matz kritisiert darüber hinaus "die nahezu unbegrenzte technische Manipulierbarkeit digitaler Bilder" heute (S. 96). Doch Benjamin, der die verfremdenden Fotomontagen des Antifaschisten John Heartfield euphorisch rezipierte, geht es gerade um das Zerschlagen der konservativen Fundierung der auratischen Kunst mit ihren Ansprüchen auf Echtheit, Original und ewige Wahrheiten. Durch Montage und Bearbeitung von Fotografien besteht zwar stets die Gefahr einer politischen Instrumentalisierung und Verfälschung, jedoch sollte daraus kein Plädoyer für eine vermeintlich authentische, wahre oder gar ausschließlich dokumentarische Fotografie abgeleitet werden. Anstatt Inventarisierung und Konservierung fordert Benjamin von der Fotografie sowie von der Kunst allgemein den aktiven Gebrauch, die Handhabbarmachung, Aktualisierung und Neu-Ordnung des Alltäglichen, Gewöhnlichen, Weggeworfenen, Vergessenen, Verkommenen und Irrationalen. Das subversive Potential der Fotografie besteht für Benjamin ausdrücklich nicht in einer dokumentarischen Wiedergabe oder Aufzeichnung der Realität, sondern im spielerischen Eröffnen von neuen Wahrnehmungen und Erfahrungen.


Hinsichtlich der aktuellen fotografischen Praxis hält Matz zwar fest, dass die technische Entwicklung der Fotografie "eine Vereinfachung, Verbilligung und Vervielfältigung der Bildproduktion" mit sich gebracht hat (S. 107) und dass auch "mit diesen neuen Voraussetzungen […] ästhetisches Spiel" und "Kunst" möglich sei (S. 110). Da diese ihm zufolge jedoch nicht zum "historischen Bildgedächtnis der verlorenen Zeit" beitragen (S. 110), bewertet er die "tagtäglich produzierten Smartphonebilder", die heutige "Masse der Alltagsbilder" als "digitalen Müll", der schließlich zur extremen "Entwertung der Fotografie" führe (S. 99). Mit der Abwertung der Smartphone-Fotografie und der gleichzeitigen Aufwertung der "bewusst gestalteten Fotografie" (S. 106) reproduziert Matz die von Benjamin problematisierte Diskussion über den Kunstcharakter der Fotografie. Anstatt in kulturpessimistischem Ton die technischen Möglichkeiten unserer Zeit abzuwerten und ihnen eine vermeintlich bedrohte und verschwindende Kunst-Fotografie gegenüberzustellen, gilt es meiner Ansicht nach, die moderne "Smartphone-Fotografie" auf ihren emanzipatorischen Gehalt hin zu befragen. Ausgehend von Benjamins Emphase für eine Demokratisierung und Politisierung der Kunst durch Technik müsste eine derartige Analyse jedoch auch die problematischen Aspekte dieser Fotografie reflektieren, die unter anderem in einer massiven Fetischisierung, Sexualisierung, Kommodifizierung und Normierung von Körpern besteht.


Quellen:


Benjamin GS + Bandnummer = Benjamin, Walter: Gesammelte Schriften. VII Bde. Unter Mitwirkung von Theodor W. Adorno herausgegeben von Rolf Tiedemann und Hermann Schweppenhäuser, Frankfurt a. M.: Suhrkamp 1991.


Daraus zitierte Texte:


Benjamin, Walter: "Kleine Geschichte der Photographie". In: GS II, S. 368–386.


Benjamin, Walter: "Anmerkungen zu: Kleine Geschichte der Photographie". In: GS II, S. 1130–1143.


Benjamin, Walter: "Neues von Blumen". In: GS III, S. 151–153.


Benjamin, Walter: "Gisèle Freund, La photographie en France au dix-neuvième siècle". In: GS III, S. 542–544.


Benjamin, Walter: "Das Kunstwerk im Zeitalter seiner technischen Reproduzierbarkeit". In: GS VII, S. 350–385.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2023/2</description>
  </descriptions>
</resource>
