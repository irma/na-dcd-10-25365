<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2021-1-16</identifier>
  <creators>
    <creator>
      <creatorName>Mücke, Laura Katharina</creatorName>
    </creator>
    <creator>
      <creatorName>Krems, David</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Edgar Lissel / Gabriele Jutz / Nina Jukić (Hg.): RESET THE APPARATUS! A Survey of the Photographic and the Filmic in Contemporary Art.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2019-12-16</date>
    <date dateType="Accepted">2021-05-06</date>
    <date dateType="Updated">2021-05-20</date>
    <date dateType="Issued">2021-05-20</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-464-3286</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Was bleibt vom genuin Fotografischen/Filmischen nach dem Ende des fotografischen Zeitalters? Welche grundlegenden medialen Charakteristika des Fotografischen bzw. Filmischen werden von aktuellen künstlerischen Prozessen aufgegriffen oder umgedeutet? Derartigen Fragen widmete sich das Forschungsprojekt RESET THE APPARATUS!, das als PEEK-Projekt (Arts- based Research) an der Abteilung für Medientheorie der Universität für Angewandte Kunst in Wien realisiert wurde und mit dieser Publikation eine Mischung aus Überblickskatalog und Abschlussbericht vorlegt.  


Ausgehend von dem – freilich nur auf den ersten Blick – paradox wirkenden Umstand, dass die neuen digitalen Technologien ein gesteigertes Interesse an den analogen Medien hervorgerufen haben, widmet sich dieses Buch – das auch und insbesondere Materialsammlung ist – Fragen nach der Rolle des spezifisch Fotografischen (photographic) bzw. Filmischen (filmic) in der zeitgenössischen Kunst. Im Zentrum steht eine genaue Betrachtung der im Forschungsprojekt untersuchten Medien bzw. deren materieller/technischer Grundlagen, die niemals allein Mittel zum Zweck sind, sondern sich auf mannigfaltige Weise in den künstlerischen Prozess, dessen Ergebnis, Wahrnehmung und Verwertung einschreiben.
Dieses konstitutive Potenzial des Apparatus ist aus den Medienwissenschaften gut bekannt, weshalb das Besondere an der vorliegenden Publikation nicht die Fragestellung per se, sondern viel mehr dessen Aufbereitung ist. Als Arts-based Research-Projekt wird hier gewissermaßen der Spieß umgedreht und sprichwörtlich in der Schraube die Maschine erkannt. Ausgehend von einer umfangreichen Materialsammlung und in Zusammenarbeit mit externen Partner*innen (aus den Bereichen der Wissenschaft als auch den Künsten, heimischer als auch internationaler Provenienz) wird ein Feld erschlossen, das man in den medienwissenschaftlichen Diskursen schon öfters betreten hat, hier aber plötzlich aus einem anderen Blickwinkel sieht. Und das ist nicht bloß als Metapher gemeint, denn zu sehen gibt es in diesem Buch/Katalog wirklich einiges: Die Publikation präsentiert sich als durch theoretische Texte angereicherter Bildband und stellt in dieser Form ein beeindruckendes Zeugnis zeitgenössischer Medienkunst vor, die das Projektteam in den vergangenen Jahren befragt hat.


Das Buch ist folgendermaßen aufgebaut: Ein einleitender Aufsatz der drei Projektleiter*innen, Gabriele Lutz, Edgar Lissel und Nina Jukić – der sich selbst explizit als "Gebrauchsanweisung" ("User’s Manual") versteht – erörtert nicht nur die wesentlichen Fragestellungen und Perspektiven, sondern zieht zugleich Resümee über den aktuellen Stand von Kunstproduktionen, die Analoges und Digitales zusammendenken, beziehungsweise der theoretischen Debatten, die dieses Verhältnis begleiten. Hervorzuheben ist, dass sich die hier angestellten Betrachtungen niemals in einer Gegenüberstellung oder Aufrechnung eines Verfahrens gegenüber dem anderen erschöpfen, sondern darauf angelegt sind, die tradierten Grenzen der als überkommen (vor)verurteilten medialen Technologien aufzubrechen und deren Einfluss auf aktuelle künstlerische Verfahren zu bewerten. Die Aussage "For a contemporary understanding of medium specificity, it is necessary to give up 'the old fiction of the purity of media' and to consider their 'interpenetration and contamination'" wird unter Bezug auf die Filmwissenschaftlerin Erika Balsom als ein Hauptanliegen des Projekts formuliert. Bei dem thematischen Streifzug durch die maßgebende Film- und Medientheorie gerät denn gemäß der Stoßrichtung des Projekts insbesondere die Perspektive der Künstler*innen in den Blick, die den zweiten Teil des Buches bildet. Dabei geht es etwa um künstlerische Entscheidungen für die Arbeit mit speziellen medialen Technologien, ausgehend von dem simplen – auch wenn für die Betrachter*innen des fertigen Werkes oft nicht maßgebenden – Umstand der direkten Arbeit am künstlerischen Produkt und die Anwendung künstlerischer Verfahren, die nicht notwendigerweise auf standardisierten Wegen erfolgen muss; man denke an das breite Feld des experimentellen Films und der experimentellen Fotografie und seiner Tradition des physischen Zugriffs auf das Trägermaterial. Bezugnahmen zu jener filmischen Praxis, die unter dem Label expanded cinema ausgehend von avantgardistischen Kunstproduktionen seit den 1960er-Jahren unterschiedliche Zuschreibungen erfahren hat, prägen dann auch stark die vorliegende Analyse, wobei der hier angelegte Begriff der "Erweiterung" (im Sinne eines 'to expand') auch auf das Feld der Fotografie, oder besser: des Fotografischen, übertragen wird. Etwa wenn unter Bezug auf das erst später geprägte und weniger bekannte Schlagwort der expanded photography argumentiert wird, dass die hier verwendeten Begriffe photographic bzw. filmic darauf hinweisen würden, dass sich solcherart bezeichnete Praktiken, auch wenn sie ihre medialen Grenzen überschreiten, dennoch stets genuine Medienspezifika bewahren.


Der dritte und vierte Teil des Buches stellt die Kooperationen mit verschiedenen Medienkünstler*innen bzw. "Partner Collaborations" sowie die Arbeiten von Studierenden aus Wien und Essen vor. In dieser heterogenen Werkcollage werden theoretische Mythen des Digitalen, wie etwa der Tod des Kinos und Videos in Zeiten von digitalen Medien aufgegriffen und dekonstruiert (etwa in den Beiträgen von Jonathan Walley über Gibson+Recorder und Andy Birtwistle über die Arbeiten von Gebhard Sengmüller), Bezugnahmen zu historischen Vorläufern des kinematografischen Dispositivs, etwa zur Phantasmagorie, gezogen (Beitrag von Hubertus Amelunxen) und schließlich mit den Arbeiten der Studierenden die Frage nach dem Verbleib des Analogen ins Zeitalter der "Digital Natives" transferiert.


Der letzte Teil des Buches bindet über drei theoretische Texte und ein Essay die künstlerischen Arbeiten wieder an den Apparatus-Begriff zurück. Der im Titel des Projekts angelegte Terminus des Apparatus hatte seinen Ursprung bekanntermaßen in der französischen psychoanalytischen Filmtheorie der 1970er-Jahre, die stark mit dem Namen Jean-Louis Baudry verbunden ist, den Film seines Objektcharakters enthebt und seine Analyse in ein weiter gefasstes architektonisches wie diskursives Setting einbindet, das zahlreiche zusätzliche Aspekte – vor allem jenen der Rezeption – berücksichtigt. Die Autor*innen halten fest, dass die Apparatus-Theorie der Rolle der Produktion keine herausragende Aufmerksamkeit beikommen hat lassen – ein Missstand, den das vorliegende Werk kompensieren soll. Dieser letzte Teil steht dabei in direkter Verbindung zum sogenannten "Corpus", ein von Lissel, Jutz und Jukić zusammengetragener Katalog künstlerischer Arbeiten, die allesamt die Fragestellungen des Projektes von unterschiedlichen Perspektiven ausgehend illustrieren und kommentieren. Dieser wird von TAGS strukturiert, die jeweils einen spezifischen, medialen Aspekt im Nachdenken über den Apparatus adressieren. Damit wird quasi eine nonlineare Lektüre des Buches – ähnlich dem Blättern in einem Ausstellungskatalog – nahegelegt und so nicht nur das Verhältnis von Theorie und Medienkunst, sondern auch dessen Erfassungsprozesse als ein vernetztes Bedeutungsfeld dargestellt.
Diese von den Autor*innen in der Einleitung festgelegten TAGS werden damit nicht als unverrückbare Zuschreibungen, sondern als Ausgangspunkte zur Erkundung eines heterogenen Korpus von Medienkunst konzipiert, der so von verschiedenen Seiten aus durchwandert werden kann. Am besten funktioniert das auf der Homepage des Projektes (http://www.resettheapparatus.net), die mit präzisen Beschreibungen, Informationen und weiterführenden Links zu allen 140 versammelten Arbeiten aufwartet. Die ausgewählten Arbeiten selbst fokussieren – gemäß der Themensetzung des Projekts – die letzten zehn Jahre, reichen aber da und dort bis in die 1960er-Jahre zurück. Versammelt sind sowohl bekannte Projekte, als auch solche, die weniger bekannt sind.


So reicht etwa der TAG "Body Involvement", der auf ein Miteinbeziehen des gesamten Körpers in den medialen Prozess abzielt, von Alfons Schillings Sehmaschinen (mehrere Arbeiten ab den 1960er-Jahren) über Gustav Deutschs Taschenkino (1995), bis hin zu einer aus den frühen 2000er-Jahren stammenden Arbeit von Olena Newkryta (Absolventin der Universität für Angewandte Kunst Wien), die Proband*innen dazu aufforderte, unbelichteten Negativfilm ein Monat lang am Körper zu tragen, um diesen anschließend zu entwickeln und in Fotografien überzuführen. One month on skin (2013 – 2014) treibt so den indexikalischen Charakter der analogen Fotografie – und damit eines der zentralen Merkmale des Mediums – auf die Spitze.
Der TAG "Still / Moving" wiederum erkundet das Verhältnis zwischen Einzelbild und Laufbild, wodurch auf die ganz grundsätzliche materielle Verbindung zwischen analoger Fotografie und analogem Film fokussiert wird. Die unter diesem TAG versammelten Arbeiten bieten einen geradezu lehrbuchartigen Überblick über experimentelles Filmschaffen, das dieses Verhältnis auf unterschiedlichste Weise hinterfragt und in der Arbeit Motion Picture (1984) von Peter Tscherkassky, die auf einem Frame von Arbeiter verlassen die Fabrik (1895) der Brüder Lumière basiert, bis in die Geburtsstunde des Films zurückreicht.


Weitere TAGS, wie "Darkroom Exposed", "Fleeting Images", "Live Acts", "Lost &amp; Found", "Material Agency", "Relics", "Repurposing the Hardware", "Scale &amp; Format", "Site Specifity" und "Transplanar Images", illustrieren dabei nicht nur die strukturierte Weise, mit der sich das Forschungsprojekt den Gegenständen genähert hat, sondern auch die beeindruckende politische Tragweite der Verflechtung von Film und Fotografie. Sie zeigen so die vielen Kernbereiche der Gesellschaft auf, die von Medien schon immer durchzogen sind bzw. waren. Dass dabei etwa Themenfelder wie das Anthropozän und der materielle Überfluss, die Energieballung in städtischen Räumen und der stete Zerfall von Bildträgern wie Zelluloid als konstitutiver Bestandteil unserer Erinnerungspraxis thematisiert werden, erweist, wie vielfältig anschlussfähig die hier aufgeführten theoretischen wie künstlerischen Impulse sind.


Die englischsprachige Publikation ist – auch das soll gesagt werden – ein schön gemachtes, wertiges und preiswertes Buch, das so einige Verlage über Buchdesign und Preisgestaltung nachdenken lassen sollte. Am bemerkenswertesten ist dabei sicher die Funktion, die es – in Verbindung mit den Beiträgen der Projektpartner*innen – als überraschend übersichtliches Nachschlagwerk und gleichsam Dokumentationsversuch einer heterogenen Medienkunst-Medientheorieszene erfüllt. Der einleitende Text des Projektteams ist in seiner Klarheit und Übersichtlichkeit zudem eine Lektüre, die sich sämtlichen Leser*innen empfiehlt, die sich mit Film, Fotografie und Medienkunst beschäftigen – und zwar unabhängig davon, ob dies auf einem akademischen Niveau geschieht oder auf einem ganz "allgemeinen" Interesse an Kunst und Medien fußt. Auch das ist alles andere als eine Selbstverständlichkeit.


www.resettheapparatus.net
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2021/1</description>
  </descriptions>
</resource>
