<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2024-2-08</identifier>
  <creators>
    <creator>
      <creatorName>Hasebrink, Felix</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Fabienne Liptay (Hg.): PostProduktion. Bildpraktiken zwischen Film und Fotografie \\\\ Stefan Udelhofen/Dennis Göttel/Aycha Riffi (Hg.): Produktionskulturen audiovisueller Medien. Neuere Perspektiven der Medienindustrie- und Produktionsforschung.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2024</publicationYear>
  <dates>
    <date dateType="Submitted">2024-09-18</date>
    <date dateType="Accepted">2024-09-18</date>
    <date dateType="Updated">2024-11-13</date>
    <date dateType="Issued">2024-11-13</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="JournalArticle"/>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-675-8965</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Wie lässt sich medientheoretisch über Produktion nachdenken? Diese Frage beschäftigte schon Vilém Flusser in der Essaysammlung Gesten. Versuch einer Phänomenologie. Flusser beginnt dort seine Überlegungen zur "Geste des Filmens" mit einer überraschenden Volte. Gesten – bedeutungsstiftende und deshalb interpretationsbedürftige Körperbewegungen – hatte er zuvor aus der Perspektive derjenigen untersucht, die selbst eine gestische Bewegung ausführen. Die Geste des Fotografierens beschreibt er aus Sicht eines Fotografen, die Geste des Malens aus Sicht der Künstlerin vor der Staffelei. Anders beim Film: Er selbst habe kaum Gelegenheit, echten Dreharbeiten beizuwohnen, geschweige denn selbst eine Filmkamera zu bedienen, schreibt Flusser, deshalb würde er sich "ausnahmsweise" erlauben, die "Geste des Filmens" nicht aus der Sicht der Filmemacher*innen, sondern aus der Perspektive der Kinozuschauer*innen zu erschließen (Flusser 1994, S. 119).


Dieses Manöver ist natürlich selbst eine Geste, und zwar eine methodische, die sich durch die gesamte Geschichte der Filmtheorie verfolgen lässt. Filmtheorie wurde und wird häufig aus der Perspektive der Zuschauer*innen geschrieben. Der Bereich der realen Produktion ist dagegen selten von Interesse, höchstens dann, wenn er als Erklärungszusammenhang für ästhetische Phänomene im Film gebraucht wird. Seit geraumer Zeit lässt sich jedoch eine Hinwendung zu Fragen des Herstellens und Machens von Filmen (und Fernsehserien etc.) beobachten. Zwei neue Sammelbände greifen diese Entwicklung auf.


Produktionskulturen audiovisueller Medien, eine stattliche Sammlung von 19 Aufsätzen, die auf eine Ringvorlesung an der Universität zu Köln zurückgehen, verortet sich im Feld der kulturwissenschaftlichen Produktionsforschung. Die Autor*innen folgen mehrheitlich einem Ansatz, der in den 2000er Jahren unter dem Label 'Production Studies' oder 'Cultural Studies of Media Industries' bekannt wurde. Eine wichtige Referenzgröße sind die Arbeiten von John Thornton Caldwell, der in seiner wegweisenden Monografie Production Culture vorgeschlagen hatte, Film- und Fernsehproduktion weniger vom produzierten 'Werk' aus zu denken, sondern als eigenständige soziokulturelle Sphäre mit komplexen Ausdrucksformen zu begreifen (Caldwell 2008).


Die Idee einer dezidiert kulturwissenschaftlich ausgerichteten Erforschung von Film- und Fernsehproduktion wurde in einer ganzen Reihe von Publikationen erprobt und weiterentwickelt. Der Sammelband betritt insofern kein völliges Neuland, aber darum geht es den Herausgeber*innen Stefan Udelhofen, Dennis Göttel und Aycha Riffi auch gar nicht. Vielmehr laden sie in der Einleitung dazu ein, Produktionsforschung auf Basis bestehender Ansätze mit undogmatischer Offenheit weiterzu"treiben" (S. 6). Anschluss suchen sie dabei auch an jüngere Diskussionen innerhalb der deutschsprachigen Film- und Medienwissenschaften, die nicht direkt mit Film- oder Fernsehproduktion zu tun haben. Zu nennen ist hier erstens eine Methodendebatte, die maßgeblich in der Zeitschrift für Medienwissenschaft geführt wurde.1 Im Zentrum der Diskussion stand vor allem die Untersuchung digitaler Medien. Aber wie schon Flusser bei seiner Geste des Filmens nahelegt: Methodenfragen sind auch für den Komplex (Film-)Produktion relevant. Zweitens möchte der Band an ein wachsendes Interesse für Medienpraktiken anknüpfen, also für ein empirisches Tun von und mit Medien. Ausschlaggebend sind hier weniger der Gebrauch und die Verwendung von technischen Geräten. Der Fokus liegt auf dem herstellenden Machen von audiovisuellen Inhalten.


Dieser praxeologische Einschlag spiegelt sich in den drei thematischen Rubriken des Bandes wider. Teil 1, "Kartieren", bezieht sich vor allem auf Grundannahmen und Methoden der Production Studies. Unter dem zweiten Schlagwort "Materialisieren" wurden Beiträge versammelt, die sich mit konkreten Produktionszusammenhängen auseinandersetzen, häufig anhand eines einzelnen Werks. Texte in der dritten Rubrik "Agieren" beleuchten dagegen individuelle Personen im Produktionsgeschehen über individuelle Film- und Fernsehbeispiele hinaus.


Erfreulicherweise sind die 19 Aufsätze keine Fallstudien, die das Vokabular der Production Studies einfach nur an neuen Beispielen durchdeklinieren. Es gelingt den Autor*innen, das Feld der kulturwissenschaftlichen Produktionsforschung immer wieder substanziell weiterzuentwickeln. Das geschieht auf mindestens vier Ebenen. Eine erste betrifft die eingesetzten Untersuchungswerkzeuge. Caldwells Studie Production Culture war deshalb so einflussreich, weil sie einen ungewöhnlichen Methodenmix vorschlug: eine Kombination aus dichter ethnografischer Beschreibung (von Dreharbeiten, Branchentreffen, Workshops etc.) und kritischer Textanalyse (von Branchenblättern, Imagefilmen, Markenlogos, Making-ofs usw.). Die Beiträge des Sammelbandes gehen im direkten Vergleich zwar weniger ethnografisch vor. Dafür erweitern sie das Methodenarsenal der kulturwissenschaftlichen Produktionsforschung beträchtlich. Das geschieht in erster Linie durch umfangreiche Archivrecherchen, etwa zu feminisierter Büroarbeit im frühen Hollywood (Erin Hill), zu Drehberichten im westdeutschen Fernsehen der 1960er-Jahre (Theodor Frisorger und Dennis Göttel) oder zur "Produktion" von Fernsehpreisen (Tanja Weber). Bianka-Isabell Scharmann zeigt in ihrem Beitrag (inzwischen mit dem Karsten-Witte-Preis für den besten filmwissenschaftlichen Aufsatz des Jahres ausgezeichnet), wie Archivrecherchen den Blick außerdem auf Gestaltungsebenen wie das Kostümbild und Fragen von Überlieferung und Ausstellbarkeit lenken können. Qualitative Expert*innen-Interviews (Florian Krauß) oder Big-Data-Modellierungen (Skadi Loist und Zhenya Samoilova) sind weitere Methoden, die im Band für Produktionsstudien fruchtbar gemacht werden.


Zweitens widmen sich viele Beiträge bestimmten Personengruppen, denen bislang wenig akademische Aufmerksamkeit zuteilwurde. Das hat mutmaßlich auch damit zu tun, dass ihr Mitwirken an Filmen oder Fernsehproduktionen methodisch schwer zu greifen ist. Einzelne Aufsätze untersuchen Kompars*innen (Alexander Karpisek), Drehbuchautor*innen (Krauß) oder Kinder als Darsteller*innen (Bettina Henze). Interessanterweise ist die Figur, die am häufigsten in Erscheinung tritt, ausgerechnet der männlich vergeschlechtlichte Produzent. Als Angehöriger der oberen Management-Ebene kommt er in vielen US-amerikanischen Produktionsstudien nicht besonders gut weg. Die Autor*innen des Sammelbandes porträtieren aber keine allmächtigen Produktionssouveräne, die mit harter Hand über Geld, Menschen und Material gebieten. Produzenten erscheinen eher als Mittlerfiguren zwischen finanzstarken Geldgebern und Studiovermietern (Andreas Ehrenreich) oder zwischen Produktionsfirmen, Autor*innen und Sendern (Haydée Mareike Haass). Ebenso können Produzenten als Knotenpunkte feuilletonistischer Zuschreibungen untersucht werden (Johannes Praetorius-Rhein am Beispiel Arthur Brauner) oder lassen sich, wie Sven Grampp und Sophie Stiftinger in einem sehr lesenswerten Text demonstrieren, als emsige Anekdotenmaschinen verstehen, mit deren Hilfe ein besonderer Zugang zur Geschichte westdeutscher Fernsehunterhaltung möglich wird.


Eine dritte Stärke des Sammelbandes liegt am Interesse vieler Beitragenden, Begriffe und Ansätze der Production Studies einer kritischen Evaluation zu unterziehen. Caldwell beobachtet in seinem Beitrag zum Auftakt des Bandes, dass Ethnografie und Textanalyse kaum ausreichen, um den umfangreichen extractive schemes globaler Medienunternehmen auf die Spur zu kommen. Patrick Vonderau nimmt die Attribute 'mikro' und 'makro' in einführenden Darstellungen der Medienindustrieforschung kritisch unter die Lupe. Vinzenz Hediger fordert, die Production Studies um eine Distributionsperspektive zu ergänzen; Burrhus Njanjo nutzt Pierre Bourdieus Feldbegriff, um die Positionierung von Regisseur*innen mit Migrationsbiografie in "europäischen Filmfeldern" zu untersuchen. Eva Novrup Redvall erinnert schließlich daran, dass es instruktiv sein kann, den 'gescheiterten' statt nur den erfolgreich abgeschlossenen Film- und TV-Projekten nachzugehen.


Viertens fällt auf, dass für viele Autor*innen das, was produziert wird, wieder eine stärkere Rolle spielt. Das scheint deshalb relevant, weil in den Production Studies immer wieder nachdrücklich gefordert wurde, sich vom Fluchtpunkt des entstehenden Werkes zu lösen und den Produktionsprozess als Phänomen eigenen Rechts zu untersuchen. Demgegenüber unterstreicht Sara Malou Strandvad in ihrem Beitrag, dass auch das entstehende Werk als wirkmächtiger Akteur am Produktionsprozess beteiligt ist. Andere Autor*innen versuchen, in Filmen oder Serien Spuren ihrer Herstellung freizulegen, was besonders bei Johannes Binotto zu originellen Ergebnissen führt. Binotto interpretiert Rückprojektionen als Beispiel für (trick-)technische Vorrichtungen, die im klassischen Studiofilm eben nicht die immergleichen Bildergebnisse erzielen, sondern, ganz im Gegenteil, bei näherem Hinsehen sehr heterogene ästhetische Effekte hervorrufen. In Anlehnung an Georges Simondon versteht Binotto die Rückprojektion als offene Experimentalanordnung, in der sich ein "Unbewusstes" des Maschinenparks Hollywood manifestiert.


Binottos Interesse an einer Filmästhetik von technischen Verfahren bildet eine Brücke zu einer zweiten, neu erschienenen Aufsatzsammlung, die ebenfalls das Phänomen Produktion prominent im Titel trägt. Das Anliegen des Bandes PostProduktion. Bildpraktiken zwischen Film und Fotografie ist ein zweifaches, wie die Herausgeberin Fabienne Liptay in einem kurzen Vorwort ankündigt. Es geht zum einen um zeitgenössische Verhältnisse zwischen Film und Fotografie. Zum anderem versucht der Band, diese Verhältnisse speziell über die Bereiche Produktion und Postproduktion in den Blick zu nehmen.


Die 14 Beiträge beziehen sich unterschiedlich stark auf diese Zielsetzung. Teilweise diskutieren die Autor*innen die Medien Film und Fotografie ohne Rekurs auf Produktionsfragen; manchmal spielen Herstellungsverfahren auch ohne Verweis auf Relationen zwischen Film und Fotografie eine Rolle (zurecht merken viele Autor*innen an, dass zu dieser Beziehung ja auch schon einiges gesagt wurde). Aspekte von Postproduktion werden vor allem dort aufgerufen, wo es um fotografische Verfahren geht. So legen Friedrich Tietjen und Steffen Siegel in ihren Texten am Beginn des Bandes überzeugend dar, dass eine exakte Grenze zwischen Produktion und Postproduktion schon in der analogen Fotografie nicht präzise gezogen werden kann. Beide schließen mit der Diagnose, dass sich Produktion, eine (Weiter-)Arbeit am Bild, zunehmend in den Bereich der Bildbetrachtung verlagert. Diese Feststellung ist nicht unbedingt neu, Tietjen und Siegel versehen sie aber mit interessanten Begriffsvorschlägen, deren theoretisches Potenzial über die Aufsätze hinausweist. So schlägt Tietjen vor, Eingriffe ins Bild jenseits der Bildakquise als "Paraproduktion" (S. 25) zu verstehen; Siegel spricht anhand von Sebastian Riemers Press Paintings von einem "Make-up" der Fotografie durch andere mediale Formen, hier vor allem durch die Malerei.


Die nachfolgenden Beiträge nähern sich stärker den möglichen Relationen zwischen Fotografie und Film. Die Autor*innen folgen dabei nicht dem methodischen Paradigma der kulturwissenschaftlichen Production Studies; es geht strenggenommen auch wenig um konkrete "Praktiken" der Produktion oder Postproduktion, die im Untertitel des Bandes angekündigt werden. Eher entsprechen die Aufsätze klassisch-hermeneutischen Interpretationen, die jedoch besonderes Augenmerk auf Momente legen, in denen Filme oder Fotografien auf ihre eigenen Entstehungs- und Herstellungsbedingungen hinweisen. Produktion und Postproduktion werden also, zusammenfassend, vor allem als Fälle von filmisch-fotografischer Selbstreflexivität behandelt. Volker Pantenburg führt eine solche Lektüre mustergültig anhand von Morgan Fishers Kurzfilm Production Stills (US 1970) vor. Dabei beschränkt er sich darauf, die acht Fotografien des eigenen Produktionsgeschehens, die der Film vorführt, mit kurzen Theorievignetten zu versehen, wobei er umsichtig die gegenseitige (Selbst-)Beobachtung von Film und Fotografie im Moment ihrer Hervorbringung herausarbeitet. Erhellend sind auch andere close readings im Sammelband: Liptay zeigt mit Arbeiten von David Claerbout, Stan Douglas und Lech Majewski, wie in der digitalen Manipulation von fotografischen Bildern eine spezifische "Eigenzeit" der Postproduktion greifbar wird. Stefanie Diekmann widmet sich den Kadrierungen in Sergei Loznitsas Dokumentarfilm Maidan (UA/NL 2014); Maude Oswald untersucht multimediale Aufarbeitungen des Hurrikans Katrina. Achilleas Papakonstantis rekonstruiert das hierzulande kaum bekannte Format der Ciné-Tracts: filmische Flugblätter, die 1968 in Frankreich realisiert wurden, weitgehend aus abgefilmten Fotos und Zwischentiteln bestanden und auf eine 'Postproduktion' im herkömmlichen Sinne vollständig verzichteten. Burcu Dogramaci analysiert den Dokumentarfilm Un voyage américaine (R.: Philippe Seclier, FR 2009) als nachträgliches Quasi-Making-of von Robert Franks The Americans (1958) und bringt damit eine weitere mediale Form, nämlich das Fotobuch ins Spiel, das durch die serielle Anordnung der Bilder ein "filmisches Erleben" (S. 183) simuliert. Etwas aus dem Rahmen fällt lediglich ein langer Beitrag von Karl Prümm, der den gut erforschten Einsatz von DV-Kameras für Spielfilmproduktionen um 2000 nacherzählt und die dabei neu entstehenden Handkamera-Ästhetiken beleuchtet.


Der Band schließt mit vier Texten, die noch einmal um eine stärkere Theoretisierung der Beziehung zwischen Fotografie und Film bemüht sind. Andreas Kreul, Wolfgang Brückle, Sandro Zanetti und Oliver Fahle untersuchen jeweils das Vorkommen von Fotografien in (Spiel-)Filmen. Es geht zum einen um Fotos als bildhafte Requisiten innerhalb einer Filmszene, aber auch um Fotos als vorrübergehende Arretierung des filmischen Bewegungsbildes. Fahle untersucht mit Fotomontagen in Form eines Prologs oder Epilogs einen Spezialfall dieser stillgestellten Bilder. Er versteht sie als Manifestationen eines medialen Außen, das den jeweiligen Film jedoch nicht wie ein simpler Paratext umgibt, sondern trotz deutlicher ästhetischer Andersartigkeit in das Innere der filmischen Erzählung hineinführt.


Fahle berührt mit diesem Argument einen wichtigen Punkt. Offenbar existiert im Beziehungsgeflecht zwischen Film und Fotografie eine Art heimliches intermediales Primat des Films, das in verschiedenen Beiträgen des Bandes implizit mitläuft: Film kann fotografische Bilder als Fotos zur Anschauung bringen; umgekehrt ist das nicht ohne weiteres möglich. Fotografie kann filmische Bilder zwar ästhetisch referenzieren, wie Birk Weiberg in seinem Text zu Cindy Shermans Serie Film Stills (und ihrem Remake durch James Franco) beispielhaft zeigt. Fotografie vermag es aber nicht direkt, Filme als solche in ihr eigenes Bild zu integrieren. Denn dann wäre ein Foto kein unbewegtes Standbild mehr.


Die Beiträge des Sammelbandes demonstrieren insgesamt, dass die Frage nach dem Verhältnis zwischen Film und Fotografie immer noch theoretisch belastbar ist – und gerade anhand konkreter Werkanalysen zu anregenden Einsichten führt. Auch wenn nicht alle Texte die Beziehungen zwischen Film und Fotografie über die Analyse konkreter Herstellungspraktiken aufschlüsseln, durchzieht den Band ein spürbares Interesse daran, die Produktion von Filmen und Fotografien nicht einfach als gegeben vorauszusetzen. Insofern bildet die Publikation ein passendes Gegenstück zum stärker an tatsächlichen, historischen wie aktuellen Produktionskonstellationen ausgerichteten Band Produktionskulturen audiovisueller Medien. Beide Veröffentlichungen machen deutlich: Studien zur Herstellung audiovisueller Medien werden gerade dann produktiv, wenn ästhetische Fragen gegenüber einer Beschreibung von empirischen Entstehungsprozessen nicht vernachlässigt werden.


Es wäre spannend, diese Spielart von Produktionsforschung noch "weiterzutreiben", wie es Udelhofen, Göttel und Riffi formulieren. Eine mögliche Richtung liegt in zeitgenössischen Herstellungsverfahren, die in beiden Bänden angedeutet, aber kaum direkt adressiert werden: Digitale Techniken und Praktiken, nicht nur solche der Postproduktion, bringen längst eigene Produktionsästhetiken hervor, die dringend einer tiefergehenden Untersuchung bedürfen. In diesem Sinne wäre den beiden anregenden, engagierten Publikationen unbedingt ein Sequel zu wünschen.


 


Anmerkungen:


1 Siehe hierzu die Debattenbeiträge in der Rubrik "Methoden der Medienwissenschaft" auf der Homepage der Zeitschrift für Medienwissenschaft (https://zfmedienwissenschaft.de/online/debatte/methoden-der-medienwissenschaft).


Literatur:


Caldwell, John Thornton: Production Culture. Industrial Reflexivity and Critical Practice in Film and Television. Durham/London: Duke University Press 2008.


Flusser, Vilém: Gesten. Versuch einer Phänomenologie. Frankfurt a. M.: Fischer 1994.
</description>
  </descriptions>
  <relatedItems>
    <relatedItem relationType="IsPublishedIn" relatedItemType="Journal">
      <relatedItemIdentifier relatedItemIdentifierType="EISSN">2072-2869</relatedItemIdentifier>
      <titles>
        <title>[rezens.tfm]</title>
      </titles>
      <issue>2024/2</issue>
    </relatedItem>
  </relatedItems>
</resource>
