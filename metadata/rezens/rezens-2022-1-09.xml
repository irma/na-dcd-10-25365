<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2022-1-09</identifier>
  <creators>
    <creator>
      <creatorName>Wagner, Franzi</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Elisa Linseisen: High Definition. Medienphilosophisches Image Processing.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2022</publicationYear>
  <dates>
    <date dateType="Submitted">2021-03-17</date>
    <date dateType="Accepted">2022-05-05</date>
    <date dateType="Updated">2022-05-18</date>
    <date dateType="Issued">2022-05-18</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-592-5974</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Inwiefern hochauflösende Bilder politisch sind, zeigt sich aktuell im Angriffskrieg Putins gegen die Ukraine, bei dem die USA eben solche hochauflösenden Bilder aus Butscha als Evidenz für das Massaker verwendeten, um zu beweisen, dass die Leichen bereits vor dem Abzug von Russlands Truppen auf den Straßen der Stadt lagen. Die hochaufgelösten und mit timestamp versehenen Satellitenbilder wurden so als Evidenz für die Verbrechen an der zivilen Bevölkerung Butschas genutzt. Nach der Evidenzführung durch die Satellitenbilder warf Russland den USA/dem Westen vor, die Bilder seien manipuliert. Genau innerhalb dieses Spannungsfeldes zwischen hochaufgelöster Evidenz und manipulierbarem, weil digitalem, Bild operieren auch die Diskussionen um Wirklichkeit in Elisa Linseisens Buch High Definition. Medienphilosophisches Image Processing.


Linseisen beschäftigt sich in dem Buch, das auch ihre Dissertationsschrift ist, mit dem miteinander Denken der drei im Titel genannten Themenfelder. Dabei scheinen Image Processing, also digitale Bildbearbeitung, und HD auf den ersten Blick nicht allzu viel mit philosophischen Fragestellungen zu tun zu haben. Der titelgebende Begriff High Definition meint im Falle Linseisens jedoch weniger hochauflösendes Fernsehen, oder Versprechungen eines perfekten Bildes – was womöglich die ersten Assoziationen mit HD sind – als eben die vielmehr weitergehenden philosophischen Fragen und insbesondere jene Diskurse um Wirklichkeit und Welt, die mit Hochauflösung einhergehen: „HD bekundet das Engagement an der Wirklichkeit über Praktiken und Prozesse des Digitalbildes, als medienphilosophisches Image Processing“ (S. 14).


Der Autorin gelingt es überdies, den Diskurs um HD zu politisieren indem sie klar macht, dass Medienwissenschaft auch immer eine politische Dimension inhärent ist.  So betont Linseisen etwa, dass HD und Image Processing selbstverständlich auch mit der Frage von Sichtbarkeit(sgrenzen) zu tun haben. Genau deshalb sei "Image Processing nicht nur ein bildbearbeitendes Verfahren, sondern ein sich eröffnender Denkraum medienphilosophischer Operationen" (S. 11).


Insgesamt lässt sich fast sagen, dass Linseisen in ihrem Buch einen eigenen theoriegeprägten (Gedanken-)Kosmos – quasi in Analogie zu ihrem Buchcover – entwirft. Damit einher geht eine relativ hohe Komplexität der Gedankengänge und relationalen Verstrickungen, sodass es schwer fällt, das Buch kurz zusammenzufassen. Die Kapitel sind eng miteinander und ineinander verschachtelt, weshalb ich in meiner Rezension vielmehr subjektiv spotlightartig einzelne Themenkomplexe beschreibe, um an ihnen die Tragweite der von Linseisen erarbeiteten Thesen zu illustrieren und einen Eindruck zu dem Werk zu vermitteln, der jedoch keinerlei Vollständigkeit beansprucht.[1]


 


Form/Inhalt


Form und Inhalt sind in High Definition eng miteinander verwoben – das Buch besteht aus insgesamt sechs Kapiteln, die ineinandergreifen und sich prozessual entwickeln. Jedes Kapitel, abgesehen vom Fazit, beginnt mit einem Fallbeispiel, das die jeweilige Ausrichtung veranschaulichen soll. Die Fallbeispielauswahl setzt sich dabei aus Spielfilmen (The Tree of Life), politischen Dokumentarfilmen (El botón de nácar, Nostalgia de la luz), Videoarbeiten (How Not to Be Seen. A Fucking Didactic Educational.mov.file, Phantom Trades: Sea of People), und digitaler (Cartoon-) Animation (3´´) zusammen. Gemeinsam haben all diese Werke, dass sie in HD produziert wurden. Neben diesen rahmenden Fallbeispielen werden jedoch auch weitere Werke (bspw. Serien, Desktop Documentaries, Internetkunst und Fotografien), aktivistisch/künstlerische Gruppierungen (Forensic Architecture) oder Programme (Prezi), argumentativ mit verwoben. Dies führt dazu, dass eine klassische Trennung zwischen Theorie und Fallbeispielen unmöglich wird: die Binarität zwischen Theorie und Praxis wird dekonstruiert.


Das Buch beginnt mit dem Kapitel "High Definition", in dem erste Definitionen zu den relevanten Konzepten und Themen, sowie Einordnung innerhalb der Diskurse geliefert werden, danach geht es über zu den Kapiteln "Post/Produzieren", "Um/Formatieren", "Interpolieren", "Epistemologisch/Zoomen" und schließlich "Hochaufgelöste Ereignis/Horizonte". Es fällt dabei auf, dass allein durch die Benennung der einzelnen Kapitel Linseisens eigenes prozessuales Denken und Schreiben mitschwingt, weil alle jeweils genannten Begriffe eng miteinander in Verbindung gebracht werden – ein Prozess, den Linseisen auch autoreflexiv thematisiert. Nur die Einleitung "High Definition" und das mittlere Kapitel "Interpolieren", das als eine Art Zwischenbilanz/-berechnung gesehen werden kann, kommen ohne Slashes aus und könnten daher als weniger relational austangierend, denn als eigenständige (Zwischen)Schritte/Schnitte gelesen werden. Dass das letzte Kapitel hingegen wieder mit einem Slash arbeitet, deutet an, dass Linseisens Buch womöglich weniger linear gedacht werden sollte, sondern als eröffneter Horizont, der sich jeglicher Form von Abgeschlossenheit, Finalität und Stasis entzieht. Mit Medien zu denken und ein Denken der Medien funktioniert im Buch so auf wiederum verschiedenen medialen Ebenen.


 


/Slash/Schrägstriche/Schnitte


Direkt zu Beginn von High Definition schreibt Linseisen zu den Schrägstrichen/Slashs: Da es um verschiedene Verhältnisbestimmungen zwischen Welt und Digitalbild geht, die variierend Austausch-, Wechsel- oder auch Kongruenz-Beziehungen sind, sollen die Operationen, Prozesse und Diskursivierungen dieser Verhältnisse mit einem Schrägstrich markiert werden. Mit dem Schrägstrich können genealogische und konzeptuelle Bezüge offengelegt werden, […] V.a. aber hält der Schrägstrich Bedeutungen offen und unterstellt so auch eine von mir vorgenommene Benennung von Phänomenen oder eine Adaption von Konzepten einer Prozessualität. (S. 16)


Linseisen bezieht sich mit der Verwendung des Slashs zudem auf Karen Barads Konzept des "agentischen Schnitts" (Barad, S. 131). Barads Schnitt sei laut Linseisen, mit Rückgriff auf Lisa Handel, "wie ein Slash verfasst, der Unbestimmtheit niemals vollständig auflöst, sondern Un/Bestimmtheiten rekonfiguriert" (Linseisen, S. 69; bei Handel S. 248). Dies erinnert zum einen an Überlegungen aus den Gender Studies zu Neologismenund das Verhältnis von Sprache und Wirklichkeit (vgl. z.B. Hornscheidt: feministische w_orte). Zum anderen lassen sich aber auch Parallelen zu Überlegungen zum Schnitt aus den Trans/Cinema Studies ziehen, so wie beispielsweise Eliza Steinbock in deren[2] Buch Shimmering Images Zusammenhänge des Schnitts mit Momenten der Transfiguration und später auch Transition beschreibt: "With Shimmering Images: Trans Cinema, Embodiment, and the Aesthetics of Change, I venture that the cinematic cuts and sutures between the visual and the spoken, between frames, and between genres are delinking and relinking practices of transfiguration" (S.2). Steinbock schlägt etwas Ähnliches vor wie Linseisen, indem beide sich auf Barads agientiellen Schnitt beziehen und die Schnitte als relational – gleichsam als Differenz und Zusammenhang – verstehen. Steinbock arbeitet dabei mit drei Modellen, um trans* gemeinsam mit Schnitt zu denken: "Each conceptual model is accented and highlighted by a typographic sign, namely, the cut of the forward slash (/), the suture of the hyphen (-), and the multiplier of the asterisk (*)" (S. 20). Dey verwendet diese Satzzeichen explizit als ein konzeptuelles Gerüst zum Nachdenken über trans* Thematiken in Filmen. Linseisens Buch ist hingegen inhaltlich breiter aufgestellt.


 


Non/Binärität/Nicht-binäres Denken


So könnte man High Definition als Anregung zu einem nicht-binären Denken innerhalb der Medienwissenschaft nutzen, was bereits innerhalb des Werkes mitklingt, wenn die Autorin u.a. schreibt "HD ist medial unspezifisch. […] HD steht transversal zu Mediengrenzen genauso wie zu unterschiedlichen Bild(verwendungs)kontexten: jenseits von Kunst/Nicht-Kunst, jenseits von Kino/Nicht-Kino, jenseits von HD und Low Definition" (S.23). HD ist also viele/s. Gesagt ist damit auch, dass etwa die Klassifikationen medialer Formen wie Fernsehen, Film, Fotografie oder Video nicht weiterhelfen, wenn es um die Möglichkeiten von HD geht. Dessen Potenziale liegen vielmehr in der Gleichzeitigkeit und im auf den ersten Blick Getrennten oder gar Konträren. Denn binäre Grenzziehungen bzw. binäres Denken übersieht die Nuancen des Dazwischen und Daneben, es essenzialisiert und normiert. Wenn Linseisen also jenseits der Mediengrenzen von HD schreibt, eröffnet sich sogleich ein Möglichkeitsraum für nicht-binäre Fragestellungen, also Fragen, die die Binarität von bisher dualistischen Begriffskonzeptionen auflösen und jenseits dieser entweder/oder und 0|1-Logiken liegen. So schließen Linseisens Gedanken nicht nur an aktuelle politische Diskussionen an, sondern auch an andere nicht-binäre wissenschaftliche Auseinandersetzungen wie Meg-John Barkers und Alex Iantaffis Life isn’t Binary, das Phänomene und Konzepte als "Landschaften" oder "Spektren" behandelt. Nicht binär zu denken ist dabei möglicherweise eine der großen Herausforderungen des aktuellen geisteswissenschaftlichen Denkens, denn es spiegelt sich etwa in der Unmöglichkeit zur medialen Kategorisierung bzw. der Irrelevanz medialer Abgrenzungen wider, sowie in der steten Betonung von Prozessualität, die statt von Grenzen eher von Übergängen spricht. Insofern nimmt Linseisens Dissertation in gewisser Weise auch Abschied vom vermeintlich apolitischen medienwissenschaftlichen Denken.


 


Literatur


Barad, Karen: Verschränkungen, Berlin: Merve 2015.


Barker, Meg-John/Iantiaff, Alex: Life Isn’t Binary. On Being Both, Beyond, and In-Between. London/Philadelphia: Jessica Kingsley 2019.


Handel, Lisa: Ontomedialität: Eine medienphilosophische Perspektive auf die aktuelle Neuverhandlung der Ontologie. Bielefeld: Transcript 2019.


Hornscheidt, Lann: feministische w_orte: ein lern-, denk- und handlungsbuch zu sprache und diskriminierung, gender studies und feministischer linguistik. Frankfurt a. M.: Brandsel &amp; Apsel 2012.


Steinbock, Eliza: Shimmering Images. Trans Cinema, Embodiment, and the Aesthetics of Change. Durham/London: Duke University Press 2019.


 


[1] Wie unterschiedlich die Art und Weise mit dem Buch zu denken sein kann, zeigte sich in gemeinsamen, weniger hochaufgelösten, Zoom-Gesprächen zwischen Angela Rabing und mir. Das Denk/Schreib-Resultat zeigt sich in den sich überschneidenden, gleichzeitig unterschiedlichen – mit Linseisen geschrieben ‚überschneidend/unterschiedlichen‘ – Rezensionen zum Buch. Angela Rabings Rezension ist hier zu finden: https://nachdemfilm.de/index.php/reviews/hd-geht-es-um-die-wirklichkeit.


[2] Eliza Steinbock ist nicht-binär und verwendet im Englischen die Pronomen they/them. Im Deutschen gibt es eine Vielzahl an Neopronomen, von denen sich bisher keines durchgesetzt hat. Im Folgenden werden daher die deutschen Neopronomen dey/deren als Äquivalent zu they/them verwendet.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2022/1</description>
  </descriptions>
</resource>
