<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2021-1-12</identifier>
  <creators>
    <creator>
      <creatorName>Heine, Janna</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Alena Strohmaier: Medienraum Diaspora  Verortungen zeitgenössischer iranischer Diasporafilme.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2020-12-22</date>
    <date dateType="Accepted">2021-05-05</date>
    <date dateType="Updated">2021-05-20</date>
    <date dateType="Issued">2021-05-20</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-464-5336</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Alena Strohmaier prüft und novelliert in ihrer Dissertationsschrift den Begriff "Diasporafilm" aus einer raum- und kulturtheoretischen Perspektive. Am Beispiel zeitgenössischer iranischer Diasporafilme fragt sie, welche Bedeutung das Konzept Diaspora angesichts globaler gesellschaftlicher und medialer Transformationsprozesse heute habe. Sie leistet damit einen Beitrag zur transnationalen Film- und Medienwissenschaft. Bislang federführend im angelsächsischen Raum diskutiert, gewinnen diese transnational studies zunehmend auch im deutschsprachigen Raum an Sichtbarkeit (vgl. Ritzer 2018; vgl. Christen/Rothemund 2019). Strohmaier zeigt in ihrer Arbeit wechselseitige gesellschaftliche und filmische Wandlungsprozesse am Beispiel der iranischen Diaspora auf und erprobt ihre Idee von diasporafilmischen Räumen schließlich filmanalytisch. Unter diasporafilmischen Räumen versteht sie "filmische Räume, in denen Diasporakultur verstärkt, quasi wie durch ein Vergrößerungsglas, verhandelt wird" (S. 108).


In ihrem zweiten Kapitel "Diaspora/Film im Wandel" verschränkt Strohmaier den Forschungsstand zur Diasporatheorie dafür mit topografischen Ansätzen. Sie zeichnet Verbindungslinien zu einigen verwandten Filmtheorien, beispielsweise zu "nationale[n] (exilic cinema; migrant cinema), sozio-politische[n] (third cinema; third world cinema), ökonomische[n] (world cinema), transnationale[n] (transnational cinema; cinema of transvergence) oder ästhetisch-stilistische[n] (accented cinema; intercultural/haptic cinema)" (S. 48, Herv. i. O.) Perspektiven. Den Diasporafilm mit einer der bereits etablierten Theorien vollständig zu fassen, scheitert jedoch – laut der Autorin – entweder an Unter- oder Überkomplexität. So kritisiert sie etwa das eindimensionale Medienverständnis des diasporischen accented cinema (vgl. Naficy 2011), demnach Film eine vermittelnde Funktion habe dafür, dass es in der binäroppositionellen Konstruktion von Herkunfts- und Heimatland verhaftet bleibe (vgl. S. 55). Als Medienästhetikerin ist es Strohmeier nämlich wichtig, Film nicht nur als Repräsentationsform, sondern auch als Reflexionsmöglichkeit zu verstehen: Film lasse uns über Seinsweisen nachdenken und bringe selbst neue – antiessentialistische – Arten des Seins hervor.
Deshalb umreiße aus ihrer Perspektive auch das polyzentrische Konzept des "cinema of the world" (Nagib 2006, S. 31) das Diasporakino nur teilweise (vgl. S. 57):  Am Beispiel von Deborah Shaws fünfzehnteiliger Liste von Analysemöglichkeiten (vgl. Shaw 2013, S. 52), die von Produktionsmodi bis hin zu ethischen Fragestellungen reichen, kritisiert Strohmaier dessen Überkomplexität und mithin die fehlende Spezifik des Begriffs transnational cinema. Sie merkt zudem an, dass sich der Terminus vor allem für die Untersuchung von Produktions-, Rezeptions- und Distributionsmechanismen durchgesetzt habe und Differenzen zwischen nationalen und transnationalen Bereichen häufig überspiele (vgl. S. 55). Als produktiver erachtet sie daher Will Higbees Verständnis eines cinema of transvergence (vgl. Higbee 2007), das kulturelle Differenz nicht glattbügle, sondern betone. Durch den zelebrierten Zwischenraum, der sich in Higbees Ansatz abzeichnet, gelangt Strohmaier schließlich zu einem für ihren eigenen Ansatz produktiven Diasporaverständnis, das sich als Raum zwischen Nationen, Kulturen oder transnationalen Bewegungen denken lässt.
Dabei versteht die Autorin Raum nicht als einen gesetzten geografischen Ort, sondern ein bewegliches Konstrukt, das sich im reziproken Austausch mit sozialen, kulturellen oder medialen Entwicklungen stetig verändere. Film betrachtet Strohmaier darin "als Verhältnis, Beziehung oder auch Verbindung zwischen Kulturen" (S. 59) und schreibt diesem eine räumliche Logik zu, wie sie spätestens seit dem spatial turn an Bedeutung gewonnen hat. Im Rekurs auf Brigitte Hipfls (2004) Konzept des Zwischenraums entwirft Strohmaiers Arbeit eine diasporafilmische Topografie, in der sich eine besondere Verbindung zwischen filmischen und nicht-filmischen Welten abzeichne. Die Art und Weise, wie Medien in der Diaspora genutzt würden, brächte demnach neue, symbolische Räume hervor, in denen sich Diaspora selbst verhandle und (neu) figuriere. Im Anschluss an die Filmphilosophin Laura Frahm betont Strohmaier  so die unbegrenzten Anschlüsse und Reihenfolgen sowie mitunter paradoxe Raummodelle, die sich aus dem Ineinanderfallen von filmischen und materiellen Raum ergeben, wodurch filmischen Räumen das Potenzial zur Veränderung stets inhärent sei (vgl. Frahm 2021, S. 272). Strohmaier behauptet nun in ihrer Publikation, diese filmeigene Raumlogik lasse sich in zeitgenössischen iranischen Diasporafilmen verstärkt vorfinden, wodurch unter dem Begriff Diasporafilm vor allem Produktionen gefasst werden könnten, die eine erhöhte Raum- und Mediensensibilität aufwiesen (vgl. S. 62).


Bevor sich Strohmaier dem Close Reading der Filme zuwendet, um ihre These der erhöhten Raum- und Mediensensibilität zu prüfen, vollzieht sie im dritten Kapitel mit dem Titel "Verortung der iranischen Diaspora" ein Cross Reading zwischen Kultur- und Medienwissenschaft sowie den Postcolonial Studies, das die Parallelen zwischen den Disziplinen besonders eindrücklich zum Vorschein bringt. Im Rekurs auf Meilensteine dieser Ansätze veranschaulicht Strohmaier etwa mit Edward Saids Orientalism (1979) die Konstruiertheit von "kulturgeographischen Entitäten, wie auch Diaspora eine ist" (S. 74), die meist auf stereotypen Zuschreibungen basiere. Als dynamisches und transformatives Gegenbeispiel zum stereotypen Kulturbegriff nennt Strohmaier außerdem Arjun Appadurais Vorstellung von global scapes als mediascapes. Auch Homi K. Bhabas Idee des "dritten Raums" (2004), den dieser als eigenständigen Zwischenraum versteht, in dem kulturelle Differenz nicht aufgelöst, sondern produktiv gewendet werde, scheint hier auf. Um Diaspora als eigenständige Form kultureller Identität greifen zu können, folgt Strohmeier mithin Stuart Hall (1990), der kulturelle Identität nicht als gegeben und abgeschlossen, sondern als Prozess versteht. Ein diasporisches Selbstbewusstsein manifestiere sich noch "stärker entlang von Differenzen, Brüchen und Diskontinuitäten" (S. 86).


Die Idee von Diaspora als Medienraum erprobt die Autorin im Kapitel vier, "Neue diasporafilmische Räume", dann an sechs Spiel- beziehungsweise Dokumentarfilmen, die in Europa und Nordamerika entstanden und situiert sind. Sie arbeitet drei zentrale Raumkonzepte heraus: Zwischenräume, kosmopolitische und rebellische Räume.


Mit Women without Men (2009) und A Girl Walks Home Alone At Night (2014) widmet sie sich den Zwischenräumen über das Motiv des Gartens respektive der fiktiven Stadt Shahr-i bad. Der Garten fungiere als feministischer, dritter und filmisch-somatischer Raum und damit auch als Schnittstelle zwischen Sozial- und Medienkritik. Shahr-i bad erweise sich als filmisch-hybrider Raum, in dem kulturelle und generische Zuschreibungen kollabieren. In diesen Zwischenräumen drücke sich einerseits die Herausforderung kultureller Verortung aus, andererseits würden diese gerade deshalb filmische Wandlungsprozesse anstoßen.


Um Fragen der Gemeinschaft geht es in der Besprechung kosmopolitischer Räume. Mit Walls of Sand (2001) sowie Persepolis (2007) entdeckt Strohmaier eine cosmopolitan attitude nach den Soziolog*innen Steffen Mau, Jan Mewes und Anne Zimmermann (2008). Als kosmofeministischer Raum sei Walls of Sand etwa als Aushandlungsort geteilter Erfahrungen der Protagonistinnen zu verstehen und mit Avtar Brahs diaspora space (1996) als Ort, der beispielsweise aufgrund ihrer Geschlechtsidentität marginalisierte Gruppen einschließt. Im Animationsfilm Persepolis hingegen werde, entsprechend des Konzepts des Pop-Kosmopolitismus nach Henry Jenkins (2006), der filmische Raum durch popkulturelle Elemente wie Musik, Kleidung oder auch Sprache stetig erneuert und durch Gleichzeitigkeit, Verschachtelung und Ambivalenz in Beziehung zu anderen Räumen gesetzt. "Identität wird darin als immer wieder, synchron und diachron, austarierende Beziehung zur Welt, dem Kosmos, gesehen" (S. 224). Weniger dem Kosmos als dem Politischen zugewandt ist Strohmaiers Analyse der rebellischen Räume in den Dokumentarfilmen The Green Wave (2010) und Schwarzkopf (2011). The Green Wave montiert unter anderem Interviewaufnahmen und Amateurvideos, die im Zuge der Grünen Revolution in Iran 2009 auf Straßenprotesten entstanden sind, hinzu kommen Motion Comics (eine Mischung aus Zeichnung und Animation), mit denen Textpassagen einzelner Blogeinträge und Tweets visualisiert und von Schauspieler*innen eingesprochen werden. Gesellschaftliche und kulturelle Veränderungen des Irans würden hier zur filmischen Auseinandersetzung über Staats- und Mediengrenzen hinaus und brächten Solidarisierung und Handlungsmöglichkeiten als mediale Praktiken hervor. 


Um Handlungsmöglichkeiten geht es Strohmaier auch in ihrer Analyse von Schwarzkopf. Das in diesem Film im Mittelpunkt stehende Tonstudio, in dem drei Generationen männlicher Rapper mit prekären sozialen Hintergründen ihre Stimme erheben, sei als rebellischer Raum konzipiert, indem Herkunft und gesellschaftliche Teilhabe entkoppelt wären und so ein post-migrantischer Raum entstehe. Mit Schwarzkopf leitet Strohmeier zugleich in ihr Fazit ein, in dem sie die Aktualität des Begriffs Diasporafilm abschließend prüft. Folge man Michel Laguerre (2017), so stünde Diaspora für gesellschaftliche Ausgrenzung und Marginalisierung, während Postdiaspora deren Überwindung ausdrücke (vgl. S. 237). Strohmaier kommt daher zu dem Schluss: Wenn nun also die in dieser Arbeit analysierten diasporafilmischen Räume als Weiter-entwicklung bereits bestehender filmischer Räume zu sehen sind, die zunehmend kulturelle und filmische Zuschreibungen verunmöglichen, dann kann möglicherweise von Postdiaspora-Film gesprochen werden […]. (S. 238) Damit öffnet Strohmaier zugleich die Tür für zukünftige Auseinandersetzungen mit dem Postdiaspora-Film. Die Lektüre der Dissertationsschrift empfiehlt sich jedoch nicht nur Diasporaforschenden, sie vermittelt darüber hinaus Grundlagen und Dringlichkeit einer dekolonialen Filmwissenschaft, wie sie beispielsweise bereits im Sammelband De-Westernizing Film Studies von Saër Maty Bâ und Will Higbee (2012) gefordert wird.


 


Literatur:


Appadurai, Arjun: Modernity at Large: Cultural Dimensions of Globalization. Minneapolis/London: University of Minnesota Press 1996.


Bâ, Saër/Higbee, Will (Hg.): De-Westernizing Film Studies. Abdingdon: Routledge 2012.


Bhabha, Homi K.: The Location of Culture [1994]. London: Routledge 2004.


Brah, Avtar: Cartographies of Diaspora: Contesting Identities. London/New York: Routledge 1996.


Christen, Matthias/Rothemund, Kathrin (Hg.): Cosmopolitan Cinema. Kunst und Politik in der zweiten Moderne. Marburg: Schüren 2019. 


Frahm, Laura: "Logiken der Transformation: Zum Raumwissen des Films". In: Raum Wissen Medien. Zur raumtheoretischen Reformulierung des Medienbegriffs. Hg. v. Dorit Müller/Sebastian Scholz. Bielefeld: transcript 2012, S. 271–302.


Hall, Stuart: "Cultural Identity and Diaspora". In: Identity: Community, Culture, Difference. Hg. v. Jonathan Rutherford. London: Lawrence &amp; Wishart 1990, S. 222–237. http://www.gurunanakcollegeasc.in/userfiles/Stuart%20Hall%20Identity.pdf, abgerufen am 20.04.2021.


Higbee, Will: "Beyond the (trans)national: towards a cinema of transvergence in postcolonial and diasporic francophone cinema(s)". In: Studies in French Cinema 7/2, 2007, S. 79-91.


Hipfl, Brigitte: "Mediale Identitätsräume: Skizzen zu einem 'spatial turn' in der Medien- und Kommunikationswissenschaft". In: Identitätsräume. Nation, Körper und Geschlecht in den Medien. Eine Topografie. Hg. v. Brigitte Hipfl/Elisabeth Klaus/Uta Scheer. Bielefeld: transcript 2004, S. 16-50.


Jenkins, Henry: "Pop Cosmopolitanism: Mapping Cultural Flows in an Age of Media Convergence". In: Fans, Bloggers, and Gamers: Exploring Participatory Culture. New York: New York University Press 2006, S. 152–172.


Laguerre, Michel S.: The Postdiaspora Condition: Crossborder Social Protection, Transnational Schooling, and Extraterritorial Human Security. London: Palgrave MacMillan 2017.


Mau, Steffen/Mewes, Jan/Zimmermann, Ann: "Cosmopolitan Attitudes through Transnational Social Practices?". In: Global Networks 8/1, 2008, S. 1–24.


Naficy, Hamid: An Accented Cinema. Exilic and Diasporic Filmmaking. Princeton: Princeton University Press 2001.


Nagib, Lúcia: "Towards a Positive Definition of World Cinema". In: Remapping World Cinema: Identity, Culture and Politics in Film. Hg. v. Stephanie Dennison/Song Hwee Lim. London/New York: Wallflower 2006, S. 26–33..


Ritzer, Ivo: Medientheorie der Globalisierung. Wiesbaden: Springer Verlag für Sozialwissenschaften 2018.


Said, Edward W.: Orientalism. New York: Random House 1979.


Shaw, Deborah: "Deconstructing and reconstructing ‘transnational cinema'". In: Contemporary Hispanic Cinema. Interrogating the transnational in Spanish and Latin American Film. Hg. v. Stephanie Dennison. Suffolk: Boydell &amp; Brewer 2013, S. 47-66. https://core.ac.uk/download/pdf/29584501.pdf, abgerufen am 19.04.2021.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2021/1</description>
  </descriptions>
</resource>
