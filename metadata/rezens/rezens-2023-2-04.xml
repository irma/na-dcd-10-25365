<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2023-2-04</identifier>
  <creators>
    <creator>
      <creatorName>Schütz, Theresa</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Hannah Speicher: Das Deutsche Theater nach 1989. Eine Theatergeschichte zwischen Resilienz und Vulnerabilität.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2023</publicationYear>
  <dates>
    <date dateType="Submitted">2023-06-20</date>
    <date dateType="Accepted">2023-10-19</date>
    <date dateType="Updated">2023-11-14</date>
    <date dateType="Issued">2023-11-14</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-650-8066</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Lässt sich eine Theaterinstitutionsgeschichte entlang prägnanter Inszenierungen erzählen? Und lassen sich diese Inszenierungen, eingebettet in das Wissen um ihr Zustandekommen sowie ihr diskursives Nachleben, mit ihren ästhetischen Besonderheiten und konzeptuellen Setzungen sogar auch zu "(Sinn-)Bildern einer Intendanz" verdichten? Dass dem in der Tat so sein kann, ist die starke These der Literatur- und Theaterwissenschaftlerin Hannah Speicher und ihrer Dissertationsschrift Das Deutsche Theater nach 1989. Eine Theatergeschichte zwischen Resilienz und Vulnerabilität (transcript 2021).


Entlang der drei Inszenierungen Hamlet/Maschine von Heiner Müller (1989), Shoppen und Ficken von Thomas Ostermeier (1998) und Emilia Galotti von Michael Thalheimer (2001), die in drei verschiedenen Intendanzen der Nach-/Wendezeit am Deutschen Theater in Berlin unter Dieter Mann (1984–1991), Thomas Langhoff (1991–2001) und Bernd Wilms (2001–2008) entstanden sind, zeichnet die Autorin die Transformationsprozesse dieses geschichtsträchtigen Hauses "vom selbstbewussten Staatstheater der DDR zu einem pluralistischen Theater in neuer Bundeshauptstadt" (S. 14) nach. Ihr Anspruch: einen Beitrag zur theaterhistorischen Organisationsforschung zu leisten. Und dabei in den Analysen von der grundlegenden Verwobenheit von Ästhetik, Produktionsbedingungen und kulturpolitischem Entstehungskontext auszugehen. Allein dieser Zugriff macht Speichers Studie methodologisch betrachtet schon sehr lesenswert.


Sie kombiniert Inszenierungs-, Dramen- und Diskursanalyse mit empirischen Methoden der Oral History. Hierfür hat sie Interviews mit Zeitzeug*innen des Deutschen Theaters geführt, die zwischen 1989 und 2008 in der Dramaturgie, der Geschäftsführung, dem Ensemble oder der Presse- und Öffentlichkeitsarbeit tätig waren. Mit ihrem Erkenntnisinteresse an der exemplarischen Theaterarbeit eines ostdeutschen Stadttheaters, seinen Produktionsbedingungen und künstlerischen Hervorbringungen schließt sie thematisch an die jüngeren Studien von Tanja Bogusz zu den "Ost-West-Transformationen an der Berliner Volksbühne" (2007) und Skadi Jennicke über "ostdeutsches Theater nach dem Systemumbruch" (2011) an und berührt auch das Feld der Theaterarbeitsforschung, um das sich Axel Haunschild mit zahlreichen Publikationen seit mehr als zwei Dekaden verdient gemacht hat.


Ein knapp gehaltenes Hinführungskapitel zu den Unterschieden zwischen dem Theatersystem der DDR und dem der BRD trägt primär den existierenden Forschungsstand zusammen und legt offen, dass die spezifische Geschichte sowie die Produktionsbedingungen des DDR-Theaters noch der gleichberechtigt intensiven Beforschung harren. Im Anschluss widmet sich die Autorin in drei umfassenden, und je analog strukturierten Kapiteln den Analysen der genannten Beispielinszenierungen des Deutschen Theaters (DT), wobei sie sehr elegante, leitmotivische Querverbindungen zwischen den Arbeiten herausstellt.


Aus der in Theater- und Literaturwissenschaft reichhaltig beforschten Inszenierung Hamlet/Maschine trägt Speicher einschlägige Perspektiven zusammen, um mit Blick auf ihre Kernthese den Punkt zu machen, dass mit Heiner Müllers Arbeit – vor dem Hintergrund der historischen Zeitenwende des Mauerfalls 1989 und der Wiedervereinigung 1990 – eine Phase am DT begann, die mit den Intendanzen Mann und Langhoff auf Aufrechterhaltung des Status Quo setzte. Dazu gehört organisationsbezogen z. B., dass die (im Vergleich zu westdeutschen Theatern) personelle Überausstattung vorerst erhalten werden konnte, dass die Wirksamkeit der DDR-Rahmenkollektivverträge, die Schauspieler*innen unbefristete Anstellung sicherte, bestehen blieb und dass im Haus – wenig verwunderlich – versucht wurde, die "ostdeutsche Identität des DT" (S. 58) zu bewahren.


Es ist vor allem ein bestimmter "Wendekommentar" (Kap. 3.4), den Speicher aus Müllers Hamlet/Maschine, aus dem Bühnenbild und dem Umgang mit Zeit und Geschichtlichkeit (über die Spieldauer entwickelt sich die Szenerie von einer Eislandschaft zu einer Wüste) sowie Fortschrittspessimismus ("von einer Knechtschaft in die andere", Müller in Krieg ohne Schlacht, zitiert nach S. 119) herausarbeitet. Dieser wird dann gleichsam zu einem Bild für die "Beharrung" (S. 58) und den "ausbleibenden organisatorischen Wandel des DT in den Jahren 1989–1997" (S. 120), der Mann/Langhoff-Ära also, erklärt. Vor dem Hintergrund des immensen politischen Engagements auch zahlreicher DT-Künstler*innen im Zuge der friedlichen Revolution und der keimenden Hoffnung auf Mitbestimmung (oder auch der Option auf eine anderen sozialistische Zukunft), verdichte die Inszenierung Hamlet/Maschine fehlende Wendeeuphorie und Gefühle des Scheiterns, nicht zuletzt angesichts der Vorahnung einer nahenden "Übernahme" (Ilko-Sascha Kowalczuk) durch das kapitalistische System mit dessen Geschichtsballast von Imperialismus und Kolonialismus.


Ostermeiers Shoppen und Ficken wird institutionell betrachtet u. a. deshalb zu einer sinnbildhaften DT-Produktion, da sie im Kontext des "Theaterwunders Baracke" (Kap. 4), der Neugründung einer Nebenspielstätte des Haupthauses in der Spielzeit 1996/97 steht und bereits 1998 zum "Theater des Jahres" gewählt wurde. Strukturell interessant ist hier zum einen, dass die Baracke damals u. a. durch Sponsoring von Daimler-Benz und der großzügigen Unterstützung des Freundeskreises anschubfinanziert wurde und dass ihr Entstehen zum anderen auf das Engagement zweier westdeutscher Angestellter im Leitungsteam zurückzuführen ist. Die Geschichte zur Baracke und der Shoppen und Ficken-Inszenierung liest sich deshalb auch ein bisschen wie die Beschreibung des Einbruchs "des Westens" in das noch zu sehr an seiner Ost-Identität klebende DT, das Speicher zufolge, 1996 in einem "lethargischen Zustand" (Kap. 4.1) gewesen sei.


Shoppen und Ficken von Mark Ravenhill aufzuführen, markiere zudem eine Zuwendung zu europäischer Gegenwartsdramatik, insbesondere zu den britischen "In-yer-Face"-Dramen, was seinerzeit zum programmatischen Alleinstellungsmerkmal avancierte. Mit der künstlerischen Leitung der Baracke wurde Thomas Ostermeier gemeinsam mit Regisseur Christian von Treskow berufen, die im Team mit Dramaturg Jens Hillje, den Schauspielerinnen Nina Hoss und Jule Böwe sowie Bühnenbildner Jan Pappelbaum in räumlicher Trennung vom Haupthaus als neue Theatergeneration neue, auch radikalere, Ästhetiken ausprobierten. Mit der genauen Analyse der sogenannten "Messer-Szene" verbindet Speicher im Sinne ihrer These die Erkenntnis, dass sie die "Einschnitte, denen das Haus um die Jahrtausendwende ausgesetzt sein wird" (S. 144) präfiguriere – zumal unter Intendant Bernd Wilms fast 150 Mitarbeiter*innen weniger am DT tätig waren. Darüber hinaus stehe jene explizite Gewaltszene, in der eine Figur mit einem Messer zu Tode vergewaltigt wird (ohne, dass die Stückfassung dieses Ende vorgesehen hätte), Speicher zufolge auch für den "brutale[n] Gründungsakt" (S. 170) von Ostermeiers Neuen Realismus, mit dem er kurze Zeit später an die Schaubühne wechseln und reüssieren sollte.


Emilia Galotti, jene Erfolgsinszenierung, mit der Michael Thalheimer 2001 die Intendanz von Bernd Wilms am DT eröffnete, steht für Speicher abermals für eine sich symptomatisch wandelnde Institutionsästhetik am DT. So kommt sie nach der Analyse von Dramentext, DDR-Rezeptionsgeschichte und Neuinszenierung zum Schluss, dass die reduktionistische Ästhetik Thalheimers Wilms' organisatorisches Programm der Schuldenreduktion, Ensemblereduzierung und Apparatverschlankung widerspiegle. Mit Wilms und den neu am Haus arbeitenden Künstler*innen (wie u. a. Dimiter Gotscheff und Katrin Brack oder Jürgen Gosch und Johannes Schütz) ziehe eine – abermals sinnbildlich gedeutete – "Ästhetik des Weniger" (vgl. S. 199) ein, die das Haus, auch kulturpolitisch betrachtet, aus der Phase der Nachwende-Vulnerabilität in den Zustand der gegenwärtigen Resilienz gehievt habe. Relevant ist im Zuge der Galotti-Analysen auch Speichers Hinweis, dass Thalheimer mit seinen inhaltlichen Kürzungen und der damit verbundenen Privatisierung des Grundkonflikts spezifische historische Dimensionen der Vorlage unsichtbar machte, wodurch gleichsam eine "deutliche Abkehr vom sozialistischen Erbe des Deutschen Theaters vollzogen" (S. 231) wurde.


So innovativ der methodische Zugriff und schlüssig die drei inszenatorisch-organisatorischen Sinnbildstrukturen, so heikel ist er gewählte Ansatz der Autorin auch. Schließlich neigt diese Art der Betrachtung dazu, äußerst komplexe Sachverhalte zu verkürzen. Denn natürlich gab es in diesen knapp zwanzig Jahren so unendlich viel mehr auf den Bühnen des DTs zu sehen, so viele andere einschlägige Inszenierungen und so viel mehr Mitarbeitende als die befragten. Gleichzeitig braucht es für ein solches Forschungsvorhaben natürlich auch Eingrenzungen. Während insbesondere die dramenanalytischen Textteile qualitativ hervorstechen, fallen Einsparungen bei der konzeptuellen Begriffsarbeit auf. So wurde z. B. mit den beiden untertitelgebenden Begriffen Vulnerabilität und Resilienz im Grunde über die Einleitung hinaus kaum konkret gearbeitet, auch muss die Setzung, dass Inszenierungen als "Bilder der Organisation" (S. 15) interpretiert werden können, ohne bildtheoretische Fundierung auskommen. Angesichts der Materialfülle und des sehr angenehmen Schreibstils kann man darüber allerdings hinwegsehen.


Was allerdings wirklich schade ist – und deshalb mindestens noch eine weitere Studie wert wäre –, ist die Tatsache, dass aus den immer wieder nur kurz aufpoppenden, punktuellen Hinweisen, dass Ost-West-Differenzen am DT nicht ordentlich aufgearbeitet worden seien, nichts weiter gemacht wurde. Denn gerade das scheint mir der zentralste Punkt zu sein, wenn man die Institutionsgeschichte des Deutschen Theaters Berlins seit 1989 schreiben und verstehen möchte.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2023/2</description>
  </descriptions>
</resource>
