<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2023-2-09</identifier>
  <creators>
    <creator>
      <creatorName>Bobik, Sebastian</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Sulgie Lie: Hong Sangsoo. Das lächerliche Ernste.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2023</publicationYear>
  <dates>
    <date dateType="Submitted">2023-09-14</date>
    <date dateType="Accepted">2023-11-06</date>
    <date dateType="Updated">2023-11-14</date>
    <date dateType="Issued">2023-11-14</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-650-8264</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Im Festivalkino der Gegenwart gibt es wenige Filmemacher, die so viel Aufmerksamkeit und Lob erfahren, wie der südkoreanische Filmemacher Hong Sang-Soo. Seit nun schon über 25 Jahren setzen sich seine Filme immer wieder mit der Beziehung zwischen Männern und Frauen auseinander, die er oft erbarmungslos humoristisch portraitiert. Seine unglaubliche Produktivität führt dazu, dass er oft 2 bis 3 Filme pro Jahr produziert (mittlerweile hat er schon 30 Spielfilme inszeniert) und diese dann auf den wichtigsten Filmfestivals der Welt präsentiert. Nun gibt es mit Hong Sangsoo: Das lächerliche Ernste von Sulgi Lie endlich eine deutschsprachige Publikation, die sich mit dem Werk Hongs auseinandersetzt. Das Buch ist mit nur knapp 90 Seiten ebenso kurz und bündig wie einige Filme des Regisseurs. Lie widmet sich dabei seiner Filmografie in grob chronologischer Reihenfolge und durchquert diese anhand verschiedener Themenkomplexe, denen er einzelne Kapitel widmet.


Gleich im ersten Satz des ersten Kapitels finden wir die (vielleicht?) entscheidende Formel des Kinos von Hong Sang-Soo: Männer, Frauen und Alkohol. Davon ausgehend beschäftigt sich Lie zuerst damit, wie der Konsum von Alkohol die Kommunikation der Figuren in Hongs Kino vereinfacht, verkompliziert oder sogar verunmöglicht. Aufgrund von Hongs umfangreicher und schnell wachsender Filmografie widmet sich Lie nicht der langen Analyse einzelner Filme, sondern beschäftigt sich eher mit Ideen und Themen, die sich durch die Filme ziehen. Das Werk von Hong Sang-Soo bietet sich für diese Leseart gut an, da die Filme oft Variationen von ähnlichen Beziehungsgeflechten, Konflikten und Fragen sind. Oft hört man, dass alle seine Filme vielleicht eher so etwas ein langer Film sind, dem er sukzessiv Bruchstücke und Fragmente hinzufügt. Der theoretischen Unterbau für die Beschäftigung mit den verschiedenen Themen, die Lie in der Filmografie Hongs herausarbeitet, ist breit gefächert: Von popkulturellen Bezugspunkten, wie der Musik der Pet Shop Boys, bis hin zu den Texten von Jean Epstein, Jean Paul Sartre, André Bazin und vielen weiteren nutzt Lie verschiedenste Quellen, um über die Filme von Hong Sang-Soo nachzudenken.


Viele der Schlussfolgerungen, die Lie im Laufe seiner Analyse der Filme trifft, sind nicht unbedingt neu: Die Männer in Hongs Filmen sind erbärmlich, verlogen, lächerlich. Die Frauen oft ehrlicher, und falls doch verlogen, behalten sie wenigstens ihre Würde. Aber Lie unterbreitet diese weit verbreiteten Analysen über Hongs Figuren mit vielen Beispielen und Zitaten aus den Filmen, die ein klares Bild schaffen und auch neue Einsichten bereithalten. So wird die These klarer, wenn Lie z. B. gegenüberstellt, wie Männer und Frauen in Hongs Filmen beten, welche Worte sie dabei verwenden, was ihre Intentionen zu sein scheinen und wie sie dabei gefilmt werden.


Hongs berühmte Ästhetik (die vielleicht eher eine Nicht-Ästhetik ist) wird ebenfalls untersucht. Der Band enthält keine Bilder, stattdessen gibt es immer wieder sorgfältige, detaillierte Beschreibungen, in denen Lie Hongs Einstellungen veranschaulicht. Diese zeichnen sich oft durch die Kamerabewegung aus (Schwenks und Zooms bestimmen Hongs Bildsprache), für deren Analyse Standbilder ohnehin nicht ideal wären. Immer wieder arbeitet Lie in diesen Beschreibungen die feinen visuellen Unterschiede heraus, die in Hongs einander oft spiegelnde Szenen zur Geltung treten. Er veranschaulicht das z. B. an der Handlung von Right Now, Wrong Then (KR 2015), in dem uns zwei Versionen einer Geschichte gezeigt werden, wovon eine leicht "besser" für die Hauptfigur verläuft als die andere. Lie zeigt uns, dass Hong auch mit feinen visuellen Unterschieden, wie dem leichten Verschieben einer Kameraperspektive, die Variation dieses doppelten Narrativs unterstreicht.


Ein weiteres Thema, dem sich Lie an vielen Stellen seines Buchs – am stärksten im Kapitel "Eingebildete Bilder" – widmet, sind Hongs filmische Einflüsse. Wann immer Hong Sang-Soos Filme auf ihre filmischen (und ästhetischen) Einflüsse hin untersucht werden, fallen dieselben Namen: Eric Rohmer, Paul Cezanne, Robert Bresson. Sie werden auch in diesem Band genannt. Doch Lie fügt dieser Liste noch einige Namen hinzu, wie z. B. Jean Renoir, Yasujiro Ozu, Luis Buñuel, Jean-Luc Godard oder François Truffaut. Hong Sang-Soos Filme sind nicht dafür bekannt, dass sie die Filmgeschichte ausgiebig zitieren. Eher werden sie als eine Art in sich geschlossenes Universum betrachtet, in dem sich die einzelnen Filme stärker aufeinander beziehen als auf Werke andere Filmemacher. Dementsprechend interessant ist es, wenn Hong als ein bewanderter Filmkenner gezeigt wird. Lie findet filmgeschichtliche Zitate nicht nur in einigen Filmtiteln (Woman on the Beach (KR 2006) in Bezug auf Jean Renoir oder Claire’s Camera (KR/FR 2017) in Anlehnung an Eric Rohmer) oder in visuellen Motiven Querverweise auf andere Filmemacher sondern z. B. auch in der Auswahl bestimmter Musikstücke.


Andere Blicke, die Lie auf die Filme wirft, sind weniger vertraut. So rückt er einige der späteren Filme Hong Sang-Soos in die Nähe des Melodramas. Für Hong, dessen Filme oftmals eine ironische Distanz zu ihren Figuren und deren Verhalten einnehmen, ist diese Zuordnung eine ungewohnte. Lie erkennt jedoch in der Zuwendung der Filme zu weiblichen Protagonistinnen eine Bewegung weg von der Komödie in Richtung des Melodrams. Diesen widersprüchlichen Raum zwischen Dramatik und Lächerlichkeit, der dem Band seinen Namen gibt, macht Lie sogar in einzelnen Handlungen Hong'scher Figuren aus (besonders gut veranschaulicht er diese Gratwanderung anhand der Tränen der männlichen Figuren, denn auch wenn diese oft aus lächerlichen Gründen weinen, macht sie das nicht weniger traurig). So stellt sich Lie gezwungenermaßen der Frage: sind Hongs Filme ernst oder lustig? Die Antwort lautet natürlich, dass es sich um kein "entweder, oder" handelt. Er schlägt den Begriff des "lächerlich Ernsten" bzw. des "ernsten Lächerlichen" vor, um diese Ambivalenz des Kinos von Hang Sang-Soo zu fassen.


Hong Sang-soo: Das lächerliche Ernste von Sulgi Lie ist eine erhellende Einführung in das Werk Hong Sang-Soos und füllt als erster deutschsprachiger Band, der sich ganz den Filmen des Regisseurs widmet, eine Lücke und gibt hoffentlich Anstöße für weitere Beschäftigungen mit dem Werk dieses bedeutenden zeitgenössischen Filmemachers.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2023/2</description>
  </descriptions>
</resource>
