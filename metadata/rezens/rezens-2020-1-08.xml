<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2020-1-08</identifier>
  <creators>
    <creator>
      <creatorName>Mücke, Laura Katharina</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Lars C. Grabbe/Patrick Rupert-Kruse/Norbert M. Schmitz (Hg.): Immersion – Design – Art: Revisited. Transmediale Formprinzipien neuzeitlicher Kunst und Technologie.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2020</publicationYear>
  <dates>
    <date dateType="Submitted">2019-06-25</date>
    <date dateType="Accepted">2020-05-13</date>
    <date dateType="Updated">2020-05-25</date>
    <date dateType="Issued">2020-05-25</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-293-2776</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Die Situation der Forschungsliteratur zum Mode-Begriff "Immersion" ist eine schnelllebige. Dies beweist aus der Perspektive des aktuellen Jahres 2020 dieser 2018 erschiene Band mit dem Titel Immersion – Design – Art: Revisited. Transmediale Formprinzipien neuzeitlicher Kunst und Technologie, der in Kooperation verschiedener Kunst- und Fachhochschulen in Kiel und Münster, basierend auf einer Tagung von 2016, erarbeitet wurde. Während nach der Veröffentlichung von Fabienne Liptays und Burcu Dogramacis Immersion in Visual Arts and Media im Jahr 2016[1] im medienwissenschaftlichen Bereich eine zweijährige Pause der theoretischen Auseinandersetzung mit dem Begriff Immersion entstanden ist (die lediglich mit praktischen Zuwendungen wie etwa dem mehrjährigen Ausstellungsprojekt "Immersion" der Berliner Festspiele zu füllen wäre), erscheint 2018 etwa zeitgleich mit Rainer Mühlhoffs in der Philosophie angesiedelter Dissertation Immersive Macht hier noch eine weitere Publikation.
&#13;

Der Sammelband hat sich eine "eigenständige medientheoretische Perspektive" vorgenommen, "in der Medienkunst weder als einfacher Effekt der Medientechnologie erscheint, noch die gesellschaftliche Realität der Medien mit der künstlerischen Phantasie der Medienkünstler verwechselt wird" (S. 7). In dieser Dimension schlägt die hier vorgelegte Veröffentlichung der Herausgeber Lars C. Grabbe, Patrick Rupert-Kruse und Norbert Schmitz in eine verwandte Kerbe zu Mühlhoffs Buch, der als "immersive Macht" "den strategischen Machteffekt [begreift], der sich auf der makroskopischen Ebene affektiver Resonanz- und Interaktionsmuster"[2] manifestiert. Überdies bleibt der Sammelband anschlussfähig an die 2019 erschienene Ausgabe der Navigationen-Zeitschrift, die herausgegeben von Thiemo Breyer und Dawid Kasprowicz den Titel Immersion. Grenzen und Metaphorik des digitalen Subjekts trägt und betont, sie wolle "die reziproken Verhältnisse zwischen Subjekt und Objekt, Werk und Rezipient"[3] untersuchen. Und trotzdem liegt hier ein sich von diesen (medien-)philosophisch orientierten Schriften stark unterscheidendes Buch vor.
&#13;

So präsentiert sich der Sammelband im Vergleich zur 2016er-Tagung als wesentlich theoretischer; im Vergleich zu anderen aktuellen Texten des Diskurses aber praktischer orientiert. In der Einleitung wird bereits auf die unüberwindbare Utopie hingewiesen, die den Diskurs um immersive Erfahrungen und Medientechnologien durchzieht, seit mit Oliver Graus Buch Virtual Art. From Illusion to Immersion (2003)[4] das Schreiben einer Genealogie immersiver Medienformate als lineare und teleologische Steigerung medialer Mimesis begonnen wurde. Jener Steigerungslogik steht auf theoretischer Seite einerseits der Vorwurf der Negation zeitspezifischer Wahrnehmungskonfigurationen auf Nutzer*innenseite gegenüber, welche die Herausgeber im Verweis auf die veränderten Wahrnehmungsmodalitäten von digital natives thematisieren. Andererseits muss auch die oft formulierte Annahme eines Immersionsbedürfnisses als anthropologische Konstante mit Kritik insofern rechnen, als dass einem 'perfektionierten' Realitätseffekt von Repräsentationen mit der philosophischen Sicht auf die mediale Illudierung als willing suspension of disbelief längst eine dynamischere Konzeption entgegengestellt wurde.
&#13;

Dieser komplexen Konzeption des Immersionsdiskurses kommt der Sammelband differenziert nach, indem in verschiedenen Artikeln theoretische Binärkategorien wie Realität vs. Virtualität, Unmittelbarkeit vs. Hypermedialität[5], Kunst vs. Medialität, Ästhetik vs. Aisthesis sowie Immersion vs. Reflexion aufs Neue befragt und deren Dualismus zur Debatte gestellt wird. Gleichzeitig ist dem Buch sein Ursprungsort, das praktisch orientierte "Institut für immersive Medien" in Kiel, das auch einer der beiden Austragungsorte der Tagung war, weiterhin anzumerken. Randbemerkungen wie jene über einen "Fortschritt der Technologien" (S. 8) oder Aussagen wie "Diese Unterscheidung [zwischen einer Gleichzeitigkeit von der Wahrnehmung des Bildgegenstandes als einer außerbildlichen Referenz und dem Bewusstsein des Bildes als solchem] ist logischerweise bei vollständiger Immersion nicht mehr möglich." (S. 11) erweisen, dass das Anliegen des Buches in Wahrheit ein doppeltes ist: Einerseits den wahrnehmungstheoretischen Novitäten im Diskurs Rechnung zu tragen, gleichzeitig jedoch die technologische Komponente – die Frage nach den technischen Potenzialen sogenannter immersiver Medien – weiterhin aufrecht zu erhalten.
&#13;

Ein solches doppeltes Anliegen manifestiert sich auch in der inhaltlichen Zweiteilung des Buches in Teil 1 "Die Kunst der Immersion", der sich mit der "Spezifik des Künstlerischen gegenüber dem allgemein Medialen" (S. 8) auseinandersetzt, und Teil 2 "Zur Ästhetik der Immersion", der sich eine "systematische Bestimmung von Begriff und Phänomen hin zu gegenwärtigen und einschlägigen kulturellen Praxen" (S. 16) vorgenommen hat. Auf den ersten Blick scheint es überraschend, dass die Herausgeber entschieden haben, zwei ältere und etwas eingestaubt wirkende Texte – Oliver Graus Aufsatz zur Telepräsenz von 2001 und Lambert Wiesings Antrittsvorlesung zur Unterscheidbarkeit von Virtualität und Imagination von 2005 – wieder abzudrucken, zumal es die originären Autoren den Herausgebern sogar selbst überlassen haben, Abstracts für deren Texte zu formulieren. Auf den zweiten Blick lässt sich diese Entscheidung aber mit der in diesen Artikeln angesprochenen Technikgeschichte der Utopien und Mythen (Grau) sowie der Unterscheidung von "Kontinuitätstheoretikern" und "Diskontinuitätstheoretikern" (S. 140) als Ausgangspunkte der oben angesprochenen Diskurse begreifen, die auch in den anderen Beiträgen weitergeführt werden.
&#13;

Von diesen erscheinen vier insbesondere erwähnenswert: etwa jener von Norbert Schmitz, dessen provokante Frage, ob wir nicht grundlegend in Immersionen leben würden, wiederum zu einer komplexen Befragung der Funktion einer "Kunst der Immersion" führt. Schmitz formuliert als Fazit seines Textes klug: "Die 'Kunst der Immersion', ob nun als subversive Strategie innerhalb der Populärkultur oder innerhalb des Kunstsystems, bestände also darin, die ästhetische Differenz zwischen Objekt und seiner Abbildung wieder sichtbar zu machen, aber nicht im überkommenen Geist einer Dekonstruktion der Mimesis, sondern als Thematisierung der vollständigen Konstruiertheit unserer alltäglichen phänomenalen Wahrnehmung als unüberschreitbare Grenze und conditio humana" (S. 73) und schafft so die thematische Verbindung zu einem Artikel im zweiten Teil – jenem von Jonathan Lahey Dronsfield. Dieser befragt in seinem Text unter anderem den anthropologischen Wunsch nach "immediacy" neu und stellt dabei ganz ähnlich fest: "[W]hat is the desire for 'unified experience' in an immersive environment […]? Nothing other than the longing for subjectivity in the loss of self" (S. 189). Dass neben der technologischen Überwältigungsstrategie also auch das rezipierende Subjekt theoretisch befragt werden muss, zeigt ebenso der Text von Alberto Gabriele auf, der aus literaturwissenschaftlicher Perspektive die Relevanz eines interdisziplinären Blickwinkels andeutet, um spezifische Wahrnehmungskonfigurationen zu spezifischen Zeit-Punkten untersuchen zu können. Gabriele inspiziert dafür beispielhaft die Wechselwirkung zwischen dem "cartographic writing" (S. 193) und der Position, die ein*e Zuschauer*in während der Rezeption eines Panoramas einnimmt. Seine Schlussfolgerung, "[v]ision, therefore, becomes a self-induced normative rearrangement of the faculties of memory and perception" (S. 203) beschreibt so Immersion mit der nötigen Komplexität und Relationalität. Eine solche Komplexität klingt auch in Lars C. Grabbes Text an, wenngleich dieser über die von ihm sogenannte "Phänosemiose" (die "medieninduzierte Körper-Geist-Dynamik" (S. 155)) "die menschliche Wahrnehmung [als] abhängig von der jeweils kulturell realisierten medialen Technizität" (S. 158) beschreibt, jedoch die Frage nach einer möglichen Autonomie des Subjekts gänzlich außen vor lässt.
&#13;

Weitere Texte des Bandes sind Patrick Rupert-Kruses Beschäftigung mit verschiedenen Formen medialen Realismus' (im Spiegel von Ästhetik und Aisthesis), Diego Mantoans Generationenvergleich von Videokünstler*innen, der Videotechnologien auf ihre "aesthetic maturity" befragt, Carolina Fernández-Castrillos Rückblick auf die "desire of uniting art and life" (S. 127), die die Futurist*innen in ihren Manifesten vorgeschlagen hatten, und Christiane Heibachs Bericht von einem Versuch, Proband*innen in eine synästhetische Medienumgebung zu versetzen, während theoretisch auf Mark Weisers Konzept der Re-Naturierung von Technik, der Nahtlosigkeit zwischen Welt und Repräsentation zurückgegriffen wird.
&#13;

So verbleibt nach der Lektüre der hinsichtlich ihrer Aktualität, ihrer Diskursfreudigkeit und ihrer Verzahnung von Theorie und Praxis höchst heterogenen Texte vor allem ebendiese Verschiedenheit als erfreulicher Output: Indem das Buch praktische wie theoretische, disziplininterne wie interdisziplinäre, aktuellere wie ältere Perspektiven auf den medienkünstlerischen und -wissenschaftlichen Immersionsbegriff präsentiert, scheint zwar gelegentlich die Bezugnahme auf die aktuellen Publikationen im gleichen thematischen Feld – etwa zu Liptays und Dogramacis Herausgeber*innenschaft – aus dem Blick zu geraten. Insgesamt präsentiert sich das Buch jedoch als adäquates Nachschlagewerk, in dem sehr verschiedene Zugänge zur Immersion aufeinandertreffen. Um diese erfreuliche Heterogenität weiter voranzutreiben, wäre zwar auch das Hinzuziehen marginalisierter Perspektiven auf die vermeintlich anthropologische Konstante Immersion wichtig gewesen. Es bleibt jedoch darauf zu hoffen, dass eine solche – die bislang prominenten Diskurspfade verlassende – alternative Schreibweise einer politisch äußerst relevanten Geschichte immersiver Medien im nicht-westlichen Erfahrungsraum in künftige Publikationen stärker Einzug nehmen wird. Im hiesigen Sammelband sind zumindest einführend Gedanken für einen solche zeitgemäße Heterogenität zu finden.
&#13;

 
&#13;

[1] Fabienne Liptay/Burcu Dogramaci (Hg.): Immersion in Visual Arts and Media. Leiden 2016.
&#13;

[2] Rainer Mühlhoff: Immersive Macht. Affekttheorie nach Spinoza und Foucault. Frankfurt/New York 2018, S. 22.
&#13;

[3] Dawid Kasprowicz/Thiemo Breyer (Hg.): Immersion. Grenzen und Metaphorik des digitalen Subjekts. Ausgabe von: Navigationen, Zeitschrift für Medien- und Kulturwissenschaft 19/1, 2019, S. 8.
&#13;

[4] Oliver Grau: Virtual Art from Illusion to Immersion. Cambridge/London 2003.
&#13;

[5] Jay Bolter/Richard Grusin: Remediation. Understanding New Media. Cambridge 1999.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2020/1</description>
  </descriptions>
</resource>
