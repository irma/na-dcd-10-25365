<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2022-2-17</identifier>
  <creators>
    <creator>
      <creatorName>Becker, Alexander</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Joanna Nowotny/Julian Reidy: Memes. Formen und Folgen eines Internetphänomens.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2022</publicationYear>
  <dates>
    <date dateType="Submitted">2022-11-07</date>
    <date dateType="Accepted">2022-11-25</date>
    <date dateType="Updated">2022-12-13</date>
    <date dateType="Issued">2022-12-13</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-609-7640</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
OK Boomer, jetzt ist es so weit: Memes sind endgültig in unseren akademischen Kreisen angelangt. Während sie von der englischsprachigen Forschung seit längerem interessiert beobachtet wurden (vgl. z. B. Shifman 2013), hat man sich hierzulande bislang nur zaghaft an sie herangewagt. Mit der ersten deutschsprachigen Monografie zu memes demonstrieren Joanna Nowotny und Julian Reidy, wie aufschlussreich die Diskussion um das, oft als Witz oder Unfug abgetane, Internetphänomen sein kann.


Denn memes, zu deutsch 'Meme', sind Ausdruck unserer "Kultur der Digitalität" (Stalder 2016). Unter anderem zeigen sie, wie weit sich kollektive Partizipation im digitalen Raum etabliert hat. Längst sind wir dazu übergegangen, uns im Netz nicht bloß passiv 'berieseln' zu lassen, sondern selbst Inhalte zu generieren, zu teilen, zu kommentieren, zu modifizieren, zu zitieren usw. (vgl. S. 9). Insofern legen memes auch die Spezifik des Internets offen: Im Gegensatz zu klassischen Medien wie dem Film oder dem Fernsehen, bei denen das Kommunikationsverhältnis tendenziell einseitig gestaltet ist, befähigt das "demokratisierende[...] Potenzial des Webs" (Stier 2017, S. 20) jede*n dazu, selbst Sender zu werden.


Für die gemeinschaftliche und massenhafte Verbreitung – also die Viralität – von memes ist das Internet also ebenso verantwortlich wie geschaffen. Doch als digitale und daher auch veränderbare visuelle beziehungsweise akustische Einheiten wollen memes nicht bloß kopiert und geteilt werden. "Memes sind immer auch darauf ausgerichtet, die Bereitschaft und Fähigkeit zu wecken, ein Rezeptionsangebot produktiv in einen user generated content umzuformen und umzudeuten; formale Gestalt und semantischer Gehalt sind in diesem Replikationsvorgang untrennbar miteinander verschränkt und von gleicher Relevanz" (S. 12; Herv. im Orig.). So betrachtet setzen memes ein anderes Verständnis der Platon’schen Mimesis voraus, das sich, folgt man den Autor*innen, als Memesis beschreiben ließe – hier geht Replikation Hand in Hand mit Mutation (vgl. S. 33). Wir vervielfältigen memes nicht nur, wir gestalten sie auch unablässig um.


Memes haben daher einen außerordentlich heterogenen Charakter. Schon ihre Formatierungen zeugen von Facettenreichtum: Meist sind es (Bewegt-)Bilder, die mit Text und/oder Ton arbeiten und auf den unterschiedlichsten Plattformen zirkulieren. Dazu zählt das am häufigsten gewählte Format Image Macro, ein unbewegtes Bild mit darübergelegtem Text, das auf Websites wie 9GAG, Reddit, Twitter oder auch Instagram Verbreitung findet. Daneben stößt man häufig auf zumeist kurze, mit dem Smartphone produzierte Videoclips, die beispielsweise als Vines oder TikToks im Umlauf sind. Doch auch in die Gestalt von bloßen Tönen oder Phrasen können memes schlüpfen. Vor allem letztere führen dazu, dass sie als Zitate Fuß im Alltag fassen und sich in gewisser Weise von ihren medialen Trägern befreien (vgl. S. 13).


Vielfalt zeigen memes auch im Inhalt. Humoristische, aber auch durchaus kritische Auseinandersetzungen mit Gesellschaft und Politik sind ebenso möglich wie mit Populär- und Hochkultur. Mit einem Streifzug durch die unterschiedlichsten meme-Kulturen stellt der Beitrag einen abwechslungsreichen und trotzdem kohärent aufgebauten analytischen Korpus unter Beweis: Politische memes eröffnen Partizipationsmöglichkeiten in Wahlkämpfen, Demonstrationen und Bewegungen (vgl. S. 111-156). Pandememes vermitteln das Bild eines alltagstauglichen Covid-19-Virus, indem sie die tiefgreifenden Effekte der Pandemie verharmlosen oder gänzlich ausblenden (vgl. S. 51-64). Hingegen machen Meta-memes deutlich, dass an ihrem Entstehungsprozess nicht nur menschliche, sondern auch algorithmische Autorschaft involviert sein kann (vgl. S. 89-96). Schließlich wird das trolling – das provokante, zuweilen sadistische Aufwiegeln anderer User*innen – als "basale memetische Praktik" (S. 190) beschrieben: Häufig bildet es den "pragmatischen, […] performativen und ästhetischen Rahmen" (ebd.) für die Entstehung und Progression von memes.


Memes operieren hochgradig referenziell, sind sie doch in der Lage, sich auf jedes nur erdenkliche Material zu beziehen. Nowotny und Reidy erklären daher "Referenzialität [als] das formale Kriterium von memes überhaupt" (S. 17). Nicht selten erscheinen sie rätselhaft, derart unzusammenhängend wirken die simultan vermittelten Botschaften auf den ersten Blick. Dass es möglichst kleinteiligen wie spektral aufgebauten Analysen gelingt, den in die unterschiedlichsten Richtungen zeigenden Querverweisen zu folgen und der Vielschichtigkeit von memes gerecht zu werden, führen die Autor*innen exemplarisch mit der Untersuchung eines 4chumblr-memes vor. Seinerseits Resultat einer sich im Jahr 2010 abgespielten 'Fehde' zwischen User*innen der Plattformen 4chan und Tumblr zitiert dieses Bild nicht nur ihm vorangegangene memes, sondern bezieht sich auch auf Werke aus der Film-, Musik- und Kunstgeschichte und setzt ein Wissen um einen spezifischen Kontext voraus – in diesem Fall um den Diskurs zwischen zwei Plattformen (vgl. S. 26-33). Das Ergebnis dieser Betrachtung soll hier nicht vorweggenommen werden, nur so viel: Eine solche Analyse zeigt, dass sich die Aussage von memes oft erst im Zusammenspiel von "primär formalästhetisch interessierten (also sozusagen textimmanenten Zugriffen) und Kontextlektüren" (S. 67) erschließt. Neben visuellen, textuellen und tonalen Elementen spielen bei der Untersuchung von memes immer auch gesellschaftliche, historische und/oder kulturelle Zusammenhänge eine zentrale Rolle.


Ihre iterative Bezugnahme spricht dafür, memes als "Teil der visuellen Kultur(en) [zu] begreif[en] und sie in einschlägige ästhetische Traditionslinien ein[zuordnen]." (S. 37, Herv. v. A. B.) Parallelen zu geläufigen (künstlerischen) Praktiken wie beispielsweise der Bricolage, dem Zitat oder der Appropriation – ihrerseits oftmals Akteure mit politischer Sprengkraft – erscheinen als sinnvoll. Im Unterschied zu solchen Verfahren ist der Grad an Referenzialität bei memes höher, wenn man ihnen die oben beschriebene Mutationsfähigkeit attestiert: Je länger sie im Umlauf sind, desto mehrdeutiger, aber auch vielstimmiger werden sie. Genau hierin liegt das fundamental politische Potenzial von memes, insofern sie typischerweise "einer anonymen und kollektiven Autor*innenschaft [entspringen]." (S. 46)


Nun, da die erste deutschsprachige Monografie zum Thema erschienen ist, gilt es einen Blick auf die hierzulande aufblühende meme-Szene zu werfen, wo Subreddits wie r/ich_iel (als Pendant zu r/me-irl) oder Satiriker wie elhotzo scherzhaft mit anglo-amerikanischen Vorbildern konkurrieren und sich (nicht nur deshalb) großer Beliebtheit erfreuen. Zwar bleiben sie in den Analysen von Nowotny und Reidy noch von memes aus dem englischsprachigen Umfeld überschattet, doch ist es nur eine Frage der Zeit, bis hiesige memetische Gepflogenheiten ins Visier kulturwissenschaftlicher Debatten geraten. In jedem Fall verspricht der Beitrag Beispielhaftigkeit in der Auswahl seiner Untersuchungsgegenstände, weshalb sich von ihm ausgehende Analysen weiterer, wenn auch vorwiegend 'westlich' orientierter Transformationen und Translationen (vgl. S. 76-79) als ertragreich erweisen dürften.


Angesichts sich permanent wandelnder meme-Kulturen, deren Kommunikation inhaltlich wie formalästhetisch hochgradig heterogen verläuft, ist es kein Wunder, dass Nowotny und Reidy nahelegen, sich nicht auf eine fixe und allgemeingültige Definition von memes festzulegen: Vielmehr müsse "[d]en online zirkulierenden Informationsfragmenten terminologisch und konzeptuell […] mit Offenheit und Kreativität begegnet werden." (S. 13) Anstatt auf Vereinheitlichung zu setzen, plädiert der Beitrag für eine perspektivische Begriffsbestimmung. Dennoch verliert man bei der Lektüre an keiner Stelle den Boden unter den Füßen: Trotz (oder vielleicht gerade wegen) seiner multimodalen theoretischen wie analytischen Herangehensweise besticht der Beitrag konsequent durch präzise Beschreibungen. Bisherige Erkenntnisse – wie die Etymologie und frühe Theorie von memes – werden pointiert wiedergegeben und kritisch reflektiert.


Insofern kann der Band als denkbar aktuelle wie solide Einführung gelesen werden, die Anschlüsse für weiterführende Untersuchungen bietet. Darüber hinaus zeigen Nowotny und Reidy: Ist man bereit, memes als ernstzunehmendes Internet- und Kulturphänomen aufzufassen, kann man durch die Auseinandersetzung mit ihnen, nicht nur relevante Aussagen über die gegenwärtige Medienlandschaft, sondern auch unseren Umgang mit Digitalität treffen.


 


Zitierte Literatur


Shifman, Limor: Memes in Digital Culture. Cambridge: MIT Press 2013.


Stalder, Felix: Kultur der Digitalität. Berlin: Suhrkamp 2016.


Stier, Sebastian: Internet und Regimetyp. Netzpolitik und politische Online-Kommunikation in Autokratien und Demokratien. Wiesbaden: Springer 2017.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2022/2</description>
  </descriptions>
</resource>
