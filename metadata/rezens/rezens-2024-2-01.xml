<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2024-2-01</identifier>
  <creators>
    <creator>
      <creatorName>Kotte, Andreas</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Jan Lazardzig: Wissenschaft aus Gefolgschaft. Der Fall Knudsen und die Anfänge der Theaterwissenschaft.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2024</publicationYear>
  <dates>
    <date dateType="Submitted">2024-03-20</date>
    <date dateType="Accepted">2024-05-21</date>
    <date dateType="Updated">2024-11-13</date>
    <date dateType="Issued">2024-11-13</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="JournalArticle"/>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-675-8634</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Mitläufer – das war das knappe Fazit des Rezensenten zu Hans Knudsen bisher und es deckte sich wahrscheinlich mit jenem anderer fachgeschichtlich interessierter Forscher*innen, die leicht erreichbare Schriften Knudsens aus der Nachkriegszeit zur Kenntnis genommen haben. Jan Lazardzigs tiefgreifende Untersuchung korrigiert dieses Bild gründlich. Von entscheidender Bedeutung ist dabei Knudsens Gesinnungsgenese von 1906 an, als er aus der ostpreußischen Provinz nach Berlin kam und bei Max Herrmann Vorlesungen besuchte. Akribisch wird die Entfaltung völkisch-nationalistischen Denkens nachgezeichnet. Lesende müssen mehrfach schlucken oder entlastend "wow" sagen, weil so manche Seite schwer erträglich daherkommt (z. B. S. 51–58): Wie kann es sein, dass der Jude Max Herrmann solche Denkweisen in seinem nahen Umfeld tolerierte? Es deutet sich an, dass er für sein Ziel, die Theaterwissenschaft als eigenständige Universitätsdisziplin zu etablieren, Akzeptanz von rechts bis links anstrebte, also von Assistent Knudsen bis zum sozialdemokratischen Assistenten Bruno Satori-Neumann, ohne einen von ihnen als Nachfolger ins Auge zu fassen.


Das wirft ein neues Licht auf Herrmann und stützt die kluge Ergänzung des Untertitels "… und die Anfänge der Theaterwissenschaft", der das Buch voll und ganz gerecht wird. Denn Knudsen allein wäre, wissenschaftlich gesehen, kein lohnender Forschungsgegenstand gewesen. Die auch in den 1920er Jahren umstrittene Theaterwissenschaft, die rigorose Gleichschaltung theaterwissenschaftlicher Erkenntnissuche in den 1930er Jahren und die problematische "Entnazifizierung" nach 1945 hingegen schon. Sie bilden die tragenden Pfeiler der Studie. Die Machtergreifung des Nationalsozialismus ist, das arbeitet der Autor unmissverständlich heraus, für Knudsen nicht Anbruch einer schweren Zeit, die durchzustehen ihm auferlegt war (wie er es später darstellte), sondern Erfüllung seiner Träume. Ein Leben in vier Gesellschaftssystemen, in denen er "stets da war" (S. 15) und jeweils mit den Tugenden Fleiß, Treue und Gefolgschaft die Theaterwissenschaft als Universitätsfach durchzusetzen versuchte, bleibt seine Leistung und bestimmt den Aufbau, die Kapiteleinteilung und die treffsicheren Überschriften.


Da im ersten Kapitel zu erklären ist, warum Knudsen sich für Max Herrmanns Ziel, Theatergeschichte "zur Wissenschaft zu erheben" (S. 43) so schnell begeistern kann, widmet Lazardzig zu Recht den konservativen Zügen von Herrmanns Rekonstruktionsmethode mehr Aufmerksamkeit als in den letzten drei Jahrzehnten Forschungsgeschichte üblich: Nicht nur der Raum, sondern letztlich auch das Spiel der Darsteller*innen und die Gesamtvorstellung sollen wieder lebendig werden. Dieser Wunsch bedient, gepaart mit Detailverliebtheit, gerade wegen seiner offensichtlichen Uneinlösbarkeit den idealistischen Enthusiasmus, an Großem teilzuhaben. Nur wer selbst etwas von Iffland hat, kann nachschaffen, was auf dem Papier steht, so Knudsen. Für ihn geht es bei Rekonstruktion um die Einfühlungskraft der Auserwählten, für Herrmann hingegen um das Deuten und Verstehen von Geschichte.


Herrmanns Bestimmung von Theater als sozialem Spiel erscheint im weit gefächerten Kapitel zum Berliner theaterwissenschaftlichen Institut als die zentrale Argumentationsfigur, die letztlich die Gründung ermöglichte. Bezüglich Knudsen wird dessen Tätigkeit als deutschnationaler Theaterkritiker abgehandelt, der zeitkritische Autoren und Regisseure wie Friedrich Wolf und Erwin Piscator niederschreibt und "Schollen"-Dichter wie Hermann Burte fördert. Die brutale Regie wende sich gegen ihren Herrn, den Dichter, dessen Anwalt sie sein müsste. Knudsen gibt sich als widerständiger Außenseiter, einsamer Rufer gegen die sozialdemokratisch-jüdische "Ausländerei" vor allem in der Reichshauptstadt. Aus seiner unbedingten Werktreue wird (so im Kapitel zum Nationalsozialismus dargelegt) Führertreue, der er 1944 seine "Führerprofessur" verdankt. Was Knudsen in der Weimarer Republik angriff, wird im "Dritten Reich" verfolgt. Ein Tor, hätte er sich nicht bestätigt gefühlt, als unter den Fenstern des Instituts die Bücherverbrennung stattfand. Knudsen fordert danach "die Entthronung des Intellekts und die Herrschaft des Gefühls!" (S. 125) – und es geschieht.


Am abschließenden Kapitel zur Westberliner Theaterwissenschaft nach 1945 lässt sich ein vom Autor vielfach angewandtes Argumentationsmuster darstellen: Als Hans Knudsen am 15. November 1948 als erster Ordinarius für Theaterwissenschaft an der Freien Universität seine Vorlesungsreihe "Einführung in die Theaterwissenschaft" eröffnet, würdigt er die "Verdienste Max Herrmanns" ausgiebig, dessen "Schüler und Assistent" er gewesen sei. Nach dieser faktischen Ebene ordnet Jan Lazardzig ein: "Der offensive Bezug auf den im Nationalsozialismus aus Rassenhass verfolgten und im KZ Theresienstadt umgekommenen deutschen Juden half Knudsen wohl, über die eigene Verstrickung in den NS-Kulturapparat hinweggehen zu können. Der Positivismus der Scherer-Schule […] machte sich in der Nachkriegszeit erneut bezahlt. Der Begriff der Rekonstruktion passte gut in die Zeit des Wiederaufbaus". Und nach dieser Bewertung folgt die wissenschaftliche Verallgemeinerung: "Der Wissenschaftshistoriker Mitchell G. Ash prägte für solcherart Kontinuitätsbehauptungen den Begriff der 'konstruierten Kontinuität'." (S. 206) Ein solches dreistufiges Verfahren ist ebenso beispielhaft wie wissenschaftlich effizient. Es wäre zu wünschen, dass es häufiger angewandt würde.


"Der Fall Knudsen" – so heißt der letzte Abschnitt, der der um 1960 beginnenden Aufarbeitung gewidmet ist, und damit jenen Forscher*innen und Publizist*innen, die den Mut aufbrachten, am Nimbus Knudsens als Widerstandskämpfer gegen den Nationalsozialismus zu kratzen. Sie traten gegen ein effizientes Netzwerk vor allem aus ehemaligen Doktorandinnen und Doktoranden des Patriarchen der Westberliner Nachkriegs-Theaterwelt an, das sich mit Knudsen solidarisierte, mit seiner Schule identifizierte und jeden Angriff auf ihn in einen auf die eigene Person umdeuteten. Die Kritiker*innen Joseph Wulf, Heinz Elsberg, Marta Mierendorff, Walter Wicclair und Rolf Seeliger schrieben keine Verurteilungen, keine Pamphlete gegen die "Funktionseliten der Bundesrepublik", sie dokumentierten lediglich Tatbestände und litten darunter, dass sie kaum Gehör fanden. Knudsen zog sich zwar neunundsiebzigjährig auf Druck der Fakultät aus der Lehrtätigkeit zurück, betreute aber danach noch 68 Dissertationen. Durch den Einbezug einer späten Würdigung auch der kritischen Stimmen entstand eine differenzierte neue Fachgeschichte, in der der Autor neben der faktischen auch immer eine paradigmatische Ebene verfolgt. Das Muster der Zerrissenheit der Zwanzigerjahre, aus dem die Vereinheitlichung (Gleichschaltung) erwuchs, führt zu einem sachlich-warnenden Grundton, der explizite Gegenwartsverweise überflüssig macht. Der gute Stil wissenschaftlicher Forschung zeigt sich hier nicht nur in der Überzeugungskraft der Argumente oder in der exzellenten Nachweisführung, sondern auch dank dramaturgischen Wechselspiels in ausgezeichneter Lesbarkeit.


Aus der Sicht einer komplexen und kompletten Berliner Fachgeschichte könnte man sagen: Der Fall Hans Knudsen ist bravourös gelöst, der Fall Leopold Magon harrt der Aufklärung. Weit mehr als die Hälfte der Berliner Fachgeschichte ist geschrieben, wenn man an die Schatten der Vergangenheit denkt. Ohne diese zu bannen, kann sich niemand guten Gewissens den zunehmend produktiven Jahrzehnten zwischen 1960 bis 1990 widmen, der Koexistenz zweier leistungsfähiger Berliner Institute für Theaterwissenschaft. Das Buch erfasst die toxische Gegensätzlichkeit einer personellen Kontinuität von Lehrer Herrmann zu Schüler Knudsen. Wie steht es aber um die örtliche Kontinuität? Der Ort des Instituts von Max Herrmann, die ehemalige Königliche Bibliothek, Ort der Bücherverbrennung, war nach 1945 ausgebrannt. Es konnte 1948 im Osten lediglich eine "Arbeitsgemeinschaft für Theaterwissenschaft" gegründet werden, die im Jahr 1950 in die "Abteilung Theaterwissenschaft am Germanistischen Institut der Humboldt-Universität" überführt wurde. Deren Leiter, der Literaturwissenschaftler und Philologe Leopold Magon, fungierte ab 1960 als Direktor jenes Instituts für Theaterwissenschaft, das nach 1990 schrittweise abgewickelt wurde. Dieses verbleibende Desiderat klingt in den letzten Sätzen des Epilogs von Jan Lazardzig an.
</description>
  </descriptions>
  <relatedItems>
    <relatedItem relationType="IsPublishedIn" relatedItemType="Journal">
      <relatedItemIdentifier relatedItemIdentifierType="EISSN">2072-2869</relatedItemIdentifier>
      <titles>
        <title>[rezens.tfm]</title>
      </titles>
      <issue>2024/2</issue>
    </relatedItem>
  </relatedItems>
</resource>
