<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2020-2-01</identifier>
  <creators>
    <creator>
      <creatorName>Zangl, Veronika</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Evelyn Annuß: Volksschule des Theaters. Nationalsozialistische Massenspiele.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2020</publicationYear>
  <dates>
    <date dateType="Submitted">2020-10-23</date>
    <date dateType="Updated">2020-11-18</date>
    <date dateType="Issued">2020-11-18</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-347-4321</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Überformen und überschreiben sind stets wiederkehrende Begriffe, wenn es darum geht dramaturgische und ästhetische Formen der NS-Massenspiele zu charakterisieren, während überbieten und die rhetorische Stilfigur der Hyperbole die politische Propaganda kennzeichnen. Joseph Goebbels Proklamation eines Theaters der Hunderttausend – mit der er sowohl Benito Mussolinis Aufruf zu einem Theater der Zwanzigtausend und Max Reinhardts Theater der Fünftausend überbietet – verdeutlicht gleichzeitig, dass ästhetische und gouvernementale Praktiken während der NS-Zeit nicht nebeneinander existieren, sondern zutiefst miteinander verschränkt sind. Der Frage nach der wechselseitigen Bezogenheit von Ästhetik und politischer Propaganda (S. 9) geht Evelyn Annuß denn auch in ihrer material- und detailreichen Studie zur Volksschule des Theaters konsequent nach.
&#13;

"Nationalsozialistische Massenspiele", so der Untertitel der Publikation, umfassen die zunächst von Goebbels unterstützten und später verworfenen Thingspiele, die von der Rosenberg-Fraktion favorisierte Landschaftsbühne bis hin zu den Festspielen im Rahmen der Olympiade 1936 und darüber hinaus. Annuß beschränkt ihre Studie jedoch nicht auf eine historische Untersuchung ästhetischer Formkonzepte und der damit einhergehenden Formprobleme, sondern verbindet diese mit einer umfassenden diskursanalytischen und mediengeschichtlichen Herangehensweise. Dadurch ergibt sich eine diachrone und synchrone Achse der Analyse, die das Phänomen der NS-Massenspiele zeitlich sowohl be- als entgrenzen.
&#13;

Konkret bedeutet dies, dass Annuß die (weitgehend) chronologisch geordneten Fallbeispiele einerseits dramaturgisch und formspezifisch untersucht und andererseits mit "Denkfiguren" konfrontiert, die diesen vorausgehen oder nachfolgen. So bilden im ersten Kapitel mit dem Titel "Regierungskünste" zwei Inszenierungen von Hanns Niedecken-Gebhard – das im Sommer 1933 inszenierte Stück Heilige Heimat in Ober-Ingelheim sowie Das Spiel von Job dem Deutschen im November 1933 in der Messehalle Köln – den Ausgangspunkt, um zentrale Konzepte des sich herausbildenden Massentheaters herauszuarbeiten. Unter dem Vorzeichen von Volkswerdung und Vergemeinschaftung kommt dem Chor als Kollektivfigur eine Schlüsselposition zu, der sich zugleich als Formproblem gegenüber der dramatischen personae einerseits und der Inszenierung der Führerinstanz andererseits erweist. Annuß verschränkt die dramaturgische und formspezifische Analyse, mit der sie etwa die Anlehnung an die historische Avantgarde, Expressionismus und Mysterienspiel (Reinhardt) aufzeigt oder die Liturgie als Formzitat verdeutlicht – mit einer Diskursanalyse, die die Anforderungen der Propaganda, die Vereinnahmung und Gleichschaltung von bestehenden Vereinskulturen wie Laienspiel sowie die AkteurInnen aus den Bereichen Theaterwissenschaft und Kultur berücksichtig. Daneben scheut sie nicht vor Exkursen zu zeitgenössischen TheatermacherInnen wie Einar Schleef oder Christoph Schlingensief zurück, um formspezifische Differenzen zu verdeutlichen.
&#13;

Bereits im ersten Kapitel zeichnen sich die grundlegenden Spannungslinien in Bezug auf die Formprinzipien der Massenspiele einerseits und das Verhältnis von Kunst und Propaganda/Politik andererseits ab: es geht hierbei insbesondere um das Verhältnis von Chor und Einzelfigur und um das Ausloten der Schnittstelle zwischen Fiktion und Politik. Wird mit den Inszenierungen von Niedecken-Gebhard Bewegung als zentrales Dispositiv von Massenspielen und Propaganda ausgewiesen, so kreisen die folgenden Kapitel um die Dispositive des Hörens im Zusammenhang mit dem Thingspiel und des Visuellen im Rahmen der Landschaftsbühne. Die "Okkupation des Hörraumes" (S. 85) zeichnet die Autorin auf der Grundlage der politischen Massenveranstaltungen anlässlich der 1. Maifeier 1933 sowie des Erntedankfestes am Bückeberg am 30. September 1934. Mit Rekursen auf Richard Wagners Meistersinger und Johann G. Fichtes "erziehungsstaatlich begründete Stimmmodell" (S. 87), wird die Mobilisierung der Massen durch die Führerstimme (S. 85) beziehungsweise die akustische Produktion von Erlebnisgemeinschaften verdeutlicht, die mit dem massiven Einsatz von Technik und der Möglichkeit von live-Übertragung über Radio einhergeht. In einer vergleichenden Darstellung kommunistischer Chorstücke, sozialdemokratischer Weihespiele und dem NS-Chorspiel rückt die "Okkupation der Vertikale" (S. 117) in den Blick, die sowohl auf die Inszenierung der Stimme (als Sound) als auch auf die Choreografie des Chores im Raum zutrifft.
&#13;

Gilt in den Anfangsjahren des nationalsozialistischen Regimes die propagandistische Aufmerksamkeit vor allem der affektiven Aktivierung des Publikums im Sinne der Volkswerdung, so steht die Entwicklung des Architekturtheaters und der Thingbühne im Zeichen der Disziplinierung. In den Blick rücken hierbei nicht nur die Allianz von Technik, Medien, Kulturbereich und Propaganda, sondern auch die Anschlussfähigkeit der sich etablierenden Theaterwissenschaft (S. 202) – mit Carl Niessen als einem der Protagonisten des Thing-Netzwerkes. Insofern sich bei Thingstätten szenischer Raum und Versammlungsraum geradezu programmatisch überschneiden, wird wiederum das Verhältnis von Fiktion und Politik virulent, hinsichtlich der Formproblematik jedoch vor allem die Allegorisierung der Einzelpersonen in den Thingspielen. Einen Ausweg bietet Lothar Müthel mit der Inszenierung von Der Weg ins Reich in Heidelberg (1935). Der Chor erhält hier Züge des Ornaments, die Volksgemeinschaft wird nicht durch Affizierung evoziert, sondern schuldet sich der Drohung, verkörpert durch die Gestaltung der Gegenseite als komische Figur, die es auszuschließen gilt. Hier zeichnet sich der Weg der Volkwerdung und Gefolgschaft im Sinne einer Politik der exklusiven Inklusion ab (S. 452).
&#13;

Lässt sich das Nationalsozialistische Thingprojekt mit Annuß als Labor und durchaus modernes Experimentierfeld politischer Kundgebung zur Produktion von Erlebnisgemeinschaften lesen, so trifft dies ebenso auf die Landschaftsbühne, das Konkurrenzproject der Rosenberg-Fraktion, zu. Entscheidend ist hierbei, dass das Landschaftstheater das Formproblem Chor und Allegorie über das Visuelle zu lösen versucht und hierbei bei der Wahrnehmungsregulierung des Films anknüpft. Als weiteres theatrales Mittel der Organisation der Perspektive erweist sich das Panoptikum, das Ende des 19. Jahrhunderts seine Konjunktur erlebte. Überraschend und zugleich bezeichnend ist hierbei, dass Annuß die durch das Panoptikum gewährleistete Produktion des Realitätseffekts nicht nur mit Roland Barthes, sondern auch durch die Linse der panoptischen Montagen von Yadegar Asisi (2013) liest.
&#13;

Das Projekt des NS-Massenspiels endet zwar nicht mit den Festspielen im Rahmen der Olympischen Spiele, findet hier jedoch mit dem Ornament der Masse, bei dem das Publikum sich selbst zum Schau- und Hörobjekt wird (S. 408), seine abschließende Transformation. Den sich stets wieder manifestierenden Formproblemen und Formlösungen und den damit einhergehenden Subjektivierungsangeboten geht Annuß präzise, geradezu unermüdlich und unterstützt durch umfangreiches Bildmaterial nach, ausgehend von den Stadionspielen über Architekturtheater und Thingbühne zu den Landschaftsbühnen und mit den Olympischen Spielen 1936 wieder zurück in die Stadien.
&#13;

Im Postscriptum formuliert Annuß: "Die Geschichte nationalsozialistischer Massenspiele und der Medienmigration theatraler Regierungskünste lässt sich nicht angemessen schreiben, ohne das Verhältnis von Propaganda und Lagern, von Massenkultur und Massenvernichtung zu adressieren und dabei auch unsere Praktiken des Erinnerns zu reflektieren." (S. 440) Obwohl Ausschlussmechanismen sowie die Inszenierung rassistischer Blut- und Bodenpropaganda immer wieder angesprochen werden, bleibt die explizite Reflexion des genannten Verhältnisses dem Postscriptum vorbehalten. Weder die Präfiguration der Lager noch der Massenvernichtung in und durch die Massenspielen und Massenspektakel an der Schnittstelle von ästhetischer und politischer Praxis kommt explizit zur Sprache. Das heißt nicht, dass die nationalsozialistische Exklusions- und Vernichtungspolitik der in diesem buchstäblich schweren Buch verhandelten Frage der NS-Massenspiele nicht eingeschrieben wäre. Doch gerade die minutiöse, detaillierte Analyse von ästhetischen Formfiguren in Verbindung mit Fragen der Subjektivierungsangeboten stellt die Möglichkeit in Aussicht, das Verhältnis von Propaganda und Lagern im Sinne der Präfiguration zu untersuchen. Eine der Fragen, die sich hierbei stellt, ist, inwiefern neben Mechanismen des Überschreibens, Überformens und Zitierens nicht auch Strategien der Aneignung und Auslöschung im Spiel sind.
&#13;

Die Fülle an Material, die permanente Verschiebung der analytischen Perspektive, das Sezieren der den Fallbeispielen eingeschriebenen Diskursen sowie ihrem Nachleben macht die Volksschule des Theaters zu keinem einfach lesbaren Buch, die gewählte Form des Schreibens ist aber letztlich konsequent im Sinne eines kritischen Subjektivierungsangebots im Prozess der Lektüre.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2020/2</description>
  </descriptions>
</resource>
