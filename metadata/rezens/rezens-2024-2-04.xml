<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2024-2-04</identifier>
  <creators>
    <creator>
      <creatorName>Huber, Hanna</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Sarah Thomasson: The Festival Cities of Edinburgh and Adelaide.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2024</publicationYear>
  <dates>
    <date dateType="Submitted">2023-06-15</date>
    <date dateType="Accepted">2024-09-20</date>
    <date dateType="Updated">2024-11-13</date>
    <date dateType="Issued">2024-11-13</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="JournalArticle"/>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-675-8057</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
In 2003, Karen Fricker, theatre critic and professor at Brock University in Canada, observed that "[f]estivals are a complex, and undertheorized, field within theatre studies." (Fricker 2003, p. 79) She argues further that "little work has been done within the academy to analyze the ways that contemporary festivals function, and the meanings they contain and disseminate" (ibid.).  Sarah Thomasson's 2022 publication The Festival Cities of Edinburgh and Adelaide makes an important contribution to filling this research gap. By locating her research at the intersection of Theatre and Performance Studies and Cultural Geography, Thomasson develops an interdisciplinary framework for Festival Studies that can be expanded to other festival cities like Edmonton, Grahamstown, or Avignon.


Broadly speaking, The Festival Cities of Edinburgh and Adelaide explores how festivals materially and discursively shape and are shaped by the urban environment of their host cities. In the wake of World War II, the Edinburgh International Festival (EIF) was established to redefine a European (cultural) identity by presenting theatre, opera, dance, music, film, and visual art. Even before the inaugural event, the EIF was confronted with criticism by local artists for the lacking representation of Scottish productions, which provoked a fringe movement (EFF) from the very first edition. The Adelaide Festival was established through cooperation between civic authorities and local businesses in 1960 and modelled on the EIF. Comparable to the EFF, the fringe movement in Adelaide developed spontaneously in opposition to the conservative programming choices of the Adelaide Festival. Today, the two case-study cities host a plethora of different arts events: for instance, Adelaide Writers' Week and WOMADelaide within Adelaide's "Mad March", and the Edinburgh International Book Festival and Royal Edinburgh Military Tattoo on the opposite side of the globe in August.


In each chapter different key moments, controversies, and debates sparked by the Edinburgh and Adelaide festivals are examined over the past ten years. These discursive, inherently performative events "spilled out beyond the festivals' performance spaces, […] intersected with broader concerns and activated the public sphere" (p. 27). The book moves beyond the analysis of individual theatre performances, complies with Ric Knowles' perception of festivals as "meta-performance" or, in other words, "cultural performance that embeds other performances within itself" (Knowles 2020, p. 2), and reads the festivals as performances of the city, sites of contestation that foreground urban tensions and conflicting interests.


The first chapter entitled "The Place Myth of the Festival City" narrates the urban histories of Adelaide and Edinburgh and investigates how the Festival City place myth has developed in the different local contexts. Throughout the book, Thomasson is cautious to also contextualise the two cities' differences in order to avoid "reiterating colonial narratives in which a European cultural phenomenon is transplanted unaltered into a postcolonial setting" (p. 2). She notes, for instance, that Edinburgh's residents have historically had an "antagonistic relationship with the EIF", while "Adelaide has embraced its festival—declaring itself the capital of the 'Festival State'" (p. 20). The chapter proceeds from the observation that the international arts festivals of Edinburgh and Adelaide "pre-date the emergence of urban entrepreneurialism in the 1980s and the widespread adoption of creative cities discourses in the 2000s" (p. 18). The Festival City place myth is considered "a symbolic image that has grown out of the international reputation of these cultural events" (p. 71). More recently, however, "it has been harnessed within place promotion to serve particular agendas" (p. 71). Festivalisation is pursued as an urban entrepreneurial strategy today to achieve economic growth and create a positive city image. City authorities, motivated by the need for place promotion, cultural tourism, and the multiplier effect for local businesses, actively promote the Festival City place myth.


This place myth is taken up in the following chapter. "Culture Wars: The Festivalisation of Public Space" investigates local resistance to the place myth and conflicts within place-making narratives. Thomasson applies concepts proposed by geographers like Don Mitchel, David Harvey, and Andrew Smith to analyse conflicts over the right to use and define public urban space and picks out two exemplary events: The Adelaide festivals in March 2012 overlapped with the Clipsal 500 V8 supercar race, whereupon the open-air concert by Ennio Morricone and the Adelaide Symphony Orchestra at the festival's opening night was disturbed by the sound of V8 engines from the nearby racetrack; and, in 2018, the construction of high black boards for the Edinburgh Summer Sessions, a ticketed concert of popular music, not only obscured the views of the concert, but also of Edinburgh Castle, and blocked residents' access to West Princes Street Gardens, which led to a debate over the commercialisation of public space. Thomasson argues, "Increased competition from the global proliferation of arts festivals and other urban events […] undermines this monopoly rent and contributes external pressure to the sustainability of the Festival City place myth" (p. 83). The chapter demonstrates the benefits of extending the analytical framework and understanding festivals as performance (see Knowles' notion of "meta-performance", as cited above). Performative events that "spilled out beyond the festivals' performance spaces" (p. 27) foreground urban tensions by putting competing agencies in friction with each other. The two exemplary events activated debate in the public sphere on topics like the eventification and commodification of public space.


In the following chapter "Entrepreneurialism on the Fringe", Thomasson unmasks the fringe festivals' open-access policy and reliance on the precarious labour of artist-participants as key tenets of neoliberalism. Economic liberalism implicitly erodes fringe festivals' values of accessibility, equality, and artistic freedom, and forces artist-participants to become creative entrepreneurs in order to survive in the competitive environment. Thomasson identifies the paradox at the very heart of open-access fringe festivals: "On the one hand, [the Fringe] grew out of opposition to the inherent elitism and agenda of an invitation-only festival, but on the other, this same artistic freedom appears to lend itself easily to the promotion of free-market competition and the potential for individual artists to incur high levels of personal debt" (p. 128). By analysing the dangers of open-access as an economic model and the delusive rhetoric of endless growth – debates sparked by artists like Stewart Lee at the EFF in 2012, Alexis Dubus at the Adelaide Fringe in 2016, and the Fair Fringe Campaign in 2017 – the chapter investigates how open-access fringe festivals normalise and reinforce neoliberal ideology. Luckily, Thomasson does not stop here, but continues by observing and suggesting interventions and future directions at open-access fringe festivals like the cooperation with local authorities to cut participation costs, remove the pressure for perpetual growth, improve the accessibility for artists and audiences, create opportunities for future presentation of shows beyond the festival, and reduce the event's carbon footprint.


The chapter "Performing Nation: Revisionist Histories on the World Stage" examines how international arts festivals function as sites for national identity formation by presenting the nation in the context of a global arts market. In her analysis of two festival productions that engage with national history – Rona Munro's The James Plays and Andrew Bovell's stage adaptation of The Secret River – Thomasson demonstrates that "festivals provide a space for debating questions of national belonging and identity that are of central concern within the public sphere" (p. 167). She also advocates to provide more opportunities for Indigenous-led productions as important steps in the process of decolonialisation and reconciliation.


The conclusion entitled "Looking Beyond the Pandemic" explores the festivals' responses to the COVID-19 pandemic and its irrevocable impact on performing arts festivals, relying on travel, interaction, and co-presence. The Edinburgh's 2020 summer festivals were cancelled for the first time in their history; the Adelaide festivals continued by complying to health and safety regulations. Thomasson concludes that restrictions on social gathering during the pandemic have "reinforced the importance of face-to-face contact and the need for the carnivalesque celebration that these modern, urban social rituals provide, even in their most commercialised forms" (p. 1). The chapter offers a valuable documentation of how the Edinburgh and Adelaide festivals experienced the pandemic in 2020 and 2021, by also comparing the different impact on the international arts festivals and their fringe counterparts. Especially for the latter, the reduction in scale due to COVID-19 has shown that "a smaller fringe is not necessarily a negative and can in fact improve the fringe experience for audience and artist alike" (p. 209).


All in all, The Festival Cities of Edinburgh and Adelaide offers a convincing interdisciplinary and transnational approach for Festival Studies. The compelling case-study analyses of different performative events provide the opportunity to explore local and national concerns as well as current debates on a broader level. Thomasson proposes an expedient methodological framework to analyse festivals' institutional structure as well as their role in local economies, place promotion, and the construction of national identities, which can be taken as inspiration for research projects on other international arts festivals and their fringe counterparts.


 


Literature:


Fricker, Karen: "Tourism, The Festival Marketplace and Robert Lepage's The Seven Streams of the River Ota". In: Contemporary Theatre Review 13/4, 2003, pp. 79–93.


Knowles, Ric: "Introduction". In: The Cambridge Companion to International Theatre Festivals. Ed. by Ric Knowles, Cambridge: Cambridge University Press 2020, pp. 1–12.
</description>
  </descriptions>
  <relatedItems>
    <relatedItem relationType="IsPublishedIn" relatedItemType="Journal">
      <relatedItemIdentifier relatedItemIdentifierType="EISSN">2072-2869</relatedItemIdentifier>
      <titles>
        <title>[rezens.tfm]</title>
      </titles>
      <issue>2024/2</issue>
    </relatedItem>
  </relatedItems>
</resource>
