<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2023-1-14</identifier>
  <creators>
    <creator>
      <creatorName>Kapfer, Leonie</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Lauren Fournier: Autotheory as Feminist Practice in Art, Writing, and Criticism.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2023</publicationYear>
  <dates>
    <date dateType="Submitted">2023-03-22</date>
    <date dateType="Updated">2023-05-10</date>
    <date dateType="Issued">2023-05-10</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-624-7934</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Die Kunsthistorikerin, Künstlerin und Kuratorin Lauren Fournier liefert in ihrer Monografie Autotheory as Feminist Practice in Art, Wirting and Criticism eine umfassende Analyse autobiografischer und/oder autofiktionaler Medienpraktiken. Im Zentrum von Fourniers Auseinandersetzung steht die Frage wie diese Praktiken mit Theorie und Erkenntnisproduktion verschaltet sind. Zur Klärung gräbt sich die Autorin durch die Theorie- und Kunstgeschichte und untermauert das feministische Argument, dass die descartesche Trennung von Körper und Geist, Verstand und Gefühl immer schon ein Trugschluss war. Fournier betont, dass sowohl die big patriarchs der modernen Theoriegeschichte – Marx, Nietzsche, Freud – als auch ihre Vorgänger* wie Rousseau oder Kant eigene Erfahrungen in ihre Werke verstrickten (S. 36-37). Es waren jedoch vor allem Frauen* und BIPoC-Personen, die diese Praxis sichtbar machten, benannten und strategisch einsetzten. Ihnen wurde die Möglichkeit, eine vermeintlich objektive und ent-körperlichte Perspektive einzunehmen, aufgrund sexistischer, rassistischer und klassistischer Zuschreibungen abgesprochen. Ziel feministischer kultureller Praxis war und ist es daher, die Zuschreibung als ver-körperlicht – das heißt sinnlich-sexuell – strategisch einzusetzen und umzudeuten. Hier argumentiert Fournier im Sinne der Autorin Chris Kraus, deren Buch I Love Dick für sie eine Hauptreferenz darstellt. Kraus interpretierte das Teilen von Persönlichem in "weiblicher" Kunst als eine ästhetisch-kreative Strategie um: "If women have failed to make 'universal' art because we’re trapped within the 'personal' why not universalize the 'personal' and make it the subject of our art?" (Kraus 2015, S. 195). Feministische Kunst und Theorie will somit immer auch den "göttlichen Trick" offenlegen, der die bürgerlich-männliche, weiße und heterosexuelle Subjektposition als transzendentale Position inszeniert (vgl. Haraway 1995). Feministische kulturelle Praktiken sind für Fournier daher immer schon Autotheorien.


Zeitlich liegt der Fokus der Monografie auf Kunst und Literatur seit den 1970er Jahren, obwohl Fournier auch auf Arbeiten von Aktivistinnen* und Schriftstellerinnen* wie Mary Wollstonecraft oder Sojourner Truth oder Künstlerinnen* wie Zora Nele Hurston oder Claude Cahun eingeht. Es waren jedoch vor allem die postmodernen Theorien als auch die Performance- und Videokunst, denen die Autotheorie ihren heutigen Stellenwert verdankt: "In the 1970s, the feminist art movement in America foregrounded women’s bodies as active and conceptual, while feminist poets and philosophers working in France sought ways to express the female body and subjectivity through writing" (S. 11).


In den an die Einleitung anschließenden Analysekapiteln werden daher ausführlich Arbeiten von Künstlerinnen* und Aktivistinnen* behandelt. Sehr überzeugend werden beispielsweise die autotheoretischen Arbeiten der US-amerikanischen Künstlerin Adrienne Piper beschrieben. Piper setzte sich in Werken wie Food for the Spirit (1971) mit der Kant’schen Ästhetik und ihrer Rezeption durch den zu ihrer Zeit renommierten Kunsthistoriker Adam Greenberg auseinander. Während der Formalist Greenberg die "Interessenslosigkeit" der Kunst als Qualitätsmerkmal hochhielt, legt Piper in ihren autodokumentarischen Körperperformances, die Widersprüche der Kant’schen Philosophie offen: "[Piper] reveals to be one of Kant’s best readers: she understands the paradox of Kant’s philosophical system – predicated on sensorial perspective, and thus on the body, while at the same time failing to consider the body in its gendered, racialized and abled aspects" (S. 97). Pipers autodokumentarisch-theoretische Arbeiten nehmen so Einfluss auf die Theorierezeption ihrer Zeit.  


Autotheoretische Arbeiten zeichnen sich jedoch nicht nur durch eine kritische Rezeption von bestehenden Theorien aus. Sie sind auch maßgeblich an der Entwicklung von "neuem" situiertem Wissen beteiligt. Fournier verweist auf die lange Tradition dieser Praxis: Bereits in den Arbeiten von bell hooks und Audre Lorde werden Alltagserfahrungen für theoretische Überlegungen produktiv. Intersektionale Theorien basieren somit immer auch auf einer Theoretisierung von Erfahrung. Am Beispiel der Latinx Künstlerin @gotshakira bespricht Fournier dieses Phänomen im Kontext der zeitgenössischen Internetkultur. @gotshakira produziert "confessional femme memes", die feministische Theoriediskurse um Sexismus und Rassismus aus dem akademischen Raum lösen und in Memes verwandeln. Die virale Wirkung und der institutionskritische Charakter entstehen vor allem auch über die Zurückweisung korrekter englischer Grammatik, welche die Künstlerin von der Meme-Kultur übernimmt (S. 129).


In ihren Analysen liefert Fournier so eine ausführliche Genealogie der Autotheorie und ihrer Einflüsse auf akademische, künstlerische und literarische Diskurse. Zudem arbeitet die Autorin auch die Möglichkeit heraus, Theorie durch autotheroetische Zugänge zu de-kolonialisieren: "While this might seem a utopic promise, one that might at first appear overly optimistic, I think the range of works emerging over the past half-century that engage autotheory in subversive and liberatory ways points to the possible radical, and ethical, capacities for autotheory as an inclusive mode with deep feminist roots. […] Autotheory can serve as a way of critiquing and transforming existing colonial discourse of philosophy and theory through such practices as autothereotical citation, […] through forms of community building and radically empathic, cross-experimental collaborations that considerately and curiously engage the personal alongside the critical" (S. 270). So wurde der intersektionale Feminismus über autotheoretische Zugänge entwickelt. Zugleich verweist Fournier auch auf einige "Risiken" der Autotheorie: "With the term autotheory, though, comes the risk of a potentially excessive methodological individualism that places primacy on the autos or self in a way that could bring the questionable logics of late postmodernism full circle (My truth is truth and your truth is your truth) or the logics of those most militant forms of intersectional thinking (I am the most marginalized by the center-margin logic of the given context and so I will be the only one to speak)" (S. 272). Erst eine kritisch selbst-reflexive Praktik, die kollektiv agiert, verhindert Solipismus. Fournier verweist hier auf die Tatsache, dass autotheoretische Arbeiten nicht unmittelbar transgressiv wirken und hält fest, dass Autotheorie, entgegen der Meinung einiger Kritiker*innen, keine "simple" und "oberflächliche" Praxis ist, die aus Narzissmus und mangelnder Selbstdistanz hervorgeht. Im Gegenteil, Fourniers Arbeit unterstützt die medienkulturwissenschaftliche These, dass erst durch distanzierende literarische und/oder künstlerische mediale Praktiken eine Selbstdistanz möglich wird, die in kritisch transgressiven Haltungen und Theorien münden kann – aber nicht muss.


Während Fournier eine umfassende und überzeugende Analyse künstlerischer und literarischen Praktiken liefert, fällt ihr die Einordnung populärkultureller bzw. massenkultureller Phänomen allerdings teilweise schwer. In einem letzten Kapitel versucht sie die #metoo-Bewegung in ihre Analysen miteinzubeziehen und verstellt sich, meiner Meinung nach, die Sicht durch eine subtil zwischen den Zeilen aufscheinende Unterscheidung von Kunst und Massenkultur. Denn es sind für die Autorin vor allem Soziale Medien die "dekontextualisieren" und "verkürzen" (S. 171). Obwohl Fournier gleichzeitig die Bedeutung der #metoo-Bewegung betont, ist es auffällig, dass sie der Autotheorie gerade dann mit Skepsis begegnet, wenn sie aus einem Massenphänomen hervorgeht. Interessanter wäre es an dieser Stelle vielleicht auf die Unterschiede zwischen Autotheorie als eine postmoderne Theorieform beziehungsweise Praxis und der #metoo-Bewegung, die versucht den Diskursen einer positivistischen Rechtsprechung gerecht zu werden, hinzuweisen. Dieser kulturkritische Ton im letzten Kapitel schmälert allerdings nicht den akademischen Wert von Fourniers Monografie. Sie lieferte eine detaillierte und längst überfällige Genealogie und Analyse von Autotheorien, die noch dazu durch einen persönlichen und selbstkritischen Ton besticht. Mit Autotheory as Feminist Practice in Art, Wirting and Criticism ist es der Autorin gelungen, die rezente Hinwendung zum Privaten theoretisch zu fundieren und das kritisch feministische und dekoloniale Potential von autobiografischen Texten, Performances und/oder Videos herauszuarbeiten.


Literatur:


Haraway, Donna: "Situiertes Wissen. Die Wissenschaftsfrage im Feminismus und das Privileg einer partialen Perspektive". In: Die Neuerfindung der Natur. Primaten, Cyborgs und Frauen. Hg. v. dies., Frankfurt am Main: Campus 1995, S. 73-97.


Kraus, Chris: I Love Dick. London: Tuskar Rock, 2015.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2023/1</description>
  </descriptions>
</resource>
