<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2021-2-12</identifier>
  <creators>
    <creator>
      <creatorName>Rothenburger, Nadja</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Martina Dobbe/Francesca Raimondi (Hg.): Serialität und Wiederholung. revisited.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2021-10-12</date>
    <date dateType="Accepted">2021-11-03</date>
    <date dateType="Updated">2021-11-30</date>
    <date dateType="Issued">2021-11-30</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-533-6386</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Spätestens seit Gertrude Steins Gedichtzeile Rose is a rose is a rose is a rose und den Improvisationen der Dada-Künstler*innen spuken Wiederholungen als diskursive und stilistische Versatzstücke durch die mitteleuropäische und US-amerikanische Kulturgeschichte. Dementsprechend fragt die Theaterwissenschaftlerin Kristin Joy Kalu in ihrer Studie zu einer möglichen Ästhetik der Wiederholung "welche Wirkung der Wiederholung als ästhetischer Strategie noch eignen kann, wenn sie sich pausenlos wiederholt."(Kalu 2013, S. 249) Mit dem vorliegenden Band veröffentlicht der Berliner August Verlag (seit diesem Jahr als Imprint von Matthes &amp; Seitz) eine weitere Reflexion dieses Diskurses, wobei sich die Publikation auch als Beitrag im Zeichen einer kritischen künstlerischen und theoretischen Praxis situiert. Die Herausgeberinnen Martina Dobbe und Francesca Raimondi (letztgenannte ist Mitbegründerin des August Verlags) verorten sich mit ihrer wissenschaftlichen Forschung in der Philosophie und Kunstgeschichte, wodurch auch der Band verschiedene medien-, tanz- sowie kunstphilosophische und -historische Perspektiven auf gegenwärtige ästhetische Praxen der Wiederholung versammelt. Flankiert von der grafischen Serie Spinning (John Morgan, 2021) und einem Bildessay Trauer und Melancholie (Olaf Nicolai, 2009/2012/2021) reflektieren mit der Einleitung acht wissenschaftliche Beiträge über Serialität und Wiederholung.


In ihrer Einführung beschreiben Dobbe und Raimondi das titelgebende Begriffspaar als "Paradigmen der Kunst und des Denkens" und "Verfahrensweisen" (S. 9) eines Diskurses, der seit den 1960er Jahren vermehrt geführt wird. Als "künstlerische, popkulturelle, ästhetische und philosophische Konzepte" setzen die Autorinnen diese ins Verhältnis zu Topoi des "Neuen" und der "Originalität" (S. 9). Kategorien wie "Künste der Wiederholung" (S. 11), "Denken der Wiederholung" (S. 18), "Performanzen der Wiederholung" und "Wiederholtes a/Ausstellen" (S. 29) dienen ihnen dabei zur Orientierung und gruppieren die sieben Beiträge.


Aus kunsthistorischer Sicht argumentiert Dorothea von Hantelmann (S. 37-54) entlang zweier Arbeiten von Pierre Huyghes Untilled (2012) und After ALive Ahead (2017) die Verschränkung von Lebendigkeit und Wiederholung. Hantelmann betont die spezifische Situierung der beiden, schreibt ihnen einen ikonografischen Status und eine transgredierende Wirkung zu, da diese "zu einer radikalen Neubestimmung sowohl der Frage der Kunst wie auch der Ausstellung" (S. 37) führten. Dies, indem sie den Ausstellungsort in seiner konkreten Materialität einbeziehen (unter anderem durch Pflanzen, Splitt, Ameisenhügel etc.) und mittels digitaler Technologien transformieren. Dadurch entstehe eine Kunst, "die sich nahezu von selbst produziert und reproduziert" (S. 54), das Lebendige ästhetisiert und mit Wiederholungsverfahren verschaltet.


In "'Ask Elaine'. Wiederholung, Reproduktion und Mimesis" durchdenkt Maria Muhle diese Konzepte als sogenannte "mindere Techniken der Kunst" (S. 57), wobei die Bezeichnung 'mindere' auf "die von der ästhetischen Verwerfungsgeschichte aussortierten nachahmenden, imitatorischen, reproduzierenden, wiederholenden Praktiken, […] wie das Kopieren, das Nachstellen oder das Mimikrysieren" (S. 61) abhebt. Mit Rückbezug auf Elaine Sturtevants Aneignungs- beziehungsweise Wiederholungs-Verfahren zeigt Muhle deren spielerischen Umgang mit Original und Kopie, das zugleich anti-modernistisch Autor*innenschaft und einzigartiges Schöpfer*innentum verneint als auch modernistisch das Wesen der Kunst zu ergründen sucht. Dies eröffne laut Muhle wiederum "eine Linie zum Digitalen" (S. 77), die das heutige zugespitzte Vervielfältigen, Zirkulieren und Distribuieren von Bildern mittels neuer Technologien über den Blick zurück auf Sturtevant erläutert und zu verstehen sucht.


Einen Ansatz im Grenzbereich von Populärkultur und Ästhetik wählt Friedrich Balke in seinen "Anmerkungen zu einem Museumsbesuch in Breaking Bad" (S. 81). Der Medienwissenschaftler analysiert einen Teaser der dritten Staffel der TV-Serie, in der die Hauptfiguren Jane Margolis und Jesse Pinkman das Georgia-O'Keeffe-Museum in Abiquiu besuchen – ein Ereignis, das in der Serie erzählerisch angedeutet, aber nicht eingelöst wird. Am Beispiel dieser "Analepse" (S. 82) entwickelt Balke u. a. mit Deleuze/Guattari den Begriff der "Empfindungsblöcke" (S. 81), die sich aus jenen Wiederholungen speisen, denen eine "morphologische Dimension" innewohnt (S. 86). Die aus dieser Untersuchungsanordnung hervorgehenden Verästelungen, intermedialen sowie motivischen Wiederholungs- und Übertragungsvorgänge – mal fokussiert Balke serielle Wirk- und Wiederholungsprinzipien von Breaking Bad, mal jene der Künstlerin O'Keeffe – eröffnen eine Vielzahl unterschiedlicher Perspektiven auf das debattierte Begriffspaar des Sammelbandes.


Die beiden tanz- beziehungsweise theaterwissenschaftlichen Artikel von Maren Butte und Sabine Huschka widmen sich Fragen der Erinnerungspolitik und des Flüchtigen in den aufführenden Künsten. Buttes Betrachtungen skizzieren Serialität und Wiederholung aus multiperspektivischer und diskursanalytischer Sicht und gliedern diese in Praxis, Entzug, Archiv und Fortdauer (vgl. S. 105-122). Der Ausgangspunkt ihrer Argumentation ist das widersprüchliche Verhältnis der Denkfiguren des Flüchtigen von Performances (im Sinne einer "Nicht-Wiederholbarkeit") und Wiederholung als "grundlegende[n] Verfahren der Aufführungskünste" (S. 105).  Entlang einer eindrücklichen Anzahl einschlägiger deutsch- beziehungsweise englischsprachiger theoretischer und ästhetischer Positionen zeigt Butte, dass es gilt "Performance selbst als Wiederholung sichtbar [zu] machen" (S. 105) und als "lebendige[s] Archiv" zu begreifen (S. 106). 


Sabine Huschka nähert sich über Wiederholungsstrategien dem Erinnern im zeitgenössischen Tanz und reflektiert diese als "'historiographische' Tanzpraktiken" (S. 125). Die Tanzwissenschaftlerin arbeitet heraus, wie die zeitgenössische künstlerische Auseinandersetzung mit tanzgeschichtlichen Erinnerungen als "ästhetischer Reflexionsraum" fungiert, in dem "überarbeitet, weitergeführt, (wieder-)gefunden und (wieder-)erfunden" wird (S. 127). Dies veranschaulicht sie anhand der choreografischen Projekte von Jochen Roller (The Source Code, 2012), Christina Ciupke und Anna Till (undo, redo and repeat, 2013) und Henrietta Horn (Le Sacre du Printempsvon Mary Wigman, 2013) und kommt zu dem Schluss, dass für eine vertiefte Analyse dieser Entwicklungen zwischen tänzerisch-körperlichen Wiederholungsverfahren und kunstwissenschaftlichen Diskursen beziehungsweise Re-Enactments der Performancekunst unterschieden werden muss. Die beiden letztgenannten würden im Zuge einer "bildkritischen Praxis" als "Akt der Verlebendigung gefasst", wohingegen dem Tanz aufgrund dessen medialer Disposition der "Akt einer Vergegenwärtigung" bereits innewohne (S. 146).


Mit der Verschränkung kuratorischer und ästhetischer Praxen beschäftigt sich der Beitrag der Kunsthistorikerin Beatrice von Bismarck "Trans(pos)ition: In der Sprache des Kuratorischen" (S. 149-165). Mit Walter Benjamins Übersetzungs-Begriff versteht Bismarck die "transpositionale Grundstruktur des Kuratorischen" (S. 151) als relationales Gefüge, das insbesondere dann produktiv wird, wenn die "angestammten Bindungen" des gewählten Ausstellungszusammenhangs gelockert und in "neue Konstellationen" überführt werden (S. 164). So untersucht sie Dominique Gonzalez-Foersters Roman de Münster (2007) als Übertragung von Ausstellungen (Skulptur Projekte Münster 1977, 1987, 1997, 2007) in eine Installation, die den "Fokus von den Einzelwerken auf ihr Bezugssystem" verschiebt (S. 157), wobei "[d]er für die Transposition gewählte Modus der Fiktionalisierung […] vorangegangene Bedeutungen, Funktionen und Status" (S. 159) neu denkt und (be-)schreibt.


Nina Möntmann beleuchtet in ihrem Beitrag das Kuratieren wiederum aus institutioneller und diskursanalytischer Perspektive (vgl. S. 167-191). Am Beispiel der documenta X und der Documenta II erläutert die Kunsttheoretikerin ihre Thesen zur Umwertung des Ausstellungsformats in Wechselwirkung mit der 'Moderne' beziehungsweise dem Konzept des Zeitgenössischen (vgl. S. 167). Mit der Denkfigur vom "Sturz in die Welt" (S. 173f) entfaltet Möntmann ihre Argumentation und den Versuch "die globalisierte Gegenwart aus einer kolonialen Vergangenheit" zu denken (S. 178f). Sie detektiert und erläutert ein "Prinzip der Aktualisierung" (S. 189), welches sich auf den Möglichkeitshorizont jener künstlerischen Arbeiten bezieht, die "dokumentarische Elemente mit fiktiven und poetischen" (S. 189) kombinieren. Anstatt also "die verlorenen Schlachten der Vergangenheit als Scheitern zu akzeptieren", bilden die entstandenen Möglichkeitshorizonte den Auftakt auf diese "Kämpfe als noch offene, nicht abgeschlossene Aufgabe zu blicken, die wiederbelebt werden muss." (S. 189)


Serialität und Wiederholung fungieren demnach als inhaltliche und formale Klammern des Sammelbandes, mit dem die Herausgeber*innen und Autor*innen diese Prinzipien (re-)konstruieren, aktualisieren und (re-)vitalisieren. Es sind dies unabgeschlossene und unabschließbare Projekte, deren Potenzial für Kunst, Theorie und Politik – dies gelingt es zu zeigen – trotz und wegen ihrer Profanisierung noch nicht erschöpft ist.


Literatur:
Kalu, Joy Kristin: Ästhetik der Wiederholung. Die US-amerikanische Neo-Avantgarde und ihre Performances. Berlin: Transcript Verlag 2013, S. 249.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2021/2</description>
  </descriptions>
</resource>
