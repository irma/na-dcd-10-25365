<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2021-2-02</identifier>
  <creators>
    <creator>
      <creatorName>Huber, Hanna</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Julia Buchberger/Patrick Kohn/Max Reiniger (Hg.): Radikale Wirklichkeiten.  Festivalarbeit als performatives Handeln.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2021-09-06</date>
    <date dateType="Accepted">2021-11-22</date>
    <date dateType="Updated">2021-11-30</date>
    <date dateType="Issued">2021-11-30</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-533-6340</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
In der Theaterwissenschaft lässt sich eine Umbruchs- bzw. Aufbruchsphase zu neuen Forschungsgebieten beobachten; neben ästhetischen Fragen wird zunehmend eine "Inblicknahme der institutionellen, ökonomischen und sozialen Rahmenbedingungen von Theater" relevant und eine interdisziplinäre Öffnung zu "neuen Methoden" diskutiert. (Wihstutz/Hoesch 2020, S.10) Diese Tendenz geht mit dem Bestreben einher, Kulturproduktion als ganzheitliche Praxis und im Kontext gesellschaftspolitischer Veränderungen zu begreifen. Der Sammelband Radikale Wirklichkeiten. Festivalarbeit als performatives Handeln, herausgegeben von Julia Buchberger, Patrick Kohn und Max Reiniger, verschreibt sich eben diesem Forschungsinteresse und beleuchtet künstlerische, technische und organisatorische Tätigkeitsbereiche der Festivalarbeit. Ein Blick hinter die Kulissen wird versprochen, dem angesichts der praxisnahen beruflichen Hintergründe der Beitragsverfasser*innen auch nachgekommen wird.


Die Herausgeber*innen verbindet die kollektive Arbeit am transeuropa fluid, dem Europäischen Festival für Performative Künste, im Mai 2018 in Hildesheim. Dieser gemeinsam durchlebte Prozess bildet somit den Erfahrungshorizont, auf Basis dessen der Sammelband Einblick in die "radikalen Wirklichkeiten" von Festivalarbeit geben möchte. Als roter Faden dient die Absicht, Festivalarbeit als performatives Handeln zu denken – dies reicht von Selbstinszenierungsstrategien der Künstler*innen während der Festivalbewerbung, bis hin zu performativen Auftritten der Geschäftsführung in Pressekonferenzen und Beiratssitzungen.


Die drei Kapitel des Sammelbands diskutieren Theaterfestivals als Produktions- und Arbeitsumfeld, das Wechselspiel zwischen institutionellen Strukturen und "ästhetisch wahrnehmbaren Aspekten von Festivals" (S. 20), sowie unsichtbare Tätigkeitsbereiche der Festivalarbeit, wobei sich die jeweilige Kapitelzuordnung der einzelnen Beiträge nicht immer auf Anhieb erschließt. Gelungen ist die abwechslungsreiche Zusammenstellung von Praxisberichten bis hin zu theoretischen Überlegungen, verschriftlichten Gesprächen und freieren Textformen.


Im Rahmen eines Interviews mit Juliane Beck und Yvonne Whyte vom PACT Zollverein untersucht Anna Barmettler Residenzformate als "Stationen (auch offener) Arbeitsprozesse" (S. 43). Insbesondere die Förderung ergebnisoffener Theaterprojekte durch die Bereitstellung von Produktionsinfrastruktur ermöglicht kreativen Freiraum, abseits eines neoliberalen Leistungs- und Effizienzdenkens. Auf Theaterfestivals hingegen – mit deren ökonomischer Funktion als "Schau" und "verdichtetes Sichtungsangebot für Kuratierende" (S. 55), wie Anne Bonfert betont – bestreiten Akteur*innen neben ihrer künstlerischen Performance, dem dargebotenen Resultat ihres kreativen Schaffensprozesses, auch eine Arbeitsperformance als Unternehmer*innen.


Dass wirtschaftliche Kalkulation neben künstlerischer Innovation zunehmend an Relevanz gewinnt, erkennt auch Benjamin Hoesch in seiner Durchsicht des Bewerbungsarchivs des internationalen Theater- und Performance-Festivals PLATEAUX. Bewerbungsverfahren um eine Festivaleinladung eröffnen einen "Wettbewerb der Subjekte" (S. 35), der nicht ohne Selbstvermarktungs- und Inszenierungsstrategien auskommt. Mit der Performanz des Arbeitssubjekts befasst sich ebenfalls Bianca Ludewigs empirische Untersuchung zu Praktika und Volunteering auf Transmedia Festivals, die sie als "Wirklichkeitsausschnitte der Spätmoderne" (S. 62) begreift. Demnach weisen Festivals auf gesellschaftspolitische Entwicklungen bzw. Problematiken hin, wie u. a. "die Prekarisierung und Entwertung von (Kultur-)Arbeit" (S. 63).


Die vier genannten Beiträge enden z. T. mit Lösungsansätzen und Zukunftswünschen: Um dem steigenden Selbstvermarktungstrend auf Festivals entgegenzuwirken, fordert Hoesch u. a. eine Erhöhung der "Aufführungshonorare und Koproduktionsbeiträge" sowie "längerfristige Verbindungen zwischen Künstler*innen und Festivals" (S. 40). Bonfert betont die Notwendigkeit einer gelungenen Balance zwischen "künstlerischer Performance" und "Arbeitsperformance", die nur dann sichergestellt werden kann, wenn auch Tätigkeiten hinter den Kulissen – eben jene "informelle und unsichtbare Arbeit" – stärker als "offizielle Arbeit" Anerkennung finden (S. 59). Hier ließe sich argumentieren, dass bereits die diskursive Auseinandersetzung mit der Thematik "Kunst als Arbeit" – dazu zählt auch Annemarie Matzkes Professionalisierungsbiografie (Matzke 2018) – ein öffentliches Bewusstsein generiert und folglich Veränderungspotential birgt.


Die Beiträge des zweiten Kapitels befassen sich mehrheitlich mit einer diskriminierungskritischen Sichtweise auf Festivalarbeit. Lisa Scheibner, neben ihrer Tätigkeit als Schauspielerin und Kulturwissenschaftlerin auch Referentin für Sensibilisierung und Antidiskriminierung bei Diversity Arts Culture, beschreibt die Zielsetzung und Angebote dieser Beratungsstelle für Diversitätsentwicklung im Kulturbetrieb. Ihr Beitrag beinhaltet v. a. konkrete Arbeitsschritte in Richtung einer "diskriminierungssensiblen Öffnung der eigenen (Festival-)Strukturen" (S. 99) und richtet sich somit insbesondere an Führungskräfte von Festivals oder Kulturbetrieben.


Ebenso betont Martine Dennewald, von 2015 bis 2020 künstlerische Leiterin des Festivals Theaterformen, die Wichtigkeit, nicht bloß "innerhalb des künstlerischen Programms, auf der Bühne, Diversität und Gleichstellung […] zu predigen", sondern diese Grundsätze auch "im eigenen System umzusetzen" (S. 112). Es "genügt" daher nicht, Arbeiten von Künstler*innen aus Afrika, Südostasien oder Südamerika in das Festivalprogramm aufzunehmen, ohne sich um eine diskriminierungskritische Prozessbegleitung zu kümmern. Einen ähnlichen selbstreflexiven Ansatz verfolgen Fanti Baum und Olivia Ebert, indem sie "Gastgeben als Infragestellung der eigenen Souveränität und der eigenen Struktur" (S. 117) begreifen.  


Auch wenn sich Festivals oftmals als "Wegbereiter der Inklusion" verstehen, bringen sie – laut Yvonne Schmidt – "eigene Mechanismen der Exklusion" hervor (S. 145). Bereits die Internationalisierung der Festivalszene steht in Konflikt zu der nicht immer gegebenen Reisefähigkeit von Ensemblemitgliedern.


Nun hat eine diskriminierungssensible Perspektive in Verbindung mit dem fluiden, temporären Charakter von Festivals, der dazu einlädt, "mit neuen Formen und Inhalten zu experimentieren" (S. 88), großes Veränderungspotential. Innovative Arbeits- und Produktionsweisen können erdacht und erprobt werden, und derart auch richtungsweisend für die gesamte Kulturlandschaft sein. So werden mitunter "Konzepte von Slowness […] und Conviviality" auf Disability Arts Festivals umgesetzt, die einen "Kontrast zu neoliberalen Logiken des Produzierens" bilden (S. 153).


Malte Wegners Arbeitsberichte, die er als Geschäftsführer des Festivals Theaterformen verfasst hat, wären eher im dritten Kapitel anzusiedeln. Hier werden jene Tätigkeitsbereiche der Festivalarbeit thematisiert, die weniger öffentliche Aufmerksamkeit erlangen und doch wesentlich zum Gelingen der Veranstaltung beitragen. Thomas Friemel, Mitbegründer der freitagsküche, erläutert beispielsweise, wie durch Festivalgastronomie "horizontale Gesprächssituationen" geschaffen werden (S. 190). Esther Bold reflektiert ihre Erfahrungen als Autorin und Theaterkritikerin, wobei sie Festivals als "verdichtete Erfahrungsräume" beschreibt, die "heterogene Kunsterfahrungen" ermöglichen (S. 208). Antonia Rohwetter und Philipp Schulte diskutieren ihre Beobachtungen im Rahmen des Veranstaltungsformats Festivalcampus.


Verena Elisabet Eitels "Architektonik des Temporären" steht den Texten des zweiten Kapitels thematisch näher. Anhand von vier exemplarischen Projekten diskutiert Eitel, wie Festivalarchitekturen "zu Schwellensituationen zwischen den Sphären werden können und damit Erfahrungs-, Ideen- und Möglichkeitsräume eröffnen" (S. 176). Daran anschließend analysiert Nicola Scherer Festivalzentren als "Inbetweens" (S. 185), produktive Zwischenräume und Orte der Begegnung.


Die vielseitigen Beiträge des Sammelbands, deren Ausgestaltung zwischen Theorietiefe und Praxisnähe deutlich variiert, bieten eine kurzweilige und abwechslungsreiche Leseerfahrung. Sowohl wissenschaftliche Positionen als auch persönliche Erfahrungsberichte aus der Festivalarbeit sind dabei vertreten. Somit wird Diversität nicht nur inhaltlich behandelt (siehe Kapitel 2), sondern auch in der Zusammenstellung des Sammelbands war man darum bemüht, zumal Stimmen aus Theorie und Praxis gleichermaßen Gehör finden.


Literatur:


Wihstutz, Benjamin/Hoesch, Benjamin (Hg.): Neue Methoden der Theaterwissenschaft. Bielefeld: transcript 2020, S. 10.


Matzke, Annemarie: "Sich selbst professionalisieren – zur Figur des Performancekünstlers im gegenwärtigen Theater". In: De-/Professionalisierung in den Künsten und Medien. Formen, Figuren und Verfahren einer Kultur des Selbermachens. Hg. v. Stefan Krankenhagen/Jens Roselt, Berlin: Kulturverlag Kadmos 2018.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2021/2</description>
  </descriptions>
</resource>
