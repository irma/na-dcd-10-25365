<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2020-2-08</identifier>
  <creators>
    <creator>
      <creatorName>Sommerlad, Elisabeth</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Michael Pekler/Andreas Ungerböck: Wien. Eine Stadt als Filmkulisse.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2020</publicationYear>
  <dates>
    <date dateType="Submitted">2020-04-30</date>
    <date dateType="Accepted">2020-11-10</date>
    <date dateType="Updated">2020-11-18</date>
    <date dateType="Issued">2020-11-18</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-347-3601</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Die österreichische Bundeshauptstadt Wien hat eine Tradition und bewegte Geschichte als Filmschauplatz. Unzählige internationale und nationale Filme wurden hier gedreht und haben der Stadt dabei viele unterschiedliche Gesichter gegeben. Auf dieser Tatsache (These?) baut das im Jahr 2019 von Andreas Ungerböck und Michael Pekler publizierte Buch Wien – Eine Stadt als Filmkulisse auf und macht es sich zum Ziel, die vielfältigen filmischen Facetten der Stadt aufzudecken und nachzuspüren. Erschienen ist es als fünfter Band der Reihe on location: Reiseführer zu den Orten des Kinos im Schüren Verlag, in der bereits ähnliche Unternehmungen für Los Angeles, Thailand/Kambodscha/Vietnam und Kroatien unternommen wurden.
&#13;

Die mannigfaltige Auseinandersetzung und Analyse filmisch imaginierter Städte steht seit vielen Jahren auf der Agenda filmwissenschaftlicher und filmgeografischer Forschungen (Sharp/Lukinbeal 2019). Dabei hat sich insbesondere der hochinteressante Gegenstand der Cinematic City etabliert. Damit sind Städte gemeint, die in Filmen nicht nur als Location-Lieferant, Schauplatz oder Kulisse fungieren, sondern filmübergreifend als filmische Größe immer wieder neu belebt und mitinszeniert werden. (Escher/Zimmermann 2005, S. 162f; Clarke 1997)
&#13;

Eine solche Cinematic City ist zweifelsohne auch Wien, wie die Autoren in ihrem Vorwort eindrücklich darlegen. Wien dient im Film dabei nicht nur als populärer Schauplatz, sondern "inszeniert sich […] immer auch als Attraktion" (S. 7). Dabei zeigen Ungerböck und Pekler auf, dass Wien zwar nicht so prominent sei wie viele andere Filmstädte, diesen jedoch aufgrund seiner besonderen Rolle in nichts nachstehe: "Wien besitzt zwar nicht die Eleganz von Paris, auch nicht die Extravaganz von London oder die Exaltiertheit von Rom. Doch Filme, die in Wien spielen, sind in gewisser Weise auch immer damit beschäftigt, die Geschichte und die Gegenwart zu verbinden, so wie es auch das kulturelle und geistige Wien selbst seit über hundert Jahren mit unterschiedlichem Ergebnis versucht" (S. 8).
&#13;

Die Autoren geben im ersten Teil des Buches eine knappe Übersicht darüber, wie sich Wien in Spielfilmen präsentiert bzw. präsentiert wird: als Attraktion in populären Spielfilmen, dunkle Peripherie, historisch-verklärter Schauplatz der Nachkriegszeit, Agent*innenumschlagplatz, Fluchtort und flüchtiger Ort, Bühne für Künstler*innenbiografien, exklusiver Blockbuster-Schauplatz, trist-graue Großstadt sowie multikulturelle Metropole – um nur einige Beispiele anzuführen. Der einleitend dargelegte Überblick zeigt, dass Wien als filmische Stadt durchaus ambivalent inszeniert wurde und wird. Umso deutlicher wird dabei, dass die Stadt viel mehr ist, als bloße Kulisse für filmische Narrationen, wie es der Titel des Buchs unterbreitet: Das filmische Wien ist nicht nur eine Filmkulisse oder ein schlichtes Ensemble gefilmter Orte und Locations – sondern vielmehr eine dichte ästhetische Konstruktion, ein komplexes, semantisch aufgeladenes Konstrukt, das sich aus dem Zusammenspiel räumlicher Perspektive, städtischer Sehenswürdigkeiten und inszenierter sozialer Problemlagen ergibt.
&#13;

Der Fokus des Buchs ist insbesondere aus filmgeografischer Perspektive überaus interessant, befasst sich diese geografische Disziplin doch insbesondere mit den vielschichtigen, filmisch erzeugten geografischen Konstruktionen und ihren Auswirkungen in alltäglichen Lebenswelten. Im Format eines Reiseführers verzichtet das Buch dabei auf theoretisch-konzeptionelle Ausschweifungen und macht diese spannende Thematik einem breiten Publikum zugänglich. Einer akademischen Leser*innenschaft mag eine intensive wissenschaftliche Fundierung unter Umständen fehlen. Es ist jedoch auch nicht das primäre Anliegen des Buchs, eine solche Einbettung und Diskussion der Thematik vorzunehmen, wenn es auch in einem wissenschaftlichen Verlag publiziert ist. Vielmehr steht im Fokus, "eine Spurensuche durch die besten, interessantesten und gefeierten Filme mit der Donau-Metropole als Kulisse" (S. 8) zu unternehmen und einen ansprechenden und breiten Überblick darüber zu geben, wie unterschiedliche Filme der Vergangenheit und Gegenwart die Stadt Wien und ihre zahlreichen Schauplätze inszenieren. Dabei werden filmwissenschaftlich/-geografisch relevante Diskurse durchaus immer wieder gestreift. In Filmen inszenierte gesellschaftliche Realitäten lassen sich stets als verdichtete Interpretationen lebensweltlicher Phänomene interpretieren, die unsere Wahrnehmungen, Handlungen und Weltbilder prägen. Die hier besprochene Publikation zeigt auf, inwiefern dies der Fall für eine filmische Stadt ist. Und auch wenn keine abstrahierende Analyse der filmischen Stadt Wien im Sinne der Cinematic City-Literatur vorgenommen wird, ist das Buch auch aus wissenschaftlicher Perspektive eine spannende Lektüre. Insbesondere bereichert es, neben seines Stellenwerts für die Wissenschaftskommunikation, auch das Feld des Filmtourismus, das gerade in den vergangenen Jahren – ökonomisch wie akademisch – immer mehr an Relevanz gewonnen hat. (Steinecke 2016)
&#13;

Der Hauptteil des Buches besteht aus einer Zusammenstellung von fünfzig exemplarischen Spielfilmen, die jeweils auf zwei bis drei Seiten vorgestellt werden. Versammelt werden hier nicht nur Wien-Filmklassiker wie Der Dritte Mann (1949) oder die Sissi-Trilogie (1955-1957), international bekannte Filme wie Before Sunrise (1995) und das Biopic Klimt (2006) oder Blockbuster wie Mission: Impossible – Rouge Nation (2015), sondern auch zahlreiche österreichische Produktionen wie Der Nachbar (1993) und Nordrand (1999), die einen subtil-differenzierteren Blick auf das Wien jenseits der bekannten Schauplätze einnehmen. Diese facettenreiche und umfassende Auswahl ist wohlüberlegt zusammengestellt und ermöglicht einen wunderbaren Überblick über Wien-Filme der vergangenen Jahrzehnte und der Gegenwart. Die kurzen Überblicks-Texte sind informativ und unterhaltsam geschrieben. Neben Auskünften zum Entstehungskontext und zum Inhalt der Filme bieten die Autoren ihren Leser*innen auch interessante Anekdoten und Insider-Details an, welche insbesondere eingefleischte Film-Fans ansprechen. Jeder Text wird ergänzt durch eine Infobox, die allgemeine Informationen zum Film gibt (beispielsweise Entstehungszeit, Regie, Darsteller*innen) sowie durch einige Screenshots, die prägnante Ansichten aus den Filmen zeigen und bekannte Szenen ins Gedächtnis rufen.
&#13;

Dabei sind jedoch vor allem die Hinweise zu den Schauplätzen der Filme von Interesse. Einige prägnante Orte sind in den Beiträgen zwar visuell hervorgehoben, finden sich jedoch oftmals nur als Aufzählung wieder. Gerade in Hinblick auf das Anliegen des Buches, auch ein Reiseführer zu sein, wäre hier eine noch intensivere Auseinandersetzung mit den einzelnen Schauplätzen und ihrer Verortung sowie mit ihren filmischen Rollen wünschenswert gewesen. Als ein weiterer kleiner Kritikpunkt kann diesbezüglich auch der Aufbau des Buches benannt werden: Die Sortierung der Filme erfolgt alphabetisch, sodass es sich zunächst mehr als Nachschlagewerk von Wien-Filmen empfiehlt, denn als tatsächlicher Reiseführer. Zwar wird das Buch mit zwei Faltplänen (1. Bezirk, innere Stadt und Gesamtplan mit weiteren Bezirken, S. 123-124) ergänzt, auf denen zentrale Schauplätze verortet sind. Hinter den markierten Orten sind jeweils auch die Filmnummern vermerkt, die dann im Buch nachgeschlagen werden können. Die Pläne sind jedoch sehr klein gedruckt, schwer lesbar und vom Maßstab her relativ ungeeignet, um sich vor Ort ohne weitere Hilfsmittel zu orientieren. Dies gilt insbesondere dann, wenn man sich nur als Laie durch die Filmlandschaft Wiens bewegt. Sollte man das Buch also tatsächlich als filmtouristischen Reiseführer nutzen wollen, empfiehlt sich definitiv eine vorbereitende Lektüre und intensive Auseinandersetzung mit der Materie.
&#13;

Diesbezüglich wäre es sicherlich hilfreich gewesen, das Buch nicht alphabetisch anhand der Filmtitel zu sortieren, sondern die Schauplätze selbst in den Fokus zu stellen – wie es beispielsweise die in der gleichen Reihe erschienenen Reiseführer zu Los Angeles oder Kroatien anbieten. Um der überaus spannenden Einleitung zu den "Ansichten einer Stadt" (S. 7) vollends gerecht zu werden, hätte es sich alternativ angeboten, den Buchinhalt entlang den verschiedenen filmischen Gesichtern der Stadt aufzubauen. Dies gilt gerade auch in Hinblick auf das wichtige Unterfangen, die imaginierten "Ansichten einer Stadt im Film" (S. 10) kritisch zu dekonstruieren, welche auch alltägliche Stadtansichten immer mitprägen, "weil in ihnen die Geschichte der Stadt bereits miterzählt wird" (S. 10).
&#13;

Trotz dieser kleineren angesprochenen Schwächen löst das Buch sein Versprechen ein und liest sich als spannender wie "unterhaltsamer Streifzug" (Klappentext) durch die Wiener Filmgeschichte. Wien – Eine Stadt als Filmkulisse präsentiert sich dabei als eine wirkliche, kleine aber feine Bereicherung auf dem Markt der Literatur zu filmischen Städten. Der Band empfiehlt sich als kurzweilige Lektüre für ein breites Publikum – ganz gleich ob man sich als Wissenschaftler*in einen kleinen Überblick über die Vielfalt der filmischen Inszenierungsstrategien Wiens machen möchte, als Film-Fan interessiert ist an Anekdoten hinter der Entstehungsgeschichte spezifischer Filme oder als Set-Jetter die nächste Städtereise vorbereitet.


&#13;

Literatur:
&#13;

Clarke, David B. (Hg.): The Cinematic City, London/New York: Routledge 1997.
&#13;

Escher, Anton/Zimmermann, Stefan: "Drei Riten für Cairo. Wie Hollywood die Stadt Cairo erschafft". In: Mythos Ägypten. West-Östliche Medienperspektiven II. Hg. v. Anton Escher/Thomas Koebner, Remscheid: Gardez 2005, S. 162-173.
&#13;

Sharp, Laura/Lukinbeal, Chris: "Imagined Cities (Cinema)". In: The Wiley-Blackwell Encyclopedia of Urban and Regional Studies. Hg. v. Anthony M. Orum, New York: John Wiley &amp; Sons 2019, S. 1-4.
&#13;

Steinecke, Albrecht: Filmtourismus. Konstanz: UVK Verlagsgesellschaft 2016.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2020/2</description>
  </descriptions>
</resource>
