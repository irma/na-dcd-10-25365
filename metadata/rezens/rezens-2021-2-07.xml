<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2021-2-07</identifier>
  <creators>
    <creator>
      <creatorName>Piechocki-Steiner, Claudia</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Henry Thorau (Hg.): Einstürzende Altbauten. Sechs Theaterstücke aus Portugal.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2021-11-29</date>
    <date dateType="Updated">2021-11-30</date>
    <date dateType="Issued">2021-11-30</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-533-6491</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Den Umschlag von Einstürzende Altbauten ziert eine Postkartenidylle, wie sie jede*r Portugal-Urlauber*in kennt. Ein enges, sich hochschlängelndes Gässchen, das von Häusern mit Azulejos-Fassaden gesäumt wird. Im Hintergrund erkennt man die Bogenbrücke Ponte Dom Luís I, die die Städte Porto und Vila Nova de Gaia miteinander verbindet. Es sind eindeutig alte Bauwerke, die sich über das gesamte Cover erstrecken. Nun stellt sich die Frage: Warum ist hier kein Theatergebäude abgebildet, wenn dieser Band sechs Dramen beinhaltet? Gibt es in Portugal etwa kein Theater?
&#13;

"In Portugal hat es niemals Theater gegeben; was man Theater nennt, hat es bei uns nie gegeben" (S. 9), soll João Baptista de Almeida Garrett (1799–1854), der als Begründer des portugiesischen Nationaltheaters gilt, einst behauptet haben. Um diese provokante Formulierung zumindest für die Gegenwart zu widerlegen, hat sich Henry Thorau, Professor em. für Brasilianische und Portugiesische Kulturwissenschaft an der Universität Trier und ehemaliger Chefdramaturg an der Volksbühne Berlin, zum Ziel gesetzt, deutschsprachigen Theaterwissenschaftler*innen und Regisseur*innen den Zugang zu andernfalls nur auf Portugiesisch erhältlicher zeitgenössischer Dramatik zu erleichtern.
&#13;

Wenn es seltene Ware gibt, dann sind es Dramen aus Portugal in Buchform – ganz unabhängig davon, in welchem Land nach ihnen gesucht wird. Laut Thorau wurde die erste Sammlung Stücke aus Portugal 1978 vom Henschelverlag Kunst und Gesellschaft in Ost-Berlin herausgegeben und beinhaltete vier Theaterstücke, die im Zeichen des sozialistischen Realismus standen. Seit der ersten Anthologie portugiesischer Dramen in deutscher Sprache sind also bereits 43 Jahre vergangen.
&#13;

Den Übersetzungen, die von Marina Spinu, Marianne Gareis und dem Herausgeber stammen, hat dieser ein Vorwort vorangestellt, in dem er – mit einem konzisen Exkurs ins 18. und 19. Jahrhundert – präzisiert, mit welchen Schwierigkeiten natürlichen und politischen Ursprungs sich das portugiesische Theater konfrontiert sah und auf welche Weise die Herausbildung einer nationalen Theatertradition, die unabhängig von höfischen und staatlichen Interessen existiert, immer wieder verzögert wurde. Denn abgesehen von einem verheerenden Erdbeben, das in Verbindung mit einem Tsunami die Stadt Lissabon am 1. November 1755 beinahe zur Gänze zerstörte, geschah es beispielsweise erst 1771, dass der Schauspielerberuf per Dekret institutionalisiert wurde. Dennoch verweigerte die Kirche Schauspieler*innen bis in die 1820er Jahre die Sakramente.
&#13;

Mit dem jungen Friedrich Schiller vergleicht Thorau Almeida Garrett, der sich 1836 im Auftrag der Regierung als Theoretiker, Reformer, Organisator und Politiker der Etablierung eines 'Nationalen Theaters' verschrieb. In Garretts Zuständigkeit fielen der Bau des Staatstheaters D. Maria II, das 1846 eröffnet wurde, die Gründung der Theaterakademie und das Verfassen von Dramen mit nationalem Charakter, von denen besonders das Heimkehrerdrama Frei Luís de Souza aus dem Jahr 1844 bekannt wurde. Darin befürchtet die Protagonistin Dona Madalena de Vilhena, ihr Ehemann könnte doch nicht bei den portugiesischen Kreuzzügen in Nordafrika gefallen sein und nach seiner Rückkehr ihre zweite Ehe vereiteln.
&#13;

Einen besonderen Fokus legt Thorau im Vorwort auf die Zeitspanne unmittelbar nach der Diktatur von António de Oliveira Salazar. Salazar regierte Portugal von 1932 bis 1968 als autoritärer Ministerpräsident und war verantwortlich für ein totalitäres System, das das Land durch kostspielige Kolonialkriege zerrüttete und bis zur sogenannten Nelkenrevolution am 25. April 1974 vom Rest der Welt abschottete. Bei dieser unblutigen Revolution war es die Armee selbst, die die Diktatur beendete – sie verweigerte sich dem Anspruch, sich für Kolonialisierungsbestrebungen als 'Kanonenfutter' zu opfern. Die Nelkenrevolution läutete den Niedergang der portugiesischen 'Überseeprovinzen' und den Beginn der Demokratie ein. Dem Theater kam in dieser historischen Umbruchszeit als Forum für politische Auseinandersetzungen ein hoher Stellenwert zu, auch auf den Bühnen der Städte Porto und Évora. Während des Salazar-Regimes war die Theaterzensur drastisch gegen Kritiker*innen vorgegangen, und ein Großteil der Dramen, die dieser Periode entstammen, wurde nie aufgeführt. Laut Thorau wurden die portugiesischen Bühnen in den post-revolutionären Zeiten von einem immer wiederkehrenden Phänomen heimgesucht: Sie setzten mit Vorliebe auf ausländische Erfolgsdramatik. Zwischen 1976 und 1978 war Bertolt Brecht der meistgespielte ausländische Autor in Portugal.
&#13;

Erst die Bestrebungen des Dramatikers Norberto Ávila, der für vier Jahre die Leitung des Theaterressorts des damals neugegründeten Staatssekretariats für Kultur übernahm, sollen zu einer Wende geführt haben. Ganz neu waren beispielsweise die Einführung einer Subventionsklausel für die Aufführung portugiesischer Stücke und ein Subventionierungsprogramm für freie Theatergruppen.
&#13;

Für tiefgreifende Veränderungen in Portugals Theaterproduktion sorgte schließlich der prominente Regisseur João Lourenço, als der Portugiesische Schriftstellerverband auf seine Initiative hin 1997 Portugals renommiertesten Dramen-Preis, den "Grande Prémio de Teatro Português", begründete. Mit diesem Preis soll unter anderem die Verbreitung zeitgenössischer portugiesischer Dramatik gefördert werden. Zusätzlich zum Preisgeld und der Publikation des Stückes ist die Prämierung mit der Uraufführung am Teatro Aberto in Lissabon verbunden.
&#13;

Einstürzende Altbauten spiegelt die jüngere Schaffensphase von portugiesischen Dramatiker*innen wider. In der Anthologie kommen die Dramen der Gegenwartsautor*innen Almeida Faria, João Santos Lopes, Tiago Rodrigues, Luísa Costa Gomes, Cecília Ferreira und Tiago Correia zur Geltung. Den Autor*innen ist gemein, dass sie dem Staub, der mit der Nelkenrevolution aufgewirbelt wurde, folgen. Sie skizzieren Bilder der historischen, politischen und sozialen Erschütterungen in einem Portugal, das nach 1974 versucht, sich auf seine Zugehörigkeit zu Europa zu besinnen und sich dem Tourismus zu öffnen. In diesen Bildern lassen sich ebenso jene Risse erkennen, die sich seit einer harten Finanzkrise und aufgrund eines aggressiven Immobilienbooms bis heute durch das ganze Land verästeln.
&#13;

Die Autor*innen verbindet außerdem der Umstand, dass sie mit den renommiertesten Literatur- und Theaterpreisen Portugals ausgezeichnet wurden. Faria erhielt u. a. den Aquilino-Ribeiro-Preis der Lissabonner Akademie der Wissenschaften; Costa Gomes bekam den "Prémio Máximo de Literatura" verliehen; Rodrigues wurde mit dem "Prémio Pessoa" prämiert; und Santos Lopes, Ferreira und Correia wurden mit dem "Gránde Prémio de Teatro Português der Sociedade Portuguesa de Autores" geehrt.
&#13;

An den Anfang stellt Thorau das Stück Umkehrung von Almeida Faria, das 1999 publiziert wurde. In diesem Stück werden vier Personen mit sozialen Umwälzungen und mit dem Verfall von gesellschaftlichen Strukturen konfrontiert. Die Lebensläufe eines alten Großknechts, einer Köchin, einer Gutsherrin und deren Sohnes verlaufen unter dem Dach eines Gutes, dessen Konstruktion ins Wanken gerät als es durch die Revolution nach 1974 zu Landreformen und zur Enteignung von Großgrundbesitzern kommt. Verfasst wurde das holzschnitthafte Lehrstück in der stilisierten Sprache eines dramatisierten Poems in zehn Kapiteln. Thorau erkennt in dieser Form eine Anspielung auf das Heldenepos Os Lusíadas von Luís de Camões, das die Geschichte des portugiesischen Königreichs thematisiert und sich ebenso in zehn Gesänge gliedert.
&#13;

Mit Manchmal schneit es im April, das 1997 als erstes Theaterstück den "Grande Prémio de Teatro Português" erhielt, legt der Autor João Santos Lopes die Finger in Portugals offene Wunden. Das macht Santos, indem er mehrere Problematiken, die dem Tabuthema der Rückkehr aus den afrikanischen Kolonien zuzuordnen sind, auf den Körper einer jungen schwarzen Frau projiziert: Es geht unter anderem um gescheiterte Remigration, Alltagsrassismus und kompensatorische Jugendgewalt. Für den Protagonisten Gabriel, der eine Gang von weißen Männern anführt, ist der Tag des Jüngsten Gerichts gekommen, an dem die gekidnappte schwarze Frau als Sündenbock herhalten muss für die Schmach und die Traumata, die Heimkehrende ab 1975 erlitten. Um seine Gang zu Gewalt und einem Krieg zwischen Schwarz und Weiß anzustiften, bedient sich Gabriel rassistischer und faschistischer Rhetorik, die das ganze Stück durchzieht. Die Schonungslosigkeit des Stücks bedürfte eigentlich einer Trigger-Warnung, wie Thorau im Vorwort anmerkt.
&#13;

Arbeitslosigkeit, Pleiten und Finanzkrise – so könnte man die Metaebene von Tiago Rodrigues' bizarrem Jugendstück und Coming-of-Age-Drama Traurig und fröhlich ist das Giraffenleben beschreiben, das 2011 uraufgeführt wurde. In Lissabon, das von diesen Vorkommnissen betroffen ist, versucht Giraffe, ein neunjähriges Mädchen, das für ihr Alter zu groß geraten ist, ihren Platz zu finden. Giraffes Vater hat keinen Job, die Mutter ist verstorben und ihr Teddybär Judy Garland leidet am Tourettesyndrom. Als besonders schlimm empfindet es Giraffe, dass ihr Vater nicht genug Geld für den Discovery Channel hat. Diesen braucht sie, um eine Schularbeit über Giraffen schreiben zu können. Gemeinsam mit Judy Garland beschließt sie, sich davonzumachen, um den Premierminister zu einem Gesetz zu überreden, das jedem ein lebenslanges Abo für den Discovery Channel garantiert. Und spätestens als Judy Garland meint: "Wer weiß schon, wohin sein Weg führt? Keiner, fuck. Wenn sie das Gegenteil behaupten, lügen sie. Wir sind alle verloren. Wir sind alle Schiffbrüchige" (S. 158), lässt sich das Stück als Metapher lesen für wirtschaftliche Aussichtslosigkeit und für das Ende des Discovery Channels, den Portugal jahrhundertelang für die überseeische Expansion abonniert hatte. Genauso wie für Giraffe ist für Portugal der Moment gekommen, sich loszulösen und unabhängig zu machen.
&#13;

Tag für Tag von Luísa Costa Gomes, die zu Portugals prominentesten Theaterautorinnen und Regisseurinnen gehört, katapultiert das Publikum in eine Gefechtszone, in der Immobilienbüros darüber entscheiden, wer obdachlos wird und wer nicht. Im Zentrum dieses Gefechts stehen zwei kaltblütige Maklerinnen, die auf die Waffen der Männer zurückgreifen, um ihr Einflussgebiet zu vergrößern. Messerscharfe Dialoge, brennende Städte und eine morbide Gesellschaft, die apathisch "Pornografie. Witze. Werbung. Weiber. Fußball." (S. 245) konsumiert: Auf diesen Pfeilern kreiert Gomes ein unsentimentales Abbild eines Landes, in dem die Teilnahme am Raubtierkapitalismus zur einzigen Überlebensstrategie wird. Auch die Spuren in die Vergangenheit scheinen verwischt zu sein: "Es gab mal eine Revolution, wusstest du das?", "Wo?", "Hier, in Portugal." […] "Davon hab ich nichts mitbekommen. War wohl vor meiner Zeit." (S. 244f.). Tag für Tag wurde 2011 im Teatro Municipal de São Luiz in Lissabon uraufgeführt.
&#13;

Eine andere weibliche Perspektive auf das Leben eröffnet sich in Cecília Ferreiras Monodrama Die Begleiterin. Ferreira, die 2013 für dieses Stück auch mit dem "Grande Prémio de Teatro Português" ausgezeichnet wurde, lässt darin die Protagonistin Luzia in einem Zimmer, in das sie sich endgültig zurückgezogen hat, Lebensbilanz ziehen. Aus den Monologen geht hervor, dass Luzia unzähligen Männern ihr Leben als Krankenschwester und Sterbebegleiterin gewidmet hat. Die Namen der Männer hat sie in einem Heft notiert, zu jedem Namen gibt es eine Geschichte. Ob sich diese tatsächlich zugetragen haben, bleibt offen. Klar ist: Luzias Biografie ist von Männern geprägt. In ihrem Beruf scheint sie unsichtbar geblieben. Nun ist Luzia allein und müde. Die Begleiterin ist eine Abrechnung mit einem Land, in dem sich feminine Lebensläufe zwischen Patriarchat und Prekariat zerstreuen: "Du bist nicht mehr die liebe Luzia, die den Mühseligen und Beladenen hilft. Du bist ein Stück Scheiße in einem stinkenden, vermüllten Quartier. Wo sind denn deine Auszeichnungen für vorbildliches Arbeitsethos? […] Ich bin eine Art Hure der Toten" (S. 293f.).
&#13;

Hoffnungslosigkeit in einer fremdbestimmten Stadt verspüren auch die Figuren in Tiago Correias Stück Turismo, das die anhaltende Problematik des Turbotourismus in Portugal thematisiert und mit der Uraufführung am 31. Januar 2020 die Aufmerksamkeit auf die Gentrifizierung von Portos Altstadt gelenkt hat. Gehetzt von der Angst, die nächste Miete nicht bezahlen zu können oder überhaupt das Dach über dem Kopf zu verlieren, sehen sich die Protagonist*innen täglich mit einer Aushöhlung ihrer Umgebung konfrontiert, mittendrin klafft der Kontrast zwischen Arm und Reich. Der fehlende Lebensraum bringt alle in Bedrängnis. Ein ausländischer Investor, der für den neuen Flughafen Naturschutzgebiete zubetoniert, navigiert ungetrübt durch das Stadtgebiet, macht verzweifelten Bewohnern unmoralische Angebote und kauft nebenbei das Nationaltheater "für 'n Appel und 'n Ei" (S. 339).
&#13;

Dieses Szenario ist in Wirklichkeit (noch) nicht eingetreten. Portugals Nationaltheater Teatro Nacional Dona Maria II wird seit 2014 von Tiago Rodrigues, dem Autor von Traurig und fröhlich ist das Giraffenleben, geleitet. Rodrigues legt Nachdruck auf die Inszenierung von portugiesischen Dramen sowie Stücken aus der Lusophonie, der Gemeinschaft aller portugiesischsprachigen Länder. Doch solange dieses Bemühen kaum subventioniert wird, fristet das portugiesische Theater ein Nischendasein. Auch ein großflächiger Brand, bei dem das Nationaltheater 1964 bis auf seine Außenwände niederbrannte, war nicht förderlich. Doch "Ruinen verbreiten Hoffnung. Sie regen zum Wiederaufbau an" (S. 23), sagt Luísa Costa Gomes. Einstürzende Altbauten zeigt das neue und firme Gerüst, in das sich portugiesische Gegenwartsdramatik einschreibt.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2021/2</description>
  </descriptions>
</resource>
