<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2021-1-10</identifier>
  <creators>
    <creator>
      <creatorName>Masoudi, Anoushirvan</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Lúcia Nagib: Realist Cinema as World Cinema Non-cinema, Intermedial Passages, Total Cinema.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2021</publicationYear>
  <dates>
    <date dateType="Submitted">2020-12-22</date>
    <date dateType="Accepted">2021-05-14</date>
    <date dateType="Updated">2021-05-20</date>
    <date dateType="Issued">2021-05-20</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-464-5338</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Der Realismus im Film war seit den 1920er- bis in die 1960er-Jahre ein beliebtes Thema von Filmtheoretiker*innen und -kritiker*innen, vom sowjetischen Filmemacher Dziga Vertov bis hin zum deutschen Filmhistoriker Siegfried Kracauer, aber auch etwa für die einflussreiche Filmzeitschrift Cahiers du Cinema und ihren Mitbegründer André Bazin. In Bezug auf die Frage nach Realismus und Film hatten Bazins Analysen und Theorien großen Einfluss, etwa auf den italienischen Neorealismus und Regisseure wie Roberto Rossellini, Vittorio De Sica und Luchino Visconti. Obwohl das Interesse am Realismus in den 1960er- und 1970er-Jahren, etwa bedingt durch (Post-)Strukturalismus, Semiotik und Psychoanalyse, abgenommen hatte, beschäftigt sich das kürzlich erschienene Buch von Lúcia Nagib wieder mit dem filmischen Realismus in Bazin'scher Tradition, während die Diskussion um den Realismus-Begriff in den letzten Jahren auch etwa für spezifischere Kontexte wie das japanische, brasilianische und iranische Kino geführt wurde (vgl. Nagib/Mello 2009).
Für Bazin meint Realismus die wirkliche Zeit der Dinge, die Dauer des Geschehens, die der Film in sich aufnehmen soll, daher versteht Bazin etwa die italienischen Nachkriegsfilme mit ihren long takes und der Suggestion eines realitätsnahen Zeitvergehens als realistisch. Während Bazin Filmemacher*innen in verschiedene, dem Bild oder der Realität verschriebene Interessensgruppen einteilt, und die Frage nach dem Einfluss der außerfilmischen Realität (vgl. Bazin 2004, S. 90–109) nur am Rande thematisiert, stellt sich Lúcia Nagib die Frage nach Realismus und Film in einer breiteren Perspektive. In der Einleitung ihres Buches führt sie verschiedene Perspektiven auf Realismus zusammen und klassifiziert so drei verschiedene Ebenen von Realität im Film:


- Realismus als Modus der Produktion
- Realismus als Modus der Rezeption, den sie in den letzten Jahrzehnten als besonders präsent empfindet
- Realismus als Modus der Ausstellung, worunter sie etwa Kinosäle, 3D- oder 4D-Arrangements oder Virtual Reality fasst (vgl. S. 28).


Mit ihrem Fokus auf den Modus der Produktion beschreibt Nagib, basierend auf Bazins Theorien, die Rolle von Schauspieler*innen, die Drehorte der Filme sowie die Bewegungen der Kamera für die Filmaufnahme (wie etwa long takes). Diesen Realismus als Modus der Produktion unterteilt sie dabei erneut in drei Kategorien: Non Cinema, Intermediale Passage und Total Cinema.
Unter "Non-Cinema" versteht Nagib die Reproduktion des 'realen Lebens' qua Film. Diese Perspektive analysiert sie anhand der Produktionssituationen von diversen Filmen. Dabei spielen für sie die Beziehungen innerhalb des Filmteams oder die Konflikte zwischen Filmemacher*innen und Schauspieler*innen eine zentrale Rolle – eine Perspektive, die einen neuen Blick auf Filmanalyse in Bezug auf die Verzahnung von Produktion und Realität ermöglicht. Als "Intermediale Passage" versteht Nagib die Transformationen zwischen verschiedenen Kunstformen und Kino, die ebenfalls eine eigene Realität produzieren können. Zum Schluss beschreibt sie das sogenannte "Total-Cinema" als ein Kino, das den Wunsch nach einer Totalität insofern verkörpert, als dass es etwa versucht, mit einem Film die gesamte Geschichte eines Landes zu erzählen oder die ganze Welt durch monumentale Landschaften zu repräsentieren.


Die eingangs genannten Realismus-Perspektiven werden im zweiten Teil des Buches dann auf den Begriff des "World-Cinema" übertragen. Nagib versteht das World-Cinema als neuen Begriff für "Realist Cinema". Sie kritisiert die eurozentrischen Perspektiven von Filmwissenschaft und -theorie und arbeitet heraus, dass der Dualismus zwischen Hollywood und Europa, der meist im Fokus des Fachdiskurses stünde, einen Blickwinkel auf das Welt-Kino, mit dem sie die Perspektive marginalisierter Nationalkinematografien beschreibt, verstellen würde.
Die Auswahl der Filme, die im Buch analysiert werden, passt sehr gut zu diesem Argument. Fast jedes Kapitel beschäftigt sich mit einem oder mehreren Filmen aus verschiedenen Ländern – ob diese aus Deutschland oder Japan, Brasilien oder dem Iran kommen, ist dabei zweitrangig, im Vordergrund stehen die Effekte der Zusammenschau dieser heterogenen Arbeiten. Die Filme sind zwar nach den drei Kategorien (Non-Cinema, Intermediale Passage und Total Cinema) ausgewählt, ihre bricolagehafte, heterogene Auswahl erzeugt trotzdem einige Verwirrung beim Lesen des Buches.


Im ersten Kapitel analysiert Nagib beispielsweise Wim Wenders The State of Things (1982) und im zweiten This is not a Film (2011) des Iraners Jafar Panahi. Es handelt sich um einen Spielfilm und um einen Dokumentarfilm, die beide sehr diverse Themen verhandeln, wie etwa Kolonialismus, Feminismus oder Gender. Diese Auswahl hat auf der einen Seite den Vorteil, ein heterogenes Bild zu formen, anderseits wirkt die Beliebigkeit zeitweise chaotisch. Die zentrale Frage, die Nagib diesen Filmen stellt, ist jene danach, wie diese Filme als realistisch verstanden werden können. Das erste Kapitel stellt außerdem die Frage nach dem Tod des Kinos. Der dort analysierte Film The State of Things solle, so Nagibs steile These, zeigen, dass das Hollywood- und das europäische Kino bereits tot seien. Wenders, so Nagib weiter, inszeniere diesen Status durch zwei unterschiedliche Filmästhetiken. Dabei greife er einerseits auf Special Effects, Farben und rapide Kamerabewegungen zurück und andererseits auf Schwarz-Weiß-Kontraste als Realismus-Elemente.
Um das Konzept des Non-Cinema anhand konkreter Beispiele zu erklären, verwendet Nagib den im Iran verbotenen Film This is not a Film von Jafar Panahi. Weil Jafar Panahi offiziell nicht mehr im Iran drehen darf, versucht dieser, sein Leben mit Smartphone-Kameras zu filmen. Die Orte seiner Filme sind etwa seine eigene Wohnung oder seine private Villa in Nord-Iran und der Protagonist des Films ist Panahi selbst. Sein eigenes Leben zu filmen, ist eine politische Reaktion auf das über ihn verhängte Berufsverbot durch die islamische Regierung. Auch im Dokumentarfilm Act of Killing (2012) von Joshua Oppenheimer erkennt Nagib das Prinzip des Non-Cinema: hier würden Realität und Fiktion zusammenfallen, wenn indonesische Kriminelle ihre eigene brutale Geschichte des historischen Massakers von 1965 bzw. 1966 erzählen. Nagibs Analyse inkludiert filmtheoretische Prämissen, Interviews, politische Nachrichten und Produktionsmodelle des Films.


Im zweiten Teil des Buches steht neben dem World-Cinema auch die Intermediale Passage im Fokus. Wie andere Kunstformen, etwa Malerei oder Theater, könnten Filme eine andere Form von politischen, sowie historischen Realitäten der Gesellschaft darstellen. Dafür analysiert Nagib die Rolle von Schauspielmethoden und Theater in den zwei sehr wichtigen japanischen Filmen The Story of the Last Chrysanthemums (1939, Kenji Mizoguchi) und Floating Weeds (1959, Yasujirō Ozu) mit einem Blick auf das Kabuki-Theater (vgl. S. 127). Nagib versucht hier zu zeigen, wie japanische Filme eine andere Realität, basierend auf der japanischen Geschichte und Theatertradition, reflektieren können. Dabei greift sie auf unterschiedliche Quellen zurück, wie etwa auf autobiografische Bücher über die beiden Regisseure, die Geschichte des Kabuki-Theaters im Japan und Interviews mit den Schauspieler*innen der Filme. Darauf folgt eine Analyse von Mysteries of Lisbon (2011) des chilenischen Regisseurs Raúl Ruiz. Am wichtigsten für den analytischen letzten Teil von Nagibs Publikation ist aber das zeitgenössische brasilianische Kino, für das die Autorin den Zusammenhang zwischen Nationalidentität und Diktaturzeit erklären will. In diesem zweiten Teil gerät die Bazin'sche Perspektive aus dem Fokus und die Autorin verwendet weitere Theorien zum Realismus, etwa von Bertolt Brecht, Walter Benjamin, Gilles Deleuze, Alain Badiou und Jürgen Habermas. Klar wird allerdings auch durch diese theoretische Unterfütterung nicht, warum die von der Autorin angesprochenen intermedialen Passagen eine Rolle für ein neues Verständnis von Realismus und Film spielen.


Der letzte Teil des Buchs soll schließlich das dritte Konzept, das Total Cinema, durch die strukturelle Nähe zu anderen Künste wie Oper oder Musik erklären. Nagib versucht durch eine sehr genaue Analyse der Strukturen der Opernvorlage von Luchino Viscontis Film Ossessione (1943) zu zeigen, dass ein Film die "Totalität" einer Oper besitzen kann. Totalität bedeutet für sie eine Kunstform, die sich aus verschiedenen Kunst- und Literaturformen wie Musik, Tanz, Schauspiel und Lyrik zusammensetzt und so ein eigenes Wesen ausbildet (vgl. S. 201). Außerdem wird in dieser Sektion noch Edgar Reitz' Langzeitdokumentation Die zweite Heimat: Chronik einer Jugend (1992) analysiert. Edgar Reitz erzählt die deutsche Geschichte in dieser langen Serie durch verschiedene Themen (Migration, Krieg, Tradition), Zeiträume (Erster und Zweiter Weltkrieg) und andere Kunstformen (Musik, Tanz, Theater). Nagib versteht diese vielfältigen Themen als Totalität, weil Reitz auf diese Weise eine 'ganze' Geschichte erzählen will. Der Begriff des Total Cinema bleibt trotzdem bis zum Ende des Buches unklar, weil anschließend noch sehr diverse Filme aus Brasilien mit sehr unterschiedlichen Inhalten als 'Total Cinema' beschrieben werden.


Die Heterogenität von Material und Themen zieht sich durch das gesamte Buch und hinterlässt schlussendlich den Eindruck, dass Lúcia Nagib dem Diskurs über Realismus und Film – selbst über die Perspektive des World Cinema – keine neuen Befunde hinzufügen konnte. Hergestellt wird vielmehr ein chaotischer Blick auf Realitätsdarstellungen in einer Vielzahl an heterogenen Filmen unterschiedlicher Nationalität.


 


Literatur:


Bazin, André: Was ist Film? Hg. v. Robert Fischer. Berlin: Alexander 2004.


Nagib, Lúcia/Mello, Cecília: Realism and the Audiovisual Media. Wiesbaden: Springer 2009.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2021/1</description>
  </descriptions>
</resource>
