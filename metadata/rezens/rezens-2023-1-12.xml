<?xml version="1.0" encoding="utf-8"?>
<resource xmlns="http://datacite.org/schema/kernel-4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd">
  <identifier identifierType="DOI">10.25365/rezens-2023-1-12</identifier>
  <creators>
    <creator>
      <creatorName>Andriamaro, Jakob</creatorName>
    </creator>
  </creators>
  <titles>
    <title>Benjamin Beil/Gundolf S. Freyermuth/Hanns Christian Schmidt/Raven Rusch (Hg.): Playful Materialities.</title>
  </titles>
  <publisher>[rezens.tfm]</publisher>
  <publicationYear>2023</publicationYear>
  <dates>
    <date dateType="Submitted">2023-03-22</date>
    <date dateType="Accepted">2023-04-27</date>
    <date dateType="Updated">2023-05-10</date>
    <date dateType="Issued">2023-05-10</date>
  </dates>
  <language>de</language>
  <resourceType resourceTypeGeneral="Text">Article</resourceType>
  <alternateIdentifiers>
    <alternateIdentifier alternateIdentifierType="publisherId">7-624-7933</alternateIdentifier>
  </alternateIdentifiers>
  <rightsList>
    <rights rightsURI="http://creativecommons.org/licenses/by-sa/4.0">Dieses Werk steht unter der Lizenz Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International.</rights>
  </rightsList>
  <descriptions>
    <description descriptionType="Abstract">
Der Sammelband Playful Materialities entstand aus dem gleichnamigen "Game Studies Gipfel" (Köln TH) anlässlich der 11. "Clash of Realities. International conference on the Art, Technology and Theory of Digital Games" (2021). Aufgeteilt auf vier Kapitel, "Places", "Exhibits", "Modifications" und "Pieces", finden sich 12 Aufsätze, die Verschränkungen von game culture und material culture untersuchen. Die Texte bieten diverse theoretische und methodische Zugänge aus der Medienkulturwissenschaft, der Film- und Theaterwissenschaft sowie den Science-Technology-Studies.


Eröffnet wird der Sammelband durch "Vegas, Disney and the Metaverse" von Gundolf S. Freyermuth mit einer umfangreichen 80-seitigen Untersuchung der Interrelationen virtueller und materieller Realitäten. Nachdem eine Genealogie virtueller Realitätserfahrungen erarbeitet wurde, theoretisiert Freyermuth basierend auf Walter Benjamin und Neal Stephenson die technisch-ästhetische Antizipation (digitaler) Medialität am Beispiel der Entwicklung von Las Vegas in der zweiten Hälfte des 20. Jahrhunderts (S. 18ff). In der Untersuchung zeigt er, wie im Falle von Las Vegas ab 1950 mediale Modifikationen in der Stadtplanung, der Architektur der Gebäude, deren Ausstattung und dem Unterhaltungsangebot eingesetzt wurden, um virtuelle Erfahrungen zu erzeugen. Diese Virtualität wurde ab den 1980ern transmedial re-materialisiert, wodurch es zu einer tiefgehenden Ergründung digitaler Medialität kam, die unter anderem um Momente der Immersion und Partizipation erweitert wurde (S. 60).
Der zweite Text "Augmenting Materialitites" von Isabel Grünberg, Raven Rusch und David Wildemann knüpft an den vorangegangenen Beitrag an und untersucht Augmented Reality anhand der Rauminstallation Maschinenklangwerk (Gründberg/Rusch/Wildemann 2022). Die theoretische Aufarbeitung der Installation, deren Räume über Partizipation durch digitale, auditive Elemente erweitert werden können, ergründet die Materialität und Medialität von Augmented Reality. Dabei begreifen die Autor*innen Maschinenklangwerk als "Ludification", und beschreiben damit eine Alternative zu dem Begriff Gamification, die Partizipation, Immersion und Agency als Einladung statt als Zwang versteht (S. 103). Daran anschließend wird kritisiert, dass in gängigen Diskursen und Anwendungen, digitale Erweiterung meist auf Bildschirme reduziert wird (S. 107). Durch alternative Angebote gelingt die kritische Hinterfragung und Ausweitung des gängigen Verständnisses von Augmented Reality. Dabei will der Text eine diversere wissenschaftliche und künstlerische Auseinandersetzung sowie eine breitere technische Umsetzungen anregen.


Der erste Aufsatz des Abschnitts "Exhibits", "Let's Play the Exhibition" von Isabelle Hamm, zeigt anhand der virtuellen auditiven Ausstellung "Kid A Mnesia Exhibition" (Radiohead 2021) exemplarisch wie Ausstellungspraktiken digitaler Inhalte aussehen können. Dabei bleibt der Text meist deskriptiv: Beschrieben wird die Ausstellung aus einer Besuchendenperspektive, knüpft jedoch mit der Untersuchung des Verhältnisses von Ausstellungspraktiken und Ludification an den Text "Augmenting Materialitites" an. Der Beitrag "To Craft a Game Arts Curators Kit" von Rene G. Cepeda und Chaz Evans ist eine Prozessdokumentation der Entwicklung eines "Curators Kit", einem Leitfaden für die Kuration digitaler Inhalte. Durch den Fokus auf Verfahren der Entwicklung, beschränkt sich die Leistung des Textes auf eine Hilfestellung bei der Produktion eines solchen Kits und die Kuration digitaler Inhalte. Der Text "On Chainsaws and Display Cases" von Benjamin Beil schließt daran mit einer gründlichen theoretischen Abhandlung der Medialität des Museums und dessen Verhältnis zu Videospielen an. Dabei werden gängige Formen der Ausstellung digitaler Spiele aufgearbeitet und in museumstheoretischen Diskursen eingeordnet.


Mit ähnlichem theoretischem Tiefgang wird der Abschnitt "Modifications" durch den Text "Unpacking the Blackbox of Normal Gaming" von Markus Spöhrer eröffnet. Während schon der vorherige Artikel u. a. einen Chainsaw Controller untersucht hat, nähert sich der Text in einem soziomateriellen Ansatz Videospiel-Controllern mit Fragen der Zugänglichkeit und bedient sich den Science-Technology-Studies, Dis-/Ability-Studies, Akteur-Netzwerk-Theorie sowie der affordance theory. Dabei wird der Controller, neben dem menschlichen Körper sowie der Hard- und Software, als Akteur in Konfigurationen von Gaming verstanden. Je nach Konfiguration entstehen dabei affordances, die normale und abnorme/abled und disabled Körper produzieren. Der Text "Being a Child Again Through Gameplays" von Cordula Heithausen analysiert Perspektiven und Identifikation von Kindern in Videospielen, um diese praktisch in Form eines Game-Prototypen umzusetzen. Einen Fokus setzt Heithausen auf Fragen des Game Designs und bleibt dadurch jedoch theoretisch unterkomplex und teilweise spekulativ. Der Anschluss an Materialität wird im Wesentlichen durch das Vorkommen von Spielzeug im Prototyp argumentiert, dabei bleibt eine tiefergehende theoretische Auseinandersetzung aus und der thematische Anschluss an Materialität schwach. 
Anders argumentiert der letzte Text des Abschnitts, "Lego Level Up" von Hanns Christian Schmidt, der game literacy als mediale Lesekompetenz denkt. Auf eine gründliche theoretische Ausarbeitung folgt hier ein materieller Hands-on-Approach in Form eines game literacy-Workshops für Kinder, der wiederum in die Theoriebildung miteinfließt.


Der letzte Abschnitt, "Pieces" beginnt mit dem Text "Beyond Pawns and Meeples" von Peter Podrez, der an die theoretische Grundlage des Textes "Unpacking the Blackbox of 'Normal Gaming'" anknüpft. Dabei werden ANT und affordance zheory herangezogen, um analoge Spielfiguren als Akteure mit ludo-materieller agency in einem ludischen Netzwerk zu verstehen (S. 283f). In Folge werden unterschiedliche Dimensionen von Materialität an Spielfiguren und Formen von agency und affordances beleuchtet, die in Interaktion mit Spielenden entstehen.
Der Aufsatz "Have We Left the Paperverse Yet?" von Michael A. Conrad bedient sich ebenfalls ANT sowie Materialitätstheorie, um die verstrickte Geschichte von Spielen und Papier zu untersuchen. Dabei postuliert Conrad, in Opposition zu Marshall McLuhan, Papier als materielle Grundlage modernen Lebens zu verstehen (S. 316f). (Digitales) Gaming wird in dem Aufsatz als materielle Praxis verstanden und unteranderem auch auf materiell-ökologische Dimensionen hin untersucht.


Nach dieser Reihe an theoretischen Beiträgen lässt sich "Keep the Innovation Rolling" von Michael Sousa wieder im Bereich von Game Design verorten. Darin soll die Leerstelle fehlender systematischer Analysenzugänge auf den Einsatz von Würfeln in analogen Spielen gefüllt werden (S. 373). Zwar wird ebenfalls auf Begriffe wie Antizipation Bezug genommen, die theoretische Auseinandersetzung bleibt jedoch oberflächlich und die Argumentation Stellenweise schwach (S. 355f).


Der letzte Text "Immateriality and Immortality" von Emma Reay greift noch einmal die Themen Kindheit, Spielzeug und Nostalgie auf und überzeugt dort wo "Being a child through gameplay" scheitert. Anhand eines Close-Readings des Spiels Unraveled (Coldwood Interactive 2016) werden haptisch-panoptische Qualitäten von digitalen Spielzeugen untersucht. Dabei bilden Roland Barthes sowie Lynda Barrys Theorien zu Spielzeug, picture book theory und affordance theories die theoretische Grundlage. Durch Zoe Jaques Konzept der Spektralität, wird Spielzeug in Verbindung zu Erinnerungen und Nostalgie theoretisiert und über Katriina Heljakka werden theoretische Bezüge zwischen Spielzeugen und Avataren hergestellt. Spielbare digitale Toys stellen sich dabei als ideale Brücke zwischen materiellen und immateriellen Welten heraus. Neben den erwähnten Theorien nimmt der Text ebenfalls Bezug auf bereits zuvor untersuchte Gegenstände wie Papier und Controller. Letztlich hinterfragt der Text auch eine, in Game Diskursen gängige, koloniale Reduktion des Begriffs des Avatars auf den Aspekt der Verkörperung, während Konnotationen der Immanenz und Reinkarnation, die in spirituellen Konzepten des Avatars zentral sind, vernachlässigt werden. So schlägt Reay vor, das Spielen eines Avatars als eine spektrale Erfahrung zu verstehen, die Spielende als Geister anstatt Götter in eine virtuelle Materialität beschwört (S. 395f). So wird der thematische Kreis des Sammelbands, der mit der Materialisierung medialer Antizipationen begann, durch die Entmaterialisierung der Spielenden, geschlossen.


Die größte Leistung des Sammelbands liegt in dem beachtlichen Angebot an unterschiedlichen theoretischen Perspektiven und methodischen Zugängen sowie der Vielzahl an Anknüpfungspunkten an Games, die durch eine breite Auslegung des Feldes möglich wird. Die meisten Artikel weisen eine tiefgehende theoretische Ausarbeitung mit überzeugender Argumentation auf. Diejenigen Beiträge, die im Bereich Game Design und praktischen Auseinandersetzungen mit Games zu verorten sind, bieten ergänzend dazu interessante Aspekte am Rande wissenschaftlicher Diskurse. Mit steigender theoretischer Komplexität werden die Texte anspruchsvoller. Trotz der Diversität erscheinen die Aufsätze durch übergreifende und gemeinsame theoretische Konzepte, Gegenstände und Bezüge als kohärentes Gesamtwerk. Dadurch ergibt sich eine breite, interdisziplinäre Abhandlung von Games und deren materieller Dimension. Der Sammelband ist ein beeindruckendes Beispiel dafür, wie holistische Spieleforschung, die gängige Diskurse überdenkt und ausweitet, aussehen kann.
</description>
    <description descriptionType="SeriesInformation">[rezens.tfm], Nr. 2023/1</description>
  </descriptions>
</resource>
